
CELL_GEOMETRY = [
    { x: 7 , y: 2  },
    { x: 11, y: 2  },
    { x: 15, y: 2  },
    { x: 5 , y: 4  },
    { x: 9 , y: 4  },
    { x: 13, y: 4  },
    { x: 17, y: 4  },
    { x: 3 , y: 6  },
    { x: 7 , y: 6  },
    { x: 11, y: 6  },
    { x: 15, y: 6  },
    { x: 19, y: 6  },
    { x: 5 , y: 8  },
    { x: 9 , y: 8  },
    { x: 13, y: 8  },
    { x: 17, y: 8  },
    { x: 7 , y: 10 },
    { x: 11, y: 10 },
    { x: 15, y: 10 }
]

VERTEX_OFFSET = [
    { x: 2 , y: 1  },
    { x: 0 , y: 1  },
    { x: -2, y: 1  },
    { x: -2, y: -1 },
    { x: 0 , y: -1 },
    { x: 2 , y: -1 }
]

EDGE_OFFSET = [
    { x: 2 , y: 0  },
    { x: 1 , y: 1  },
    { x: -1, y: 1  },
    { x: -2, y: 0  },
    { x: -1, y: -1 },
    { x: 1 , y: -1 }
]

def is_board_object(x, y, offset_list)
    for t in CELL_GEOMETRY
        for s in offset_list
            ax = t[:x] + s[:x]
            ay = t[:y] + s[:y]

            if x == ax and y == ay
                return true
            end
        end
    end
    return false
end

def is_edge(x, y)
    is_board_object(x, y, EDGE_OFFSET)
end

def is_vertex(x, y)
    is_board_object(x, y, VERTEX_OFFSET)
end

def is_valid_coord(x, y)
    0 <= x and x < 23 and 0 <= y and y < 18
end

def main
    opt = OptionParser.new
    out = nil
    opt.on('-o VAL') {|v| out = v }
    opt.parse! ARGV
    return unless out

    # Generate edge table
    edge_table = Array.new(23) {|x| Array.new(18) {|y| is_edge(x, y) } }
    vertex_table = Array.new(23) {|x| Array.new(18) {|y| is_vertex(x, y) } }
    table_data = { 'edge' => edge_table, 'vertex' => vertex_table }
    File.open(out, 'w') do |f|
        f.write(JSON::generate(table_data))
    end
end

if __FILE__ == $0
    require 'optparse'
    require 'json'
    main
end

