require 'securerandom'
require 'json'

RESOURCE_KINDS = ['lumber', 'brick', 'wool', 'grain', 'ore']
PORT_KINDS = ['versatile', 'lumber', 'brick', 'wool', 'grain', 'ore']
LAND_KIND_TO_RESOURCE_KIND = {
    'hill' => 'brick',
    'forest' => 'lumber',
    'mountain' => 'ore',
    'field' => 'grain',
    'pasture' => 'wool'
}

CHANCE_CARD_KINDS = ['knight', 'building_road', 'harvest', 'monopoly', 'victory_point']

ADJACENT_CELLS = [
    [0, 3, 4], [1, 4, 5], [2, 5, 6],
    [3, 7, 8], [4, 8, 9], [5, 9, 10], [6, 10, 11],
    [8, 12, 13], [9, 13, 14], [10, 14, 15],
    [13, 16, 17], [14, 17, 18],
    [0, 1, 4], [1, 2, 5],
    [3, 4, 8], [4, 5, 9], [5, 6, 10],
    [7, 8, 12], [8, 9, 13], [9, 10, 14], [10, 11, 15],
    [12, 13, 16], [13, 14, 17], [14, 15, 18],
]

PORT_ACCESS = {
    'versatile' => [
        { 'x' => 9, 'y' => 1 },
        { 'x' => 11, 'y' => 1 },
        { 'x' => 19, 'y' => 7 },
        { 'x' => 19, 'y' => 9 },
        { 'x' => 9, 'y' => 11 },
        { 'x' => 11, 'y' => 11 },
        { 'x' => 1, 'y' => 5 },
        { 'x' => 1, 'y' => 7 }
    ],
    'lumber' => [
        { 'x' => 3, 'y' => 9 },
        { 'x' => 5, 'y' => 9 }
    ],
    'brick' => [
        { 'x' => 3, 'y' => 3 },
        { 'x' => 5, 'y' => 3 }
    ],
    'wool' => [
        { 'x' => 15, 'y' => 1 },
        { 'x' => 17, 'y' => 1 }
    ],
    'grain' => [
        { 'x' => 19, 'y' => 3 },
        { 'x' => 19, 'y' => 5 }
    ],
    'ore' => [
        { 'x' => 15, 'y' => 11 },
        { 'x' => 17, 'y' => 11 }
    ]
}

$board_kind = []
3.times do $board_kind << 'hill' end
4.times do $board_kind << 'forest' end
3.times do $board_kind << 'mountain' end
4.times do $board_kind << 'field' end
4.times do $board_kind << 'pasture' end
$board_value = [2,3,3,4,4,5,5,6,6,8,8,9,9,10,10,11,11,12]

$random = Random.new

def gen_land
    def is_red_cell?(x) x['value'] == 6 or x['value'] == 8 end
    def is_invalid_board?(x)
        for y in ADJACENT_CELLS
            if is_red_cell?(x[y[0]]) and is_red_cell?(x[y[1]]) and is_red_cell?(x[y[2]])
                return true
            end
        end
        return false
    end

    begin
        k = $board_kind.shuffle
        v = $board_value.shuffle
        i = $random.rand(18)
        k.insert(i, 'desert')
        v.insert(i, 7)

        a = []
        for i in 0..18 do
            a << { 'kind' => k[i], 'value' => v[i] }
        end
    end while is_invalid_board?(a)

    return a
end

def get_desert(land)
  land.find_index do |x|
    x['kind'] == 'desert'
  end
end

CELL_GEOMETRY = [
    { x: 7 , y: 2  },
    { x: 11, y: 2  },
    { x: 15, y: 2  },
    { x: 5 , y: 4  },
    { x: 9 , y: 4  },
    { x: 13, y: 4  },
    { x: 17, y: 4  },
    { x: 3 , y: 6  },
    { x: 7 , y: 6  },
    { x: 11, y: 6  },
    { x: 15, y: 6  },
    { x: 19, y: 6  },
    { x: 5 , y: 8  },
    { x: 9 , y: 8  },
    { x: 13, y: 8  },
    { x: 17, y: 8  },
    { x: 7 , y: 10 },
    { x: 11, y: 10 },
    { x: 15, y: 10 }
]

VERTEX_OFFSET = [
    { x: 2 , y: 1  },
    { x: 0 , y: 1  },
    { x: -2, y: 1  },
    { x: -2, y: -1 },
    { x: 0 , y: -1 },
    { x: 2 , y: -1 }
]

EDGE_OFFSET = [
    { x: 2 , y: 0  },
    { x: 1 , y: 1  },
    { x: -1, y: 1  },
    { x: -2, y: 0  },
    { x: -1, y: -1 },
    { x: 1 , y: -1 }
]

$geometry_info = File.open('./lib/catan/geometry_info.json') {|f| JSON::parse(f.read) }

def is_edge(x, y)
    $geometry_info['edge'][x][y]
end

def is_vertex(x, y)
    $geometry_info['vertex'][x][y]
end

def gen_empty_board_objects
    Array.new(23) { Array.new(18, 0) }
end

def is_valid_coord(x, y)
    0 <= x and x < 23 and 0 <= y and y < 18
end

def get_id_of_hexagon(x, y)
  CELL_GEOMETRY.find_index do |pos|
    pos[:x] == x && pos[:y] == y
  end
end

class Player
    attr_reader :user_id, :session_id, :name

    def initialize(user_id, session_id)
        user = User.find_by(user_id: user_id)

        @user_id = user_id
        @session_id = session_id
        @name = user.name
    end

    def trigger(event, data)
        WebsocketRails.users[@session_id].send_message(event, data)
    end

    def close!
        begin
            WebsocketRails.users[@session_id].connections[0].close!
        rescue
        end
    end

    def <=>(rht)
        if @user_id < rht.user_id
            return -1
        elsif @user_id > rht.user_id
            return 1
        else
            return 0
        end
    end

    def hashify
        {'name' => @name, 'user_id' => @user_id }
    end
end

DX = [1, 0, -1, 0]
DY = [0, 1, 0, -1]

class BoardObjects
    attr_reader :object_array

    def initialize
        @object_array = Array.new(23) { Array.new(18, nil) }
    end

    def house_obj(player_num)
        { 'kind' => 'house', 'player' => player_num }
    end

    def city_obj(player_num)
        { 'kind' => 'city', 'player' => player_num }
    end

    def road_obj(player_num)
        { 'kind' => 'road', 'player' => player_num }
    end

    def is_obj(x, y, player_num, kind)
        if not is_valid_coord(x, y)
            return false
        end

        o = @object_array[x][y]
        if o != nil and o['kind'] == kind and o['player'] == player_num
            return true
        end
        return false
    end

    def is_someones_obj(x, y, kind)
        if not is_valid_coord(x, y)
            return false
        end

        o = @object_array[x][y]
        if o != nil and o['kind'] == kind
            return true
        end
        return false
    end

    def whose_obj(x, y, kind)
        if not is_valid_coord(x, y)
            return nil
        end

        o = @object_array[x][y]
        if o != nil and o['kind'] == kind
            return o['player']
        end

        return nil
    end

    def is_house_obj(x, y, player_num)
        is_obj(x, y, player_num, 'house')
    end

    def is_city_obj(x, y, player_num)
        is_obj(x, y, player_num, 'city')
    end

    def is_road_obj(x, y, player_num)
        is_obj(x, y, player_num, 'road')
    end

    def is_able_to_put_house(x, y, player_num, force)
        if not is_vertex(x, y)
            return false
        end

        if is_someones_obj(x, y, 'house') || is_someones_obj(x, y, 'city')
            return false
        end

        for i in 0..3
            if not is_edge(x + DX[i], y + DY[i])
                next
            end

            ax = x + DX[i] * 2
            ay = y + DY[i] * 2
            if is_someones_obj(ax, ay, 'house') || is_someones_obj(ax, ay, 'city')
                return false
            end
        end

        if force
            return true
        end

        for i in 0..3
            ax = x + DX[i]
            ay = y + DY[i]
            if is_road_obj(ax, ay, player_num)
                return true
            end
        end
        return false
    end

    def is_able_to_put_city(x, y, player_num)
        return is_house_obj(x, y, player_num)
    end

    def is_able_to_put_road(x, y, player_num)
        if not is_edge(x, y)
            return false
        end

        if is_someones_obj(x, y, 'road')
            return false
        end

        for i in 0..3
            ax = x + DX[i]
            ay = y + DY[i]

            if is_vertex(ax, ay)
                if is_house_obj(ax, ay, player_num) or is_road_obj(ax, ay, player_num)
                    return true
                end

                for i in 0..3
                    bx = ax + DX[i]
                    by = ay + DY[i]

                    if is_road_obj(bx, by, player_num)
                        return true
                    end
                end
            end
        end

        return false
    end

    def is_able_to_put_road_at_the_beginning(x, y, player_num, hx, hy)
        if not is_edge(x, y)
            return false
        end

        if is_someones_obj(x, y, 'road')
            return false
        end

        for i in 0..3
            ax = x + DX[i]
            ay = y + DY[i]

            if ax == hx && ay == hy
                return true
            end
        end

        return false
    end

    def put_house(x, y, player_num, force)
        unless is_valid_coord(x, y)
          throw InvalidInputError.new "invalid coordination (#{x},#{y})"
        end

        unless is_able_to_put_house(x, y, player_num, force)
          throw InvalidInputError.new "it is not possible to put a house there"
        end

        @object_array[x][y] = house_obj(player_num)
    end

    def put_road(x, y, player_num)
        unless is_valid_coord(x, y)
          throw InvalidInputError.new "invalid coordination (#{x},#{y})"
        end
        unless is_able_to_put_road(x, y, player_num)
          throw InvalidInputError.new "it is not possible to put a road there"
        end

        @object_array[x][y] = road_obj(player_num)
    end

    def put_road_at_the_beginning(x, y, player_num, hx, hy)
        unless is_valid_coord(x, y)
            throw InvalidInputError.new "invalid coordination (#{x},#{y})"
        end

        unless is_able_to_put_road_at_the_beginning(x, y, player_num, hx, hy)
            throw InvalidInputError.new "it is not possible to put a road there"
        end

        @object_array[x][y] = road_obj(player_num)
    end

    def has_house_on_vertexes(hex_id, player_id)
        pos = CELL_GEOMETRY[hex_id]
        for i in VERTEX_OFFSET
            x = pos[:x] + i[:x]
            y = pos[:y] + i[:y]

            if (is_someones_obj(x, y, 'house') && !is_house_obj(x, y, player_id)) or
              (is_someones_obj(x, y, 'city') && !is_city_obj(x, y, player_id))
                return true
            end
        end

        return false
    end

    def put_city(x, y, player_id)
        unless is_valid_coord(x, y)
            throw InvalidInputError.new "invalid coordination (#{x},#{y})"
        end

        unless is_able_to_put_city(x, y, player_id)
            throw InvalidInputError.new "it is not possible to put a city there"
        end

        @object_array[x][y] = city_obj(player_id)
    end

    def calc_road_length(player_id)
        traversed = nil

        def f(x, y, t, p)
            if not is_road_obj(x, y, p)
                return 0
            end

            t[x][y] = true

            l = 1
            for i in 0...4
                vx = x + DX[i]
                vy = y + DY[i]

                if not is_vertex(vx, vy) or t[vx][vy]
                    next
                end

                a = is_someones_obj(vx, vy, 'house') && !is_house_obj(vx, vy, p)
                b = is_someones_obj(vx, vy, 'city') && !is_city_obj(vx, vy, p)

                if a or b
                    next
                end

                t[vx][vy] = true

                for j in 0...4
                    nx = vx + DX[j]
                    ny = vy + DY[j]

                    if not is_edge(nx, ny)
                        next
                    end

                    if !t[nx][ny]
                        l = [f(nx, ny, t, p) + 1, l].max
                    end
                end

                t[vx][vy] = false
            end

            t[x][y] = false

            return l
        end

        m = 0
        for sx in 0...23
            for sy in 0...18
                traversed = Array.new(23) { Array.new(18, false) }

                m = [f(sx, sy, traversed, player_id), m].max
            end
        end

        return m
    end
end

class PlayerStatus
    attr_accessor :score, :road_length
    attr_reader :num_resources, :chance_cards, :special_cards, :ports, :structure_deck, :used_chance_cards, :new_chance_cards, :new_ports
    def initialize(name)
        # Visible information
        @score = 0
        @name = name
        @num_resources = {'lumber' => 0,
                          'brick' => 0,
                          'wool' => 0,
                          'ore' => 0,
                          'grain' => 0 }
        @chance_cards = {'knight' => 0,
                         'building_road' => 0,
                         'harvest' => 0,
                         'monopoly' => 0,
                         'victory_point' => 0 }
        @new_chance_cards = {'knight' => 0,
                                   'building_road' => 0,
                                   'harvest' => 0,
                                   'monopoly' => 0,
                                   'victory_point' => 0 }
        @used_chance_cards = {'knight' => 0,
                              'building_road' => 0,
                              'harvest' => 0,
                              'monopoly' => 0,
                              'victory_point' => 0 }
        @special_cards = {'longest_road' => false,
                          'largest_army' => false }
        @ports = {'versatile' => false,
                  'lumber' => false,
                  'brick' => false,
                  'wool' => false,
                  'ore' => false,
                  'grain' => false }
        @new_ports = {'versatile' => false,
                      'lumber' => false,
                      'brick' => false,
                      'wool' => false,
                      'ore' => false,
                      'grain' => false }
        @structure_deck = {'house' => 5,
                           'city' => 4,
                           'road' => 15 }

        # Invisible information
        @road_length = 0
    end

    def recalculate_score
        s = 0
        if @special_cards['longest_road']
            s += 2
        end
        if @special_cards['largest_army']
            s += 2
        end

        num_house = 5 - @structure_deck['house']
        num_city = 4 - @structure_deck['city']

        s += num_house * 1 + num_city * 2

        @score = s
    end

    def turn_end
      for k in @new_chance_cards.keys
        @chance_cards[k] += @new_chance_cards[k]
        @new_chance_cards[k] = 0
      end
      for k in @new_ports.keys
        @ports[k] ||= @new_ports[k]
        @new_ports[k] = false
      end
    end

    def hashify
        recalculate_score

        {'score' => @score,
         'resources' => @num_resources,
         'chance_cards' => @chance_cards,
         'used_chance_cards' => @used_chance_cards,
         'new_chance_cards' => @new_chance_cards,
         'special_cards' => @special_cards,
         'ports' => @ports,
         'structure_deck' => @structure_deck }
    end
end

#
# InvalidInputError
#

class InvalidInputError < StandardError
  def initialize(message)
    super message
  end
end

#
# GameManager controls the flow of one game.
#

class GameManager
    attr_reader :board_objects, :board_land, :robber_pos
    attr_reader :waiting_command, :waiting_player_id, :waiting_info, :player_status

    def initialize(room, player_name_list)
        @room = room
        @num_players = player_name_list.length
        @game_thread = nil

        @longest_road_id = nil
        @largest_army_id = nil

        @board_objects = BoardObjects.new
        @board_land = gen_land

        @player_status = player_name_list.map { |x| PlayerStatus.new x }

        @waiting_command = nil
        @waiting_player_id = []
        @waiting_info = {}

        @resource_deck = {'lumber' => 19,
                          'brick' => 19,
                          'wool' => 19,
                          'grain' => 19,
                          'ore' => 19 }
        @chance_card_deck = {'knight' => 14,
                             'building_road' => 2,
                             'harvest' => 2,
                             'monopoly' => 2,
                             'victory_point' => 5}
        @robber_pos = get_desert @board_land

        if @num_players != 0
            generate_fiber
        end

        notify_player_status_changed
        notify_resource_deck_info
    end

    def kick(player_id, reason)
      @room.kick_player(player_id, reason)
    end

    def notify_player_status_changed
        s = @player_status.map{|x| x.hashify }
        @room.notify_player_status_changed s

        if @waiting_player_id.length == 1 && @waiting_command == 'any'
            id = @waiting_player_id[0]
            if @player_status[id].score >= 10
                input_victory(id)
            end
        end
    end

    def set_wait_generic(command, player_ids, info)
      @waiting_command = command
      @waiting_player_id = player_ids
      @waiting_info = info
      @room.notify_waiting command, player_ids, info
    end

    def set_wait_with_info(command, player_id, info)
      set_wait_generic command, [player_id], info
    end

    def set_wait(command, player_id)
      set_wait_generic command, [player_id], {}
    end

    def stop_wait(player_id)
        @waiting_player_id.delete(player_id)
    end

    def get_resource_at_the_beginning(player_id, x, y)
        for i in 0...CELL_GEOMETRY.length
            kind = @board_land[i]['kind']
            if kind == 'desert'
                next
            end

            resource_kind = LAND_KIND_TO_RESOURCE_KIND[kind]
            for j in VERTEX_OFFSET
                ax = CELL_GEOMETRY[i][:x] + j[:x]
                ay = CELL_GEOMETRY[i][:y] + j[:y]

                if ax == x && ay == y
                    @player_status[player_id].num_resources[resource_kind] += 1
                    @resource_deck[resource_kind] -= 1
                    #p 'Resource generated'
                    #p @resource_deck
                end
            end
        end

        notify_player_status_changed
        notify_resource_deck_info
    end

    def input_house_at_the_beginning(player_id, is_second)
        begin
            set_wait('put_house_begin', player_id)
            command = Fiber.yield
            x = command['x']
            y = command['y']
            @board_objects.put_house(x, y, player_id, true)
            acquire_port(player_id, x, y)
            if is_second
                get_resource_at_the_beginning(player_id, x, y)
            end
            @player_status[player_id].structure_deck['house'] -= 1

            @room.notify_consume(player_id, 'house')
            @room.notify_object_change
            notify_player_status_changed
            notify_resource_deck_info
        rescue InvalidInputError => e
            kick player_id, "Invalid input: #{e.message}"
            retry
        end
        return command
    end

    def input_road_at_the_beginning(player_id, hx, hy)
        begin
            set_wait_with_info('put_road_begin', player_id, {
              'x' => hx,
              'y' => hy
            })
            command = Fiber.yield
            @board_objects.put_road_at_the_beginning(command['x'], command['y'], player_id, hx, hy)
            @player_status[player_id].structure_deck['road'] -= 1

            @room.notify_consume(player_id, 'road')
            @room.notify_object_change
        rescue InvalidInputError => e
            kick player_id, "Invalid input: #{e.message}"
            retry
        end
    end

    def input_road(player_id)
        begin
            set_wait('put_road', player_id)
            command = Fiber.yield
            @board_objects.put_road(command['x'], command['y'], player_id)
            @player_status[player_id].structure_deck['road'] -= 1

            update_longest_road(false)
            @room.notify_object_change
        rescue InvalidInputError => e
            kick player_id, "Invalid input: #{e.message}"
            retry
        end
    end

    def input_victory(player_id)
        set_wait('victory', player_id)
        Fiber.yield
        @room.notify_victory(player_id)
    end

# FIXME: Divide this function
    def input_diceroll(player_id)
        if @player_status[player_id].score >= 10
            input_victory(player_id)
            return
        end

        while true
          set_wait('diceroll', player_id)
          command = Fiber.yield
          if command['command'] == 'use_card'
            use_card player_id, command
          else
            break
          end
        end

        value = 0
        if $debug_mode
            begin
                print 'Dice (2..12)> '
                value = Integer(STDIN.gets.chomp)
                if not (2 <= value && value <= 12)
                    raise
                end

                if value >= 7
                    dice1 = value - 6
                    dice2 = 6
                else
                    dice1 = value - 1
                    dice2 = 1
                end
            rescue
                puts "Invalid input"
                retry
            end
        else
            dice1 = $random.rand(1..6)
            dice2 = $random.rand(1..6)
            value = dice1 + dice2
        end

        num_to_generate = {
            'lumber' => 0,
            'brick' => 0,
            'wool' => 0,
            'grain' => 0,
            'ore' => 0
        }

        resource_shortage = {
            'lumber' => false,
            'brick' => false,
            'wool' => false,
            'grain' => false,
            'ore' => false
        }

        for i in 0...CELL_GEOMETRY.length
            kind = @board_land[i]['kind']

            if i == @robber_pos || kind === 'desert' || @board_land[i]['value'] != value
                next
            end

            resource_kind = LAND_KIND_TO_RESOURCE_KIND[kind]

            for j in VERTEX_OFFSET
                x = CELL_GEOMETRY[i][:x] + j[:x]
                y = CELL_GEOMETRY[i][:y] + j[:y]

                house_owner = @board_objects.whose_obj(x, y, 'house')
                city_owner = @board_objects.whose_obj(x, y, 'city')

                if house_owner != nil
                    num_to_generate[resource_kind] += 1
                elsif city_owner != nil
                    num_to_generate[resource_kind] += 2
                end
            end
        end

        for i in 0...CELL_GEOMETRY.length
            kind = @board_land[i]['kind']

            if i == @robber_pos || kind === 'desert' || @board_land[i]['value'] != value
                next
            end

            resource_kind = LAND_KIND_TO_RESOURCE_KIND[kind]

            if num_to_generate[resource_kind] > @resource_deck[resource_kind]
                next
            end

            for j in VERTEX_OFFSET
                x = CELL_GEOMETRY[i][:x] + j[:x]
                y = CELL_GEOMETRY[i][:y] + j[:y]

                house_owner = @board_objects.whose_obj(x, y, 'house')
                city_owner = @board_objects.whose_obj(x, y, 'city')

                if house_owner != nil
                    @player_status[house_owner].num_resources[resource_kind] += 1
                elsif city_owner != nil
                    @player_status[city_owner].num_resources[resource_kind] += 2
                end
            end
        end

        for i in RESOURCE_KINDS
            if num_to_generate[i] <= @resource_deck[i]
                @resource_deck[i] -= num_to_generate[i]
            else
                resource_shortage[i] = true
                num_to_generate[i] = 0
            end
        end

        #p 'Resource generated'
        #p num_to_generate
        #p 'Resource deck'
        #p @resource_deck
        #puts ''

        @room.notify_diceroll(dice1, dice2)
        notify_player_status_changed
        notify_resource_deck_info true

        if value == 7
            # Burst
            burst_ids = []
            burst_info = { 'burst_info' => [] }
            for i in 0...@num_players
                p = @player_status[i]
                sum = RESOURCE_KINDS.inject(0) {|s, x| s + p.num_resources[x] }

                if sum >= 8
                    burst_ids << i
                    burst_info['burst_info'] << { 'is_burst' => true,
                                                  'num_to_dispose' => sum / 2}
                else
                    burst_info['burst_info'] << { 'is_burst' => false,
                                                  'num_to_dispose' => 0 }
                end
            end

            if burst_ids.length > 0
                set_wait_generic('burst', burst_ids, burst_info)
                begin
                    command = Fiber.yield
                    p_id = command['player_id']
                    p_info = @player_status[p_id]
                    amounts = command['amounts']
                    # TODO: Error handling
                    for i in 0...5
                        p_info.num_resources[RESOURCE_KINDS[i]] -= amounts[i]
                    end
                    burst_ids.delete(p_id)
                    stop_wait(p_id)

                    burst_info['burst_info'][p_id]['is_burst'] = false
                    burst_info['burst_info'][p_id]['num_to_dispose'] = 0

                    @room.notify_dispose(p_id, amounts)
                    notify_player_status_changed
                    notify_resource_deck_info
                end while burst_ids.length > 0
            end

            # Move robber
            input_knight player_id

            if @board_objects.has_house_on_vertexes(@robber_pos, player_id)
                input_target_of_robber player_id
            else
                @room.notify_robbery(player_id, nil)
            end
        else
            for i in RESOURCE_KINDS
                if resource_shortage[i]
                    @room.notify_shortage i
                end
            end
        end
    end

    def input_target_of_robber(player_id)
        begin
            set_wait('target_of_robber', player_id)
            command = Fiber.yield

            rob(player_id, command['x'], command['y'])
            notify_player_status_changed
            notify_resource_deck_info
        rescue InvalidInputError => e
            kick player_id, "Invalid input: #{e.message}"
            retry
        end
    end

    def rob(player_id, x, y)
        target = @board_objects.whose_obj(x, y, 'house')
        if target == nil
            target = @board_objects.whose_obj(x, y, 'city')
        end

        if target == nil
            throw InvalidInputError.new "There is no house or city in (#{x},#{y})"
        end

        target_resources = @player_status[target].num_resources

        target_num = target_resources.values.inject {|s, x| s + x }
        if target_num == 0
            @room.notify_robbery(player_id, nil)
            return
        end

        @room.notify_robbery(player_id, target)

        r = $random.rand(0...target_num)
        for i in RESOURCE_KINDS
            if r < target_resources[i]
                target_resources[i] -= 1
                @player_status[player_id].num_resources[i] += 1
                return
            end

            r -= target_resources[i]
        end

        throw "Logic error"
    end

    def input_knight(player_id)
        begin
            set_wait('knight', player_id)
            command = Fiber.yield

            move_robber(command['x'], command['y'])
            @room.notify_robber_moved(@robber_pos)
        rescue InvalidInputError => e
            kick player_id, "Invalid input: #{e.message}"
            retry
        end
    end

    def move_robber(x, y)
        hex_id = get_id_of_hexagon(x, y)
        if hex_id == nil
            throw InvalidInputError.new "The coord (#{x},#{y}) does not point the center of a hexagon"
        end

        #puts "Robber moved: #{hex_id}"

        @robber_pos = hex_id
    end

    def input_command(player_id)
        set_wait('any', player_id)
        command = Fiber.yield

        begin
          case command['command']
          when 'put_house'
              consume_resource(player_id, 'house')
              @board_objects.put_house(command['x'], command['y'], player_id, false)
              acquire_port(player_id, command['x'], command['y'])
              @room.notify_consume(player_id, 'house')
              update_longest_road(true)
              @room.notify_object_change
              notify_player_status_changed
              notify_resource_deck_info
          when 'put_road'
              consume_resource(player_id, 'road')
              @board_objects.put_road(command['x'], command['y'], player_id)
              @room.notify_consume(player_id, 'road')
              update_longest_road(false)
              @room.notify_object_change
              notify_player_status_changed
              notify_resource_deck_info
          when 'put_city'
              consume_resource(player_id, 'city')
              @board_objects.put_city(command['x'], command['y'], player_id)
              @room.notify_consume(player_id, 'city')
              @room.notify_object_change
              notify_player_status_changed
              notify_resource_deck_info
          when 'generate_card'
              consume_resource(player_id, 'card')
              generate_chance_card(player_id)
              @room.notify_consume(player_id, 'card')
              notify_player_status_changed
              notify_resource_deck_info
          when 'trade_oversea'
              exs = command['export']
              ins = command['import']
              trade_oversea(player_id, exs, ins)
              @room.notify_trade_oversea(player_id, exs, ins)
              notify_player_status_changed
              notify_resource_deck_info
          when 'trade_local'
              partner_id = command['partner']
              exs = command['export']
              ins = command['import']

              if not trade_is_legal(exs, ins)
                  return false
              end

              @room.notify_trade_suggestion player_id, partner_id, exs, ins

              ok = input_trade_acceptance(partner_id, player_id, exs, ins)
              if ok
                  trade_local(player_id, partner_id, exs, ins)
              end
              @room.notify_trade_result(player_id, partner_id, ok)
              notify_player_status_changed
              notify_resource_deck_info
          when 'use_card'
              player_id = command['player_id']
              use_card player_id, command
          when 'end'
              @player_status[player_id].turn_end
              return true
          when 'victory'
              unless @player_status[player_id].score >= 10
                  throw InavlidInputError.new 'Score not enough'
              end

              @room.notify_victory(player_id)
          else
              throw InvalidInputError.new "Unknown command"
          end
        rescue InvalidInputError => e
          kick player_id, "Invalid input: #{e.message}"
          retry
        end
        return false
    end

    def use_card(player_id, command)
      ps = @player_status[player_id]
      chance_card_kind = command['kind']

      if ps.chance_cards[chance_card_kind] == 0
          throw InvalidInputError.new "No chance card"
      end

      ps.chance_cards[chance_card_kind] -= 1
      ps.used_chance_cards[chance_card_kind] += 1

      @room.notify_consume_with_info(player_id, chance_card_kind, command)

      case chance_card_kind
      when 'knight'
          update_largest_army

          input_knight player_id

          if @board_objects.has_house_on_vertexes(@robber_pos, player_id)
              input_target_of_robber player_id
          else
              @room.notify_robbery(player_id, nil)
          end
          notify_player_status_changed
          notify_resource_deck_info
      when 'building_road'
          2.times {
              if @player_status[player_id].structure_deck['road'] > 0
                  input_road(player_id)
              end
          }
          notify_player_status_changed
          notify_resource_deck_info
      when 'harvest'
          resource_kinds = command['resource_kinds']
          if resource_kinds.length != 2
              throw InvalidInputError.new 'invalid command'
          end

          if resource_kinds[0] == resource_kinds[1]
              if @resource_deck[resource_kinds[0]] < 2
                  # Not enough
                  # FIXME: Error handling
                  throw ''
              end
          else
              if @resource_deck[resource_kinds[0]] < 1 or
                  @resource_deck[resource_kinds[1]] < 1

                  # Not enough
                  throw ''
              end
          end

          for i in resource_kinds
              @resource_deck[i] -= 1
              ps.num_resources[i] += 1
          end
          notify_player_status_changed
          notify_resource_deck_info
      when 'monopoly'
          resource_kind = command['resource_kind']
          n = @player_status.inject(0) {|s, x| s + x.num_resources[resource_kind] }
          @player_status[player_id].num_resources[resource_kind] = n
          for i in 0...@num_players
              if i != player_id
                  @player_status[i].num_resources[resource_kind] = 0
              end
          end
          notify_player_status_changed
          notify_resource_deck_info
      else
          throw InvalidInputError.new "Unknown card kind: #{chance_card_kind}"
      end
    end

    def trade_is_legal(exs, ins)
        for i in 0...5
            if exs[i] != 0 and ins[i] != 0
                return false
            end
        end

        exs_sum = exs.inject {|s, x| s + x }
        ins_sum = ins.inject {|s, x| s + x }

        if exs_sum == 0 or ins_sum == 0
            return false
        end

        return true
    end

    def input_trade_acceptance(partner_id, player_id, exs, ins)
        info = {
            'partner' => player_id,
            'export' => exs,
            'import' => ins
        }

        set_wait_with_info('trade_acceptance', partner_id, info)
        command = Fiber.yield

        return command['ok'] && is_able_to_trade(player_id, partner_id, exs, ins)
    end

    def is_able_to_trade(player_id, partner_id, exs, ins)
        s1 = @player_status[player_id]
        s2 = @player_status[partner_id]

        for i in 0...5
            kind = RESOURCE_KINDS[i]
            if s1.num_resources[kind] < exs[i] || s2.num_resources[kind] < ins[i]
                # p 'not able to trade'
                return false
            end
        end

        # p 'able to trade'
        return true
    end

    def trade_local(player_id, partner_id, exs, ins)
        s1 = @player_status[player_id]
        s2 = @player_status[partner_id]

        #p 'before trade'
        #p s1.num_resources
        #p s2.num_resources
        #p exs
        #p ins

        for i in 0...5
            kind = RESOURCE_KINDS[i]
            throw if s1.num_resources[kind] < exs[i] || s2.num_resources[kind] < ins[i]

            s1.num_resources[kind] -= exs[i]
            s2.num_resources[kind] += exs[i]

            s1.num_resources[kind] += ins[i]
            s2.num_resources[kind] -= ins[i]
        end

        #p 'after trade'
        #p s1.num_resources
        #p s2.num_resources
    end

    RECIPE = {
        'house' =>
            {'lumber' => 1,
             'brick' => 1,
             'wool' => 1,
             'ore' => 0,
             'grain' => 1 },
        'road' =>
            {'lumber' => 1,
             'brick' => 1,
             'wool' => 0,
             'ore' => 0,
             'grain' => 0 },
        'city' =>
            {'lumber' => 0,
             'brick' => 0,
             'wool' => 0,
             'ore' => 3,
             'grain' => 2 },
        'card' =>
            {'lumber' => 0,
             'brick' => 0,
             'wool' => 1,
             'ore' => 1,
             'grain' => 1 },
    }

    def generate_chance_card(player_id)
        num_card_rest = 0
        for i in CHANCE_CARD_KINDS
            num_card_rest += @chance_card_deck[i]
        end

        if num_card_rest == 0
            raise 'No cards'
        end

        if $debug_mode
            begin
                avail = (@chance_card_deck.select {|k, v| v > 0 }).keys
                #p "Card", avail
                print '> '
                i = STDIN.gets.chomp

                for x in avail
                    if x.start_with?(i)
                        @chance_card_deck[x] -= 1
                        @player_status[player_id].new_chance_cards[x] += 1
                        return
                    end
                end

                raise "Invalid input"
            rescue
                puts $!
                retry
            end
        else
            r = $random.rand(0...num_card_rest)
            for i in CHANCE_CARD_KINDS
                if r < @chance_card_deck[i]
                    @chance_card_deck[i] -= 1
                    @player_status[player_id].new_chance_cards[i] += 1
                    return
                end

                r -= @chance_card_deck[i]
            end
        end

        throw "Logic error"
    end

    def consume_resource(player_id, kind)
      player_status = @player_status[player_id]
      num_resources = player_status.num_resources
      if kind != 'card' && !player_status.structure_deck.has_key?(kind)
        throw InvalidInputError.new "No such resource"
      end
      if kind != 'card' && player_status.structure_deck[kind] < 1
        throw InvalidInputError.new "No #{kind} in deck"
      end
      unless RESOURCE_KINDS.all? {|k| num_resources[k] >= RECIPE[kind][k] }
        throw InvalidInputError.new 'Resource is not enough'
      end

      if kind == 'city'
          player_status.structure_deck['city'] -= 1
          player_status.structure_deck['house'] += 1
      elsif player_status.structure_deck.has_key?(kind)
          player_status.structure_deck[kind] -= 1
      end

      RESOURCE_KINDS.each { |x|
          num_resources[x] -= RECIPE[kind][x]
          @resource_deck[x] += RECIPE[kind][x]
      }
    end

    def trade_oversea(player_id, export, import)
        player_status = @player_status[player_id]
        num_capacity = 0;
        num_require = 0;

        for i in 0...5
            kind = RESOURCE_KINDS[i]
            step = 4
            if player_status.ports[kind]
                step = 2;
            elsif player_status.ports['versatile']
                step = 3;
            end

            if export[i] % step != 0 or
                !(0 <= export[i] && export[i] <= player_status.num_resources[kind])

                throw InvalidInputError.new 'Invalid export'
            end

            num_capacity += export[i] / step
        end

        num_require = import.inject {|s, x| s + x }
        for i in import
            if i < 0
                throw InvalidInputError.new 'Invalid import'
            end
        end

        if num_capacity != num_require
            throw InvalidInputError.new 'Import and export are mismatched'
        end

        for i in 0...5
            kind = RESOURCE_KINDS[i]
            player_status.num_resources[kind] -= export[i]
            @resource_deck[kind] += export[i]
            player_status.num_resources[kind] += import[i]
            @resource_deck[kind] -= import[i]
        end
    end

    def acquire_port(player_id, x, y)
        player_status = @player_status[player_id]

        for i in PORT_KINDS
            for pos in PORT_ACCESS[i]

                if pos['x'] == x and pos['y'] == y
                    player_status.new_ports[i] = true
                end
            end
        end
    end

    def recalc_road_length
        ls = []
        m = 0
        for i in 0...@num_players
            l = @board_objects.calc_road_length(i)
            @player_status[i].road_length = l
            m = [m, l].max
            ls << l
        end

        #p ls, m

        return ls.each_index.select {|i| ls[i] == m && ls[i] >= 5 }
    end

    def recalc_longest_road(devided)
        old_longest_road_id = @longest_road_id

        ms = recalc_road_length

        for i in 0...@num_players
            @player_status[i].special_cards['longest_road'] = false
        end

        if ms.length > 1
            if devided
                @longest_road_id = nil
            end
        elsif ms.length == 1
            @longest_road_id = ms[0]
        else # length == 0
            @longest_road_id = nil
        end

        if @longest_road_id
          @player_status[@longest_road_id].special_cards['longest_road'] = true
        end

        return (old_longest_road_id != @longest_road_id) && @longest_road_id != nil
    end

    def update_longest_road(devided)
        updated = recalc_longest_road(devided)
        if updated
            @room.notify_longest_road_changed @longest_road_id
        end
    end

    def update_largest_army
        ns = @player_status.map {|x| x.used_chance_cards['knight'] }
        m = ns.max
        is = ns.each_index.select {|i| ns[i] == m && ns[i] >= 3 }

        for i in 0...@num_players
            @player_status[i].special_cards['largest_army'] = false
        end

        if is.length != 1
            return
        end

        if @largest_army_id != is[0]
            @largest_army_id = is[0]
            @player_status[is[0]].special_cards['largest_army'] = true
            @room.notify_largest_army_changed @largest_army_id
        end
    end

    def notify_resource_deck_info(on_diceroll = false)
        d = {}
        @resource_deck.each {|k, v|
            if v > 19 * (2 / 3.0)
                d[k] = 2
            elsif v > 19 * (1 / 3.0)
                d[k] = 1
            else
                d[k] = 0
            end
        }
        s = @chance_card_deck.each_value.inject(0) {|s, x| s + x }
        if s > 25 * (2 / 3.0)
            d['chance_cards'] = 2
        elsif s > 25 * (1 / 3.0)
            d['chance_cards'] = 1
        else
            d['chance_cards'] = 0
        end
        d['on_diceroll'] = on_diceroll

        @room.notify_resource_deck_info d
    end

    def generate_fiber
        @game_thread = Fiber.new { |command_obj|
            for i in 0..(@num_players - 1) do
                c = input_house_at_the_beginning i, false
                input_road_at_the_beginning i, c['x'], c['y']
            end

            (@num_players - 1).downto(0) do |i|
                c = input_house_at_the_beginning i, true
                input_road_at_the_beginning i, c['x'], c['y']
            end

            while true do
                for i in 0..(@num_players - 1) do
                    input_diceroll i

                    cont = nil
                    begin
                        cont = input_command i
                    end until cont
                end
            end
        }

        begin
          @game_thread.resume nil
        rescue
          puts "Error: #{$!}"
        end
    end

    def on_receiving_command(command)
        if @waiting_player_id.include?(command['player_id']) &&
            (@waiting_command == 'any' || @waiting_command == command['command'] ||
             (@waiting_command == 'diceroll' && command['command'] == 'use_card'))

            begin
              @game_thread.resume command
            rescue
              puts "Error: #{$!}"
            end
        end
    end
end

class Room
    attr_accessor :password, :name, :owner_user_id, :owner_name
    attr_reader :is_playing, :room_members, :room_id

    CHAT_LOG_MAX_LENGTH = 100

    def initialize(name, password, owner_user_id, owner_name)
        @room_id = SecureRandom.urlsafe_base64(8)

        @name = name
        @password = password
        @owner_user_id = owner_user_id
        @owner_name = owner_name

        # Array of Player object
        @room_members = []

        # Array of user_id
        @not_players = []
        @players = []

        @chat_log = []

        @game_manager = GameManager.new self, []
        @is_playing = false
    end

    def add_member(user_id, session_id)
        p = Player.new(user_id, session_id)
        @room_members << p
        @room_members.sort!

        if not @players.find {|x| x == user_id }
            if @players.length < 4 and not @is_playing
                @players << user_id
            else
                @not_players << user_id
            end
        end

        p.trigger 'joined', {
            'user_id' => user_id,
            'chat_log' => @chat_log }

        notify_object_change
        notify_room_members_change(p.name, true)
        send_whole_game_data(p) if @is_playing
    end

    def remove_member(session_id)
        member = @room_members.find {|x| x.session_id == session_id }

        @room_members.reject! {|x| x == member }
        @not_players.reject! {|x| x == member.user_id }
        # Not for @players

        member.close! if member

        notify_room_members_change(member.name, false)

        # If there is no room member, the room should be deleted
        return @room_members.length == 0
    end

    def kick_player(player_id, reason)
      x = @players[player_id]
      x.trigger 'kicked', { 'reason' => reason }
      remove_member x.session_id
    end

    def get_player_by_user_id(user_id)
        @room_members.find {|x| x.user_id == user_id }
    end

    def get_player_by_session_id(session_id)
        @room_members.find {|x| x.session_id == session_id }
    end

    def change_player_list(player_list, not_player_list)
        player_list_s = player_list.sort.uniq
        not_player_list_s = not_player_list.sort.uniq
        member_list = (player_list + not_player_list).sort

        return if (player_list & not_player_list).length != 0 ||
            player_list.length != player_list_s.length ||
            not_player_list.length != not_player_list_s.length ||
            member_list != @room_members.map {|x| x.user_id } ||
            player_list.length >= 4

        @players = player_list
        @not_players = not_player_list

        notify_room_members_change('', false)
    end

    def transfer_authority(new_owner_id)
        owner = @room_members.find {|x| x.user_id == new_owner_id }
        return false unless owner

        @owner_user_id = new_owner_id
        @owner_name = owner.name

        notify_room_members_change '', false
        true
    end

    def game_start
        return if @players.length < 2

        notify_new_game
        @is_playing = true
        @game_manager = GameManager.new self,
            @players.map {|x| get_player_by_user_id(x).name }
        @room_members.each {|x| notify_board_change x }

        robber_moved = gen_robber_moved @game_manager.robber_pos
        @room_members.each {|x| x.trigger 'robber_moved', robber_moved }
    end

    def on_receiving_command(session_id, command)
        user_id = get_player_by_session_id(session_id).user_id
        player_id = @players.find_index {|x| x == user_id }
        return unless player_id

        command['player_id'] = player_id
        @game_manager.on_receiving_command command
    end

    def on_receiving_chat(session_id, command)
        p = get_player_by_session_id(session_id)
        player_id = @players.find_index {|x| x == p.user_id }
        name = p.name

        d = { 'player_id' => @is_playing ? player_id : nil,
              'name' => name,
              'message' => command['message'] }

        if @chat_log.length >= CHAT_LOG_MAX_LENGTH
            @chat_log.shift
        end
        @chat_log.push d

        @room_members.each {|x| x.trigger 'chat', d }
    end

    def dice_1d100(session_id)
        p = get_player_by_session_id(session_id)
        player_id = @players.find_index {|x| x == p.user_id }

        d = { 'player_id' => @is_playing ? player_id : nil,
              'name' => "#{p.name} (DICE)",
              'message' => $random.rand(1..100).to_s }

        if @chat_log.length >= CHAT_LOG_MAX_LENGTH
            @chat_log.shift
        end
        @chat_log.push d

        @room_members.each {|x| x.trigger 'chat', d }
    end

    def notify_room_members_change(name, is_joined)
        print "COMMAND notify room_members:\n"
        @room_members.each {|x| puts " #{x.name}" }

        d = {'member' => name,
             'is_joined' => is_joined,
             'owner_id' => @owner_user_id,
             'room_member_list' => @room_members.map {|x| x.hashify },
             'not_players' => @not_players,
             'players' => @players }
        @room_members.each {|x|
            x.trigger 'room_member_changed', d
        }
    end

    def send_whole_game_data(ws_conn)
        notify_board_change ws_conn

        ws_conn.trigger 'game_start', { 'command' => 'game_start', 'players' => @players }

        ws_conn.trigger 'object_changed', gen_object_change

        robber_moved = gen_robber_moved @game_manager.robber_pos
        ws_conn.trigger 'robber_moved', robber_moved

        sta = gen_player_status_changed @game_manager.player_status.map {|x| x.hashify }
        ws_conn.trigger 'player_status_changed', sta

        player_ids = @game_manager.waiting_player_id
        if player_ids.length != 0
            waiting_command = @game_manager.waiting_command
            info = @game_manager.waiting_info
            wai = gen_waiting waiting_command, player_ids, info
            ws_conn.trigger 'waiting', wai
        end
    end

    def notify_board_change(ws_conn)
        ws_conn.trigger 'board_changed', gen_board_change
    end

    def gen_board_change
        { 'command' => 'board_changed',
                         'board_info' => @game_manager.board_land }
    end

    def notify_object_change
        dat = gen_object_change
        @room_members.each {|x|
            x.trigger 'object_changed', dat
        }
    end

    def gen_object_change
        { 'object_info' => @game_manager.board_objects.object_array }
    end

    def notify_waiting(command, player_ids, info)
        dat = gen_waiting command, player_ids, info
        @room_members.each {|x| x.trigger 'waiting', dat }
    end

    def gen_waiting(command, player_ids, info)
        dat = { 'command' => 'waiting',
                'waiting_player_ids' => player_ids,
                'waiting_command' => command }
        dat.merge! info
        dat
    end

    def notify_trade_result(player_id, partner_id, ok)
        dat = gen_trade_result(player_id, partner_id, ok)
        @room_members.each {|x| x.trigger 'trade_result', dat }
    end

    def gen_trade_result(player_id, partner_id, ok)
        {
            'command' => 'trade_result',
            'player_id' => player_id,
            'partner_id' => partner_id,
            'ok' => ok
        }
    end

    def notify_player_status_changed(status_data)
        command = gen_player_status_changed status_data
        @room_members.each {|x|
            x.trigger 'player_status_changed', command
        }
    end

    def notify_resource_deck_info(deck)
        e = {
            'command' => 'resource_deck_info',
            'deck' => deck
        }
        @room_members.each {|x| x.trigger 'resource_deck_info', e }
    end

    def gen_player_status_changed(status_data)
        { 'command' => 'player_status_changed', 'data' => status_data }
    end

    def gen_new_game
        { 'command' => 'game_start', 'players' => @players }
    end

    def notify_new_game
        new_game = gen_new_game
        @room_members.each {|x| x.trigger 'game_start', new_game }

        board_change = gen_board_change
        @room_members.each {|x| x.trigger 'board_changed', board_change }

        robber_moved = gen_robber_moved @game_manager.robber_pos
        @room_members.each {|x| x.trigger 'robber_moved', robber_moved }

        notify_object_change
    end

    def gen_diceroll(dice1, dice2)
        { 'command' => 'diceroll', 'dice' => [dice1, dice2] }
    end

    def notify_diceroll(dice1, dice2)
        diceroll = gen_diceroll dice1, dice2
        @room_members.each {|x| x.trigger 'diceroll', diceroll }
    end

    def gen_robber_moved(hex_id)
        { 'command' => 'robber_moved', 'hex_id' => hex_id }
    end

    def notify_robber_moved(hex_id)
        c = gen_robber_moved hex_id
        @room_members.each {|x| x.trigger 'robber_moved', c }
    end

    def gen_system_log(log)
        { 'command' => 'system_log', 'log' => log }
    end

    def send_system_log(log)
        c = gen_system_log log
        @room_members.each {|x|
            x.trigger 'system_log', c
        }
    end

    def gen_victory(player_id)
        { 'command' => 'victory', 'player_id' => player_id }
    end

    def notify_victory(player_id)
        c = gen_victory player_id
        @room_members.each {|x| x.trigger 'victory', c }
    end

    def notify_shortage(kind)
        c = { 'command' => 'shortage', 'kind' => kind }
        @room_members.each {|x| x.trigger 'shortage', c }
    end

    def notify_consume(player_id, kind)
        c = { 'command' => 'consume', 'player_id' => player_id, 'kind' => kind }
        @room_members.each {|x| x.trigger 'consume', c }
    end

    def notify_consume_with_info(player_id, kind, info)
        d = { 'command' => 'consume',
              'player_id' => player_id,
              'kind' => kind }
        d = info.merge d
        @room_members.each {|x| x.trigger 'consume', d }
    end

    def notify_trade_oversea(player_id, exs, ins)
        c = { 'command' => 'trade_oversea',
                              'player_id' => player_id,
                              'exs' => exs,
                              'ins' => ins }
        @room_members.each {|x| x.trigger 'trade_oversea', c }
    end

    def notify_trade_suggestion(player_id, partner_id, exs, ins)
        c = { 'command' => 'trade_suggestion',
                              'player_id' => player_id,
                              'partner_id' => partner_id,
                              'exs' => exs,
                              'ins' => ins }
        @room_members.each {|x| x.trigger 'trade_suggestion', c }
    end

    def notify_robbery(player_id, target_id)
        c = { 'command' => 'robbery',
                             'player_id' => player_id,
                             'target_id' => target_id }
        @room_members.each {|x| x.trigger 'robbery', c }
    end

    def notify_dispose(player_id, amounts)
        c = { 'command' => 'dispose',
                             'player_id' => player_id,
                             'amounts' => amounts }
        @room_members.each {|x| x.trigger 'dispose', c }
    end

    def notify_longest_road_changed(player_id)
        c = { 'command' => 'longest_road_changed',
                             'player_id' => player_id }
        @room_members.each {|x| x.trigger 'longest_road_changed', c }
    end

    def notify_largest_army_changed(player_id)
        c = { 'command' => 'largest_army_changed',
                             'player_id' => player_id }
        @room_members.each {|x| x.trigger 'largest_army_changed', c }
    end
end
