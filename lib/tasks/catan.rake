namespace :catan do
    desc 'For rule precalculation'

    task :default => ['lib/catan/geometry_info.json',
                      'app/assets/geometry_info.json']

    file 'lib/catan/geometry_info.json' => 'lib/catan/board_geometry.rb' do |t|
        sh "ruby lib/catan/board_geometry.rb -o #{t.name}"
    end

    file 'app/assets/geometry_info.json' => 'lib/catan/board_geometry.rb' do |t|
        sh "ruby lib/catan/board_geometry.rb -o #{t.name}"
    end
end
