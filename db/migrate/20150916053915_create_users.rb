class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      # Authentication
      t.string :provider
      t.string :uid

      # User information
      t.string :name
      t.string :session_id
      t.string :user_id
      t.boolean :activated

      t.timestamps null: false
    end
  end
end
