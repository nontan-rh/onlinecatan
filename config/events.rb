WebsocketRails::EventMap.describe do
  # You can use this file to map incoming events to controller actions.
  # One event can be mapped to any number of controller actions. The
  # actions will be executed in the order they were subscribed.
  #
  # Uncomment and edit the next line to handle the client connected event:
  #   subscribe :client_connected, :to => Controller, :with_method => :method_name
  #
  # Here is an example of mapping namespaced events:
  #   namespace :product do
  #     subscribe :new, :to => ProductController, :with_method => :new_product
  #   end
  # The above will handle an event triggered on the client like `product.new`.

  subscribe :join, to: CatanRoomController, with_method: :join_receive
  subscribe :command, to: CatanRoomController, with_method: :command_receive
  subscribe :chat, to: CatanRoomController, with_method: :chat_receive

  subscribe :change_player_list, to: CatanRoomController, with_method: :player_list_changed

  subscribe :client_disconnected, to: CatanRoomController, with_method: :disconnected
  subscribe :connection_closed, to: CatanRoomController, with_method: :disconnected
  subscribe :game_start, to: CatanRoomController, with_method: :start_button_clicked
  subscribe :transfer_authority, to: CatanRoomController, with_method: :transfer_authority
  subscribe :dice_1d100, to: CatanRoomController, with_method: :dice_1d100
end

WebsocketRails.setup do |config|
    config.user_identifier = :session_id
end
