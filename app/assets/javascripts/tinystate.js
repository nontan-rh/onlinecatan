var TinyState = null;

(function() {
  'use strict';

  // 'inherits' function from Node.js
  function inherits(ctor, superCtor) {
    ctor.super_ = superCtor;
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  }

  // Supporing functions
  function convert_action(x, def) {
    if (x == null || 'undefined' == typeof x) {
      return def;
    }
    return x;
  }

  function convert_args(x) {
    if (x == null || 'undefined' == typeof x) {
      return {};
    }
    return x;
  }

  // State controller
  TinyState = function(info, init_state, init_args) {
    this.info = info;
    this.state = init_state;
    this.state_parameters = convert_args(init_args);
    this.null_action = null_action;
  };

  // Public API
  TinyState.prototype.transit = function(to, args) {
    var new_sp = convert_args(args);
    this.execute(
            this.get_leave(this.state),
            this.get_move(this.state, to),
            this.get_reach(to), this.state_parameters, new_sp, this.state, to);
    this.state = to;
    this.state_parameters = new_sp;
  };

  TinyState.prototype.check_state = function(state) {
    if (state instanceof String) {
      throw new TypeError('State name must be string');
    }
    if (!this.info.hasOwnProperty(state)) {
      throw new NoStateError(state);
    }
  };

  TinyState.prototype.get_leave = function(from) {
    this.check_state(from);
    return convert_action(this.info[from].leave, this.null_action);
  };

  TinyState.prototype.get_move = function(from, to) {
    this.check_state(from);
    this.check_state(to);

    var trans_state = from + '->' + to;
    return convert_action(this.info[trans_state], this.null_action);
  };

  TinyState.prototype.get_reach = function(to) {
    this.check_state(to);

    return convert_action(this.info[to].reach, this.null_action);
  };

  // If you want to customize the behaviour, override this method
  TinyState.prototype.execute = function(leave, move, reach, old_sp, new_sp) {
    leave.execute(old_sp);
    move.execute(old_sp, new_sp);
    reach.execute(new_sp);
  };

  // Actions
  function Action() { };

  Action.prototype.execute = function() {};

  // Null action value
  var null_action = new Action();
  null_action._ = 'null_action';

  // ClosureAction
  function ClosureAction(closure) {
    Action.call(this);
    this.closure = closure;
  }

  inherits(ClosureAction, Action);

  ClosureAction.prototype.execute = function() {
    this.closure.apply(this, arguments);
  };

  // StateChangeAction
  function StateChangeAction(child_state, child_to,
      before_action, after_action) {
    Action.call(this);
    this.child_state = child_state;
    this.child_to = child_to;
    this.before_action = convert_action(before_action, this.null_action);
    this.after_action = convert_action(after_action, this.null_action);
  };

  inherits(StateChangeAction, Action);

  StateChangeAction.prototype.execute = function() {
    this.before_action.execute();
    this.child_state.transit(this.child_to);
    this.after_action.execute();
  };

  // Errors
  function NoStateError(state) {
    Error.call(this, 'There is no state named ' + '"' + state + '"');
  };

  inherits(NoStateError, Error);

  // Helpers
  function make_action(x) {
    if (x instanceof Function) {
      return new ClosureAction(x);
    } else if (x instanceof TinyState) {
      return new StateChangeAction(
          x, arguments[1], arguments[2], arguments[3]);
    } else {
      throw new Error('Invalid type');
    }
  }

  // Dispatch
  TinyState.Action = Action;
  TinyState.ClosureAction = ClosureAction;
  TinyState.StateChangeAction = StateChangeAction;
  TinyState.NoStateError = NoStateError;

  TinyState.make_action = make_action;
})();

