//= require tinystate
'use strict';

// 'inherits' function from Node.js
function inherits(ctor, superCtor) {
  ctor.super_ = superCtor;
  ctor.prototype = Object.create(superCtor.prototype, {
    constructor: {
      value: ctor,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
}

// Widget manager

function CanvasUI(canvas, num_layers, animation_interval, bg_color) {
  var that = this;

  this.num_layers = num_layers;

  this.canvas = canvas;
  this.object_layer = [];
  for (var i = 0; i < num_layers; i++) {
    this.object_layer.push([]);
  }

  this.animations = [];
  this.animations_started_time = [];
  this.animation_clock = 0;
  this.animation_interval = animation_interval;

  this.just_stopped_animation_layer = [];
  this.reset_just_stopped_animation_layer();

  this.waiting_animations = [];
  this.action_list_queues = [];
  this.action_list_arg = null;
  this.action_list_timer_obj = setInterval(
      function() { that.proceed_action_list(); that.tick(); },
      this.animation_interval);

  this.bg_color = bg_color;

  $(canvas).click(function(evt) { that.click_event(evt); });
  $(canvas).mousemove(function(evt) { that.mousemove_event(evt); });
  $(canvas).mousedown(function(evt) { that.mousedown_event(evt); });
  $(canvas).mouseup(function(evt) { that.mouseup_event(evt); });
}

CanvasUI.prototype.reset_just_stopped_animation_layer = function() {
  this.just_stopped_animation_layer.forEach(function(x) {
    x.forEach(function(y) {
            y.working = false;
    });
  });

  this.just_stopped_animation_layer = [];
  for (var i = 0; i < this.num_layers; i++) {
    this.just_stopped_animation_layer.push([]);
  }
};

CanvasUI.prototype.add_object = function(obj, level) {
  this.object_layer[level].unshift(obj);
  obj._level = level;
  obj.set_parent_canvas_ui(this);
  this.redraw();
};

CanvasUI.prototype.start_animation = function(anime) {
  for (var i = 0; i < this.animations.length; i++) {
    if (this.animations[i] === anime) {
      //throw "The animation is already working.";
      console.warn('The animation is already working');
      return;
    }
  }

  this.animations.push(anime);
  this.animations_started_time.push(this.animation_clock);
};

CanvasUI.prototype.stop_animation = function(anime) {
  for (var i = 0; i < this.animations.length; i++) {
    if (this.animations[i] === anime) {
      this.animations.splice(i, 1);
      this.animations_started_time.splice(i, 1);

      this.just_stopped_animation_layer[anime._level].push(anime);

      if (this.animations.length === 0) {
        //clearInterval(this.animation_timer_obj);
        //this.animation_timer_obj = null;
      }

      for (var i = 0; i < this.waiting_animations.length; i++) {
        if (this.waiting_animations[i] === anime) {
          this.waiting_animations[i] = null;
          this.proceed_action_list();
        }
      }
      return;
    }
  }
  throw 'The animation is not working.';
};

CanvasUI.prototype.tick = function() {
  this.animation_clock += this.animation_interval;
  for (var i = 0; i < this.animations.length; i++) {
    this.animations[i].tick(this.animation_clock - this.animations_started_time[i]);
  }

  this.proceed_action_list();
  this.redraw();

  this.reset_just_stopped_animation_layer();
};

CanvasUI.prototype.redraw = function() {
  var ctx = this.canvas.getContext('2d');
  // console.time("CanvasUI.prototype.redraw");
  if (this.bg_color !== 'clear') {
    ctx.fillStyle = this.bg_color;
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
  } else {
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
  for (var i = 0; i < this.object_layer.length; i++) {
    var x = this.object_layer[i];
    x.forEach(function(y) {
      if (y.visible) {
                y.redraw(ctx);
      }
    });
    var a = this.just_stopped_animation_layer[i];
    a.forEach(function(y) {
      if (y.visible) {
                y.redraw(ctx);
      }
    });
  }
  // console.timeEnd("CanvasUI.prototype.redraw");
};

CanvasUI.prototype.click_event = function(evt) {
  for (var i = this.object_layer.length - 1; i >= 0; i--) {
    var layer = this.object_layer[i];
    for (var j = 0; j < layer.length; j++) {
      if (layer[j].active) {
        var accepted = layer[j].click_event(evt);
        if (accepted) { return; }
      }
    }
  }
};

CanvasUI.prototype.mousemove_event = function(evt) {
  var acc_in_upper = false;
  for (var i = 0; i < 3; i++) {
    this.object_layer.forEach(function(x) {
      var acc_in_curr = false;
      x.forEach(function(y) {
              if (y.active) {
                  acc_in_curr = y.mousemove_event(evt, acc_in_upper, i) || acc_in_curr;
              } else {
                acc_in_curr = y.mousemove_inactive_event(evt, acc_in_upper, i) || acc_in_curr;
              }
      });
      acc_in_upper = acc_in_curr || acc_in_upper;
    });
  }
};

CanvasUI.prototype.mousedown_event = function(evt) {
  var acc_in_upper = false;
  this.object_layer.forEach(function(x) {
    var acc_in_curr = false;
    x.forEach(function(y) {
            if (y.active) {
                acc_in_curr = y.mousedown_event(evt, acc_in_upper) || acc_in_curr;
            }
    });
    acc_in_upper = acc_in_curr || acc_in_upper;
  });
};

CanvasUI.prototype.mouseup_event = function(evt) {
  var acc_in_upper = false;
  this.object_layer.forEach(function(x) {
    var acc_in_curr = false;
    x.forEach(function(y) {
            if (y.active) {
                acc_in_curr = y.mouseup_event(evt, acc_in_upper) || acc_in_curr;
            }
    });
    acc_in_upper = acc_in_curr || acc_in_upper;
  });
};

CanvasUI.prototype.get_offset = function() {
  return $(this.canvas).offset();
};

CanvasUI.prototype.proceed_action_list = function() {
  var d = [];
  var q = this.action_list_queues;
  var w = this.waiting_animations;

  for (var i = 0; i < q.length; i++) {
    if (q[i].length === 0 && w[i] === null) {
      d.unshift(i);
    }
  }

  d.forEach(function(i) {
    q.splice(i, 1);
    w.splice(i, 1);
  });

  for (var i = 0; i < q.length; i++) {
    if (w[i] !== null) {
      // Waiting the animation finishing
      continue;
    }

    while (q[i].length !== 0) {
      var a = q[i][0];
      q[i].shift();

      if (a instanceof CanvasAnimation) {
        w[i] = a;
        a.start();
        break;
      } else if (a instanceof Function) {
        this.action_list_arg = a(this.action_list_arg);
      } else if (a instanceof Array) {
        if (a.length !== 0) {
          w[i] = a.reduce(
              function(x, y) { return x.duration > y.duration ? x : y; },
              a[0]);
          a.forEach(function(x) { x.start(); });
        }
        break;
      } else {
        throw 'Illegal object for action list';
      }
    }
  }
};

CanvasUI.prototype.do_action_list = function(action_list) {
  this.action_list_queues.unshift(action_list);
  this.waiting_animations.unshift(null);
};

// Null widget

function CanvasWidget() {
  this.parent_canvas_ui = null;
  this.visible = true;
  this.active = true;
}

CanvasWidget.prototype.set_parent_canvas_ui = function(ui) {
  this.parent_canvas_ui = ui;
};

CanvasWidget.prototype.redraw = function(ctx) { };

CanvasWidget.prototype.click_event = function(evt) { };

CanvasWidget.prototype.mousemove_event = function(evt) { };

CanvasWidget.prototype.mousemove_inactive_event = function(evt) { };

CanvasWidget.prototype.mouseup_event = function(evt) { };

CanvasWidget.prototype.mousedown_event = function(evt) { };

CanvasWidget.prototype.set_visibility = function(visible) {
  this.visible = visible;
};

CanvasWidget.prototype.set_activeness = function(active) {
  this.active = active;
};

CanvasWidget.prototype.appear = function() {
  this.set_visibility(true);
  this.set_activeness(true);
};

CanvasWidget.prototype.disappear = function() {
  this.set_visibility(false);
  this.set_activeness(false);
};

// Button widget

function CanvasButton(x, y, width, height, img_off, img_on, mask_img) {
  CanvasWidget.call(this);

  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.img_off = img_off;
  this.img_on = img_on;

  this.mask_canvas = null;

  this.parent_canvas_ui = null;

  this.handler = null;
  this.mouse_off_handler = [];
  this.mouse_on_handler = [];
  this.mouse_off_inactive_handler = [];
  this.mouse_on_inactive_handler = [];
  this.mouse_on = false;

  this.clicked_x = null;
  this.clicked_y = null;

  this.mousedown_img = null;
  this.mousedown = false;

  this.inactive_img = null;

  if (mask_img !== null) {
    this.mask_canvas = document.createElement('canvas');
    this.mask_canvas.width = width;
    this.mask_canvas.height = height;

    var ctx = this.mask_canvas.getContext('2d');
    ctx.drawImage(mask_img, 0, 0, width, height);
  }
}

inherits(CanvasButton, CanvasWidget);

CanvasButton.prototype.set_mousedown_img = function(img) {
  this.mousedown_img = img;
};

CanvasButton.prototype.redraw = function(ctx) {
  if (!this.active && this.inactive_img !== null) {
    ctx.drawImage(this.inactive_img, this.x, this.y);
  } else if (this.mousedown && this.mousedown_img !== null) {
    ctx.drawImage(this.mousedown_img, this.x, this.y);
  } else if (this.mouse_on) {
    ctx.drawImage(this.img_on, this.x, this.y);
  } else {
    ctx.drawImage(this.img_off, this.x, this.y);
  }
};

CanvasButton.prototype.click_event = function(evt) {
  var canvas_offset = this.parent_canvas_ui.get_offset();
  var x = evt.pageX - (canvas_offset.left + this.x);
  var y = evt.pageY - (canvas_offset.top + this.y);

  this.clicked_x = x;
  this.clicked_y = y;

  if (!(0 <= x && x < this.width &&
      0 <= y && y < this.height)) {
    if (this.external_click_handler) {
      this.external_click_handler(evt);
    }
    return false;
  }

  if (this.mask_canvas === null) {
    if (this.handler !== null) {
      this.handler(evt);
    }
    return true;
  }

  var ctx = this.mask_canvas.getContext('2d');
  var pixel_color = ctx.getImageData(x, y, 1, 1).data;
  if (pixel_color[0] === 0 && pixel_color[1] === 0 && pixel_color[2] === 0) {
    if (this.handler !== null) {
      this.handler(evt);
    }
    return true;
  }
  return false;
};

CanvasButton.prototype.mousemove_event = function(evt, acc_in_upper, cnt) {
  var canvas_offset = this.parent_canvas_ui.get_offset();
  var x = evt.pageX - (canvas_offset.left + this.x);
  var y = evt.pageY - (canvas_offset.top + this.y);

  var mouse_on_now = false;
  if (!(0 <= x && x < this.width &&
      0 <= y && y < this.height)) {
    mouse_on_now = false;
    if (this.mousedown) {
      this.mousedown = false;
      this.parent_canvas_ui.redraw();
    }
  } else if (this.mask_canvas === null) {
    mouse_on_now = true;
  } else {
    var ctx = this.mask_canvas.getContext('2d');
    var pixel_color = ctx.getImageData(x, y, 1, 1).data;
    if (pixel_color[0] === 0 && pixel_color[1] === 0 && pixel_color[2] === 0) {
      mouse_on_now = true;
    }
  }

  if (this.mouse_on === false && mouse_on_now === true && cnt === 1) {
    this.mouse_on = true;
    this.parent_canvas_ui.redraw();
    this.mouse_on_handler.forEach(function(x) { x.call(this); });
  } else if (this.mouse_on === true && mouse_on_now === false && cnt === 0) {
    this.mouse_on = false;
    this.parent_canvas_ui.redraw();
    this.mouse_off_handler.forEach(function(x) { x.call(this); });
  }
};

CanvasButton.prototype.mousemove_inactive_event = function(evt, acc_in_upper, cnt) {
  var canvas_offset = this.parent_canvas_ui.get_offset();
  var x = evt.pageX - (canvas_offset.left + this.x);
  var y = evt.pageY - (canvas_offset.top + this.y);

  var mouse_on_now = false;
  if (!(0 <= x && x < this.width &&
      0 <= y && y < this.height)) {
    mouse_on_now = false;
    if (this.mousedown) {
      this.mousedown = false;
      this.parent_canvas_ui.redraw();
    }
  } else if (this.mask_canvas === null) {
    mouse_on_now = true;
  } else {
    var ctx = this.mask_canvas.getContext('2d');
    var pixel_color = ctx.getImageData(x, y, 1, 1).data;
    if (pixel_color[0] === 0 && pixel_color[1] === 0 && pixel_color[2] === 0) {
      mouse_on_now = true;
    }
  }

  if (this.mouse_on === false && mouse_on_now === true && cnt === 1) {
    this.mouse_on = true;
    this.parent_canvas_ui.redraw();
    this.mouse_on_inactive_handler.forEach(function(x) { x.call(this); });
  } else if (this.mouse_on === true && mouse_on_now === false && cnt === 0) {
    this.mouse_on = false;
    this.parent_canvas_ui.redraw();
    this.mouse_off_inactive_handler.forEach(function(x) { x.call(this); });
  }
};

CanvasButton.prototype.mousedown_event = function(evt) {
  var canvas_offset = this.parent_canvas_ui.get_offset();
  var x = evt.pageX - (canvas_offset.left + this.x);
  var y = evt.pageY - (canvas_offset.top + this.y);

  if (0 <= x && x < this.width && 0 <= y && y < this.height) {
    this.mousedown = true;
    this.parent_canvas_ui.redraw();
  }
};

CanvasButton.prototype.mouseup_event = function(evt) {
  if (this.mousedown === true) {
    this.mousedown = false;
    this.parent_canvas_ui.redraw();
  }
};

CanvasButton.prototype.set_handler = function(handler) {
  this.handler = handler;
};

CanvasButton.prototype.set_external_click_handler = function(handler) {
  this.external_click_handler = handler;
};

CanvasButton.prototype.set_mouse_off_handler = function(handler) {
  this.mouse_off_handler.push(handler);
};

CanvasButton.prototype.set_mouse_on_handler = function(handler) {
  this.mouse_on_handler.push(handler);
};

CanvasButton.prototype.set_mouse_on_inactive_handler = function(handler) {
  this.mouse_on_inactive_handler.push(handler);
};

CanvasButton.prototype.set_mouse_off_inactive_handler = function(handler) {
  this.mouse_off_inactive_handler.push(handler);
};

CanvasButton.prototype.set_visibility = function(visible) {
  this.visible = visible;
};

CanvasButton.prototype.set_activeness = function(active) {
  this.active = active;
};

// Button container
function Container(x, y, width, height, children) {
  CanvasButton.call(this, x, y, width, height, null, null, null);
  this.children = children;
  this.set_visibility(false);
  this.set_activeness(false);
}

inherits(Container, CanvasButton);

Container.prototype.appear = function() {
  this.set_activeness(true);
  this.children.forEach(function(x) {
    x.appear();
  });
};

Container.prototype.disappear = function() {
  this.set_activeness(false);
  this.children.forEach(function(x) {
    x.disappear();
  });
};

// Image transformation for Animations

function ImageTransformation(easing_fn) {
  this.easing_fn = easing_fn;
  this.image = null;
  this.width = 0;
  this.height = 0;
}

ImageTransformation.prototype.update_ = function(r, elapsed_time) {
  if (r === 0) {
    this.reset();
  }

  this.update(this.easing_fn(r), elapsed_time);
};

ImageTransformation.prototype.update = function(ratio, elapsed_time) {
};

ImageTransformation.prototype.reset = function() {
};

ImageTransformation.prototype.get_image = function() {
  return this.image;
};

function get_image(i) {
  if (i instanceof ImageTransformation) {
    return i.get_image();
  }

  return i;
}

// From MDN
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function SwitchImageRandom(images, period) {
  ImageTransformation.call(this, easing_linear);

  this.images = images;
  this.period = period;
  this.last_update = 0;
  this.last_index = 0;

  this.switch_start = 0;
  this.switch_end = Infinity;

  this.image = images[0];
  this.width = images[0].width;
  this.height = images[0].height;
}

inherits(SwitchImageRandom, ImageTransformation);

SwitchImageRandom.prototype.update = function(ratio, elapsed_time) {
  if (elapsed_time < this.switch_start || this.switch_end < elapsed_time) {
    return;
  }

  if (elapsed_time - this.last_update <= this.period) {
    return;
  }

  var index = null;
  do {
    index = getRandomInt(0, this.images.length - 1);
  } while (index === this.last_index);

  this.image = this.images[index];

  this.last_update = elapsed_time;
  this.last_index = index;
};

SwitchImageRandom.prototype.reset = function() {
  this.last_update = 0;
  this.last_index = 0;
};

SwitchImageRandom.prototype.set_start_time = function(s) {
  this.switch_start = s;
};

SwitchImageRandom.prototype.set_end_time = function(e) {
  this.switch_end = e;
};

// Animation widget

function CanvasAnimation(is_reversed, easing_fn, duration, image) {
  CanvasWidget.call(this);

  this.is_reversed = is_reversed;
  this.easing_fn = easing_fn;
  this.duration = duration;

  this.working = false;
  this.on_end_handler = null;

  this.image = image;
}

inherits(CanvasAnimation, CanvasWidget);

CanvasAnimation.prototype.start = function() {
  this.tick(0);
  this.parent_canvas_ui.start_animation(this);
  this.working = true;
};

CanvasAnimation.prototype.stop = function() {
  this.parent_canvas_ui.stop_animation(this);
  //this.working = false;
};

CanvasAnimation.prototype.tick = function(elapsed_time) {
  if (elapsed_time > this.duration) {
    this.stop();
    if (this.on_end_handler !== null) {
      this.on_end_handler();
    }
    return;
  }

  var r = elapsed_time / this.duration;
  if (this.image instanceof ImageTransformation) {
    this.image.update_(r, elapsed_time);
  }

  var ratio = this.easing_fn(this.is_reversed ? (1 - r) : r);
  this.update(ratio, elapsed_time);
};

CanvasAnimation.prototype.set_on_end_handler = function(handler) {
  this.on_end_handler = handler;
};

CanvasAnimation.prototype.get_image = function(handler) {
  return get_image(this.image);
};

// Straight line

function StraightLineAnimation(start_x, start_y, end_x, end_y, image, easing_fn, duration) {
  CanvasAnimation.call(this, false, easing_fn, duration, image);

  this.start_x = start_x;
  this.start_y = start_y;
  this.end_x = end_x;
  this.end_y = end_y;
  this.width = image.width;
  this.height = image.height;

  this.pos_x = null;
  this.pos_y = null;
}

inherits(StraightLineAnimation, CanvasAnimation);

StraightLineAnimation.prototype.update = function(ratio) {
  this.pos_x = this.start_x + (this.end_x - this.start_x) * ratio;
  this.pos_y = this.start_y + (this.end_y - this.start_y) * ratio;
};

StraightLineAnimation.prototype.redraw = function(ctx) {
  if (!this.working) {
    return;
  }

  ctx.drawImage(this.get_image(), this.pos_x, this.pos_y, this.width, this.height);
};

// Revolution

function RevolutionAnimation(rev_center_x, rev_center_y, radius,
                             start_pos_angle, end_pos_angle,
                             start_rotate_angle, end_rotate_angle,
                             image, img_center_x, img_center_y,
                             easing_fn, duration) {
  CanvasAnimation.call(this, false, easing_fn, duration, image);

  this.rev_center_x = rev_center_x;
  this.rev_center_y = rev_center_y;
  this.radius = radius;
  this.start_pos_angle = start_pos_angle;
  this.end_pos_angle = end_pos_angle;
  this.start_rotate_angle = start_rotate_angle;
  this.end_rotate_angle = end_rotate_angle;
  this.img_width = image.width;
  this.img_height = image.height;
  this.img_center_x = img_center_x;
  this.img_center_y = img_center_y;

  this.pos_x = null;
  this.pos_y = null;
  this.rotate_angle = null;
}

inherits(RevolutionAnimation, CanvasAnimation);

RevolutionAnimation.prototype.update = function(ratio) {
  var pos_angle = this.start_pos_angle +
      (this.end_pos_angle - this.start_pos_angle) * ratio;

  this.pos_x = this.rev_center_x + this.radius * Math.cos(pos_angle);
  this.pos_y = this.rev_center_y + this.radius * Math.sin(pos_angle);
  this.rotate_angle = this.start_rotate_angle +
      (this.end_rotate_angle - this.start_rotate_angle) * ratio;
};

RevolutionAnimation.prototype.redraw = function(ctx) {
  if (!this.working) {
    return;
  }

  ctx.translate(this.pos_x, this.pos_y);
  ctx.rotate(this.rotate_angle);

  ctx.drawImage(this.get_image(), -this.img_center_x, -this.img_center_y,
      this.img_width, this.img_height);

  ctx.setTransform(1, 0, 0, 1, 0, 0);
};

// Fade in/out

function FadeAnimation(pos_x, pos_y, start_opacity, end_opacity, image_from, image_to, easing_fn, duration) {
  CanvasAnimation.call(this, false, easing_fn, duration, null);

  this.pos_x = pos_x;
  this.pos_y = pos_y;
  this.start_opacity = start_opacity;
  this.end_opacity = end_opacity;
  this.image_from = image_from;
  this.image_to = image_to;
  this.width = image_from.width;
  this.height = image_from.height;

  if (image_to !== null) {
    this.offscreen_canvas = document.createElement('canvas');
    this.offscreen_canvas.width = this.width;
    this.offscreen_canvas.height = this.height;
  }

  this.opacity = null;
}

inherits(FadeAnimation, CanvasAnimation);

FadeAnimation.prototype.update = function(ratio) {
  this.opacity = this.start_opacity + (this.end_opacity - this.start_opacity) * ratio;
};

FadeAnimation.prototype.redraw = function(ctx) {
  if (!this.working) {
    return;
  }

  if (this.image_to === null) {
    var saved_alpha = ctx.globalAlpha;
    ctx.globalAlpha = this.opacity;
    ctx.drawImage(this.image_from, this.pos_x, this.pos_y, this.width, this.height);
    ctx.globalAlpha = saved_alpha;
  } else {
    var offctx = this.offscreen_canvas.getContext('2d');
    offctx.fillStyle = 'rgb(255,255,255)';
    offctx.fillRect(0, 0, this.width, this.height);
    offctx.globalAlpha = 1;
    offctx.drawImage(this.image_to, 0, 0, this.width, this.height);
    offctx.globalAlpha = this.opacity;
    offctx.drawImage(this.image_from, 0, 0, this.width, this.height);

    ctx.drawImage(this.offscreen_canvas, this.pos_x, this.pos_y, this.width, this.height);
  }
};

// Bloom

function BloomAnimation(pos_x, pos_y, image, is_reversed, easing_fn, duration) {
  CanvasAnimation.call(this, is_reversed, easing_fn, duration, image);

  this.pos_x = pos_x;
  this.pos_y = pos_y;
  this.width = image.width;
  this.height = image.height;
  this.center_x = pos_x + this.width / 2;
  this.center_y = pos_y + this.height / 2;

  this.ratio = 0;

  this.on_end_handler = null;

  this.parent_canvas_ui = null;
}

inherits(BloomAnimation, CanvasAnimation);

BloomAnimation.prototype.update = function(ratio) {
  this.ratio = ratio;
};

BloomAnimation.prototype.redraw = function(ctx) {
  if (!this.working) {
    return;
  }

  var w = this.width * this.ratio;
  var h = this.height * this.ratio;
  var x = this.center_x - w / 2;
  var y = this.center_y - h / 2;

  ctx.drawImage(this.get_image(), x, y, w, h);
};

function RectangleExpandAnimation(image, st, sb, sl, sr, et, eb, el, er, easing_fn, duration) {
  CanvasAnimation.call(this, false, easing_fn, duration, image);

  this.s = [st, sb, sl, sr];
  this.e = [et, eb, el, er];
  this.pos = [0, 0, 0, 0];
}

inherits(RectangleExpandAnimation, CanvasAnimation);

RectangleExpandAnimation.prototype.update = function(ratio) {
  for (var i = 0; i < 4; i++) {
    this.pos[i] = this.s[i] + (this.e[i] - this.s[i]) * ratio;
  }
};

RectangleExpandAnimation.prototype.redraw = function(ctx) {
  if (!this.working) {
    return;
  }

  var x = this.pos[2];
  var y = this.pos[0];
  var w = this.pos[3] - this.pos[2];
  var h = this.pos[1] - this.pos[0];

  ctx.drawImage(this.get_image(), x, y, w, h);
};

// Easing functions

function easing_linear(x) {
  return x;
}

function easing_easeOutCubic(x) {
  return 1 - Math.pow(1 - x, 3);
}

function easing_easeOutQuad(x) {
  return 1 - Math.pow(1 - x, 4);
}

function easing_easeOutQuart(x) {
  return 1 - Math.pow(1 - x, 5);
}

function easing_easeOutSine(x) {
  return Math.sin(x / 2 * Math.PI);
}

function easing_easeOutExpo(x) {
  return 1 - Math.pow(2, -5 * x);
}

function easing_easeInExpo(x) {
  return Math.pow(2, -5 * x);
}

// State for Animation

function BoundAction(actions) {
  TinyState.Action.call(this);
  this.actions = actions;
}

inherits(BoundAction, TinyState.Action);

BoundAction.prototype.get_action_list = function() {
  var bind_args = Array.prototype.slice.call(arguments);
  bind_args.unshift(this);

  function bind_if_function(a) {
    if (a instanceof Function) {
      var bound_function = Function.prototype.bind.apply(a, bind_args);
      return [bound_function];
    } else {
      return [a];
    }
  }

  var bounds = this.actions.map(bind_if_function);
  return Array.prototype.concat.apply([], bounds);
};

function LazyAction(fn) {
  TinyState.Action.call(this);
  this.fn = fn;
}

inherits(LazyAction, TinyState.Action);

LazyAction.prototype.get_action_list = function(old_sp, new_sp) {
  return this.fn(old_sp, new_sp);
};

function AnimationState(canvas_ui, info, init_state) {
  TinyState.call(this, info, init_state);

  this.canvas_ui = canvas_ui;
  this.null_action = new BoundAction([]);
}

inherits(AnimationState, TinyState);

AnimationState.prototype.execute = function(leave, move, reach, old_sp, new_sp, from, to) {
  var a = leave.get_action_list(old_sp, new_sp, to).concat(
      move.get_action_list(old_sp, new_sp, from, to),
      reach.get_action_list(old_sp, new_sp, from));
  this.canvas_ui.do_action_list(a);
};

// Helper

function make_lazy_action(fn) {
  return new LazyAction(fn);
}

function make_bound_action() {
  return new BoundAction(Array.prototype.slice.call(arguments));
}

function button(kwargs) {
  var img = kwargs.imgOff;
  if (!img) {
    throw TypeError('No imgOff');
  }
  var w = _.get(img, 'width', 0);
  var h = _.get(img, 'height', 0);
  var cb = new CanvasButton(
      _.get(kwargs, 'x', 0),
      _.get(kwargs, 'y', 0),
      _.get(kwargs, 'width', w),
      _.get(kwargs, 'height', h),
      _.get(kwargs, 'imgOff', img),
      _.get(kwargs, 'imgOn', img),
      _.get(kwargs, 'imgMask', null)
      );
  cb.handler = _.get(kwargs, 'clickHandler', _.identity);
  cb.mouse_on_handler = [_.get(kwargs, 'mouseOnHandler', _.identity)];
  cb.mouse_off_handler = [_.get(kwargs, 'mouseOffHandler', _.identity)];
  cb.mousedown_img = _.get(kwargs, 'imgMouseDown', null);
  cb.inactive_img = _.get(kwargs, 'imgInactive', null);
  cb.visible = _.get(kwargs, 'visible', false);
  cb.active = _.get(kwargs, 'active', false);
  return cb;
}

function container(kwargs) {
  var cc = new Container(
      _.get(kwargs, 'x', 0),
      _.get(kwargs, 'y', 0),
      _.get(kwargs, 'width', 0),
      _.get(kwargs, 'height', 0),
      _.get(kwargs, 'children', 0));
  return cc;
}
