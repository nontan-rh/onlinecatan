
// Global configuration
var bg_color = 'rgb(41,171,226)';
var num_ui_layers = 10;
var ms_in_frame = 16;

var resource_order = ['lumber', 'brick', 'wool', 'grain', 'ore'];
var resource_color = ['rgb(36,114,0)', 'rgb(171,110,0)', 'rgb(111,211,14)', 'rgb(208,163,0)', 'rgb(153,161,163)'];

var chance_card_order = ['knight', 'victory_point', 'building_road', 'harvest', 'monopoly'];
var chance_card_button_order = ['knight', 'building_road', 'harvest', 'monopoly'];

// Deck info configuration
var di_bg_color = 'rgb(75,75,75)';
var deck_info_x = 0;
var deck_info_y = 0;
var deck_info_w = 960;
var deck_info_h = 128;
var deck_info_margin_top = 24;
var deck_info_margin_left = 120;
var deck_info_margin_mid = 32;
var deck_info_margin_top_chance_card = 32;
var deck_info_margin_mid_chance_card = 40;

// Player info configuration
var player_colors = ['rgb(194,58,48)',
                     'rgb(72,75,223)',
                     'rgb(175,143,0)',
                     'rgb(0,128,0)'];
var player_edge_color = 'rgb(255,255,255)';
var player_edge_width = 3;
var player_area_x = 712;
var player_area_y = 152;
var player_area_width = 960 - player_area_x;
var player_area_other_height = 72;
var player_area_me_height = 136;
var player_area_margin = 8;
var player_area_name_offset_x = 40;
var player_area_name_offset_y = 24;
var player_area_name_color = 'rgb(255,255,255)';
var player_area_name_font = '700 23px Roboto';
var player_area_victory_point_offset_x = 8;
var player_area_victory_point_offset_y = 8;
var player_area_victory_point_radius = 12;
var player_area_victory_point_bg = 'rgb(255,255,255)';
var player_area_victory_point_font = '900 13px Roboto';

var four_squares = [{x: 0, y: 0}, {x: 72, y: 0}, {x: 0, y: 72}, {x: 72, y: 72}];

// Stub game information
var game_info = {
  me: 0,
  turn: 0,
  name: '',
  input_state: 'disabled',
  input_state_any: false,
  player_list: [],
  robber_pos: 0,
  deck_info: {
    lumber: 2,
    brick: 2,
    wool: 2,
    ore: 2,
    grain: 2,
    chance_cards: 2
  },
  player_info: [
    { name: '',
      score: 0,
      resources: {
        lumber: 0,
        brick: 0,
        wool: 0,
        ore: 0,
        grain: 0
      },
      chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      used_chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      new_chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      special_cards: {
        longest_road: false,
        largest_army: false
      },
      ports: {
        versatile: false,
        lumber: false,
        wool: false,
        ore: false,
        grain: false
      }
    },
    { name: '',
      score: 0,
      resources: {
        lumber: 0,
        brick: 0,
        wool: 0,
        ore: 0,
        grain: 0
      },
      chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      used_chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      new_chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      special_cards: {
        longest_road: false,
        largest_army: false
      },
      ports: {
        versatile: false,
        lumber: false,
        wool: false,
        ore: false,
        grain: false
      }
    },
    { name: '',
      score: 0,
      resources: {
        lumber: 0,
        brick: 0,
        wool: 0,
        ore: 0,
        grain: 0
      },
      chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      used_chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      new_chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      special_cards: {
        longest_road: false,
        largest_army: false
      },
      ports: {
        versatile: false,
        lumber: false,
        wool: false,
        ore: false,
        grain: false
      }
    },
    { name: '',
      score: 0,
      resources: {
        lumber: 0,
        brick: 0,
        wool: 0,
        ore: 0,
        grain: 0
      },
      chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      used_chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      new_chance_cards: {
        knight: 0,
        building_road: 0,
        harvest: 0,
        monopoly: 0,
        victory_point: 0
      },
      special_cards: {
        longest_road: false,
        largest_army: false
      },
      ports: {
        versatile: false,
        lumber: false,
        wool: false,
        ore: false,
        grain: false
      }
    }
  ],
  board_info: [
    { kind: 'hill' , value: 2 },
    { kind: 'forest' , value: 3 },
    { kind: 'mountain' , value: 4 },
    { kind: 'field' , value: 5 },
    { kind: 'pasture' , value: 6 },
    { kind: 'hill' , value: 2 },
    { kind: 'forest' , value: 3 },
    { kind: 'mountain' , value: 4 },
    { kind: 'field' , value: 5 },
    { kind: 'pasture' , value: 6 },
    { kind: 'hill' , value: 2 },
    { kind: 'forest' , value: 3 },
    { kind: 'mountain' , value: 4 },
    { kind: 'field' , value: 5 },
    { kind: 'pasture' , value: 6 },
    { kind: 'desert' , value: 7 },
    { kind: 'mountain' , value: 4 },
    { kind: 'field' , value: 5 },
    { kind: 'pasture' , value: 8 }
  ],
  object_info: []
};

for (var i = 0; i < 23; i++) {
  var a = new Array(18);
  for (var j = 0; j < 18; j++) {
    a[j] = 0;
  }
  game_info.object_info.push(a);
}

var consume_kind_e2j = {
  'house': '家',
  'road': '道',
  'city': '街',
  'card': 'チャンスカード',
  'knight': 'ソルジャー',
  'building_road': '街道',
  'harvest': '収穫',
  'monopoly': '独占',
};

var resource_kind_e2j = {
  'lumber': '木',
  'brick': '土',
  'wool': '羊',
  'grain': '麦',
  'ore': '鉄'
};

var develop_recipe = {
  'house' :
      {'lumber' : 1,
        'brick' : 1,
        'wool' : 1,
        'ore' : 0,
        'grain' : 1 },
  'road' :
      {'lumber' : 1,
        'brick' : 1,
        'wool' : 0,
        'ore' : 0,
        'grain' : 0 },
  'city' :
      {'lumber' : 0,
        'brick' : 0,
        'wool' : 0,
        'ore' : 3,
        'grain' : 2 },
  'card' :
      {'lumber' : 0,
        'brick' : 0,
        'wool' : 1,
        'ore' : 1,
        'grain' : 1 },
};

var global_hint = '';

function sprintf(format) {
  for (var i = 1; i < arguments.length; i++) {
    var r = new RegExp('\\{' + (i - 1) + '\\}');
    format = format.replace(r, arguments[i]);
  }
  return format;
}

_.mixin({
  filterIndex: function(collection, predicate) {
    predicate = predicate || function (x) { return x; };
    var result = [];
    if (_.isArray(collection)) {
      for (var i = 0; i < collection.length; i++) {
        if (predicate(collection[i])) {
          result.push(i);
        }
      }
    } else {
      throw new Error('filterIndex only accepts arrays');
    }
    return result;
  }
})

Math.sign = Math.sign || function(x) {
  x = +x; // convert to a number
  if (x === 0 || isNaN(x)) {
    return x;
  }
  return x > 0 ? 1 : -1;
};

function cutout_to_canvas(image, x, y, width, height) {
  var dest = document.createElement('canvas');
  dest.width = width;
  dest.height = height;

  var ctx = dest.getContext('2d');
  ctx.drawImage(image, x, y, width, height, 0, 0, width, height);

  return dest;
}

var setup_completed = false;

function go_with_setup(fn) {
  if (setup_completed) {
    fn();
  } else {
    setTimeout(function() { go_with_setup(fn); }, 500);
  }
}

$(function() {
  var canvas = document.getElementById('board');
  ui = new CanvasUI(canvas, num_ui_layers, ms_in_frame, bg_color);

  var overlay = document.getElementById('overlay');
  overlay_ui = new CanvasUI(overlay, 1, ms_in_frame, 'clear');

  function all_images_ready() {
    var flag = true;
    $('img').each(function(i, x) {
      if (!x.complete) {
        flag = false;
        return false;
      }
    });
    return flag;
  }

  function wait_all_images() {
    if (all_images_ready()) {
      console.log('ready!');
      setup_ui();
    } else {
      console.log('not ready...');
      setTimeout(wait_all_images, 500);
    }
  }

  ws_disp.bind('player_changed', on_player_changed);
  ws_disp.bind('board_changed', on_board_changed);
  ws_disp.bind('object_changed', on_object_changed);
  ws_disp.bind('waiting', on_waiting);
  ws_disp.bind('player_status_changed', on_player_changed);
  ws_disp.bind('game_start', on_game_start);
  ws_disp.bind('diceroll', on_diceroll);
  ws_disp.bind('robber_moved', on_robber_moved);
  ws_disp.bind('robbery', on_robbery);
  ws_disp.bind('trade_result', on_trade_result);
  ws_disp.bind('trade_suggestion', on_trade_suggestion);
  ws_disp.bind('trade_oversea', on_trade_oversea);
  ws_disp.bind('longest_road_changed', on_longest_road_changed);
  ws_disp.bind('largest_army_changed', on_largest_army_changed);
  ws_disp.bind('room_member_changed', on_room_member_changed);
  ws_disp.bind('victory', on_victory);
  ws_disp.bind('shortage', on_shortage);
  ws_disp.bind('consume', on_consume);
  ws_disp.bind('resource_deck_info', on_resource_deck_info);

  wait_all_images();
});

var ui, overlay_ui;
// Spinbox widget
var up_off, up_on, down_off, down_on;

var trade_form_player_imgs = [];
var trade_form_player_imgs1 = [];

var resource_deck_imgs = {};
var chance_card_deck_imgs = [];

var di;

var chance_card_imgs = [];

var dice_imgs = [];

var chance_used_imgs = [];

var chance_available_imgs = [];
var chance_used_order = ['knight', 'building_road', 'harvest', 'monopoly'];

var special_card_army_img;
var special_card_road_img;

var chance_other_used_imgs = [];

var land_number_img, land_edge_img;

var board_object_img;
var robber_pink_img, robber_blue_img;

var di, pi, bd;
var button_trade_img, button_consume_img, button_card_img, button_trade_kind_img;
var button_roll_img, button_end_img, button_card_kind_available_img;
var button_card_kind_unavailable_img;
var button_trade_player_img, button_trade_oversea_img;
var button_player_choice_imgs = [];
var button_player_choice_imgs1 = [];
var button_player_choice_whole_img;
var trade_player_choice_buttons = [];
var button_player_choice_img_src;
var trade_player_choice_button_in_anime, trade_player_choice_button_out_anime;
var trade_player_choice_button_next_animes = [];
var trade_player_choice_button_expand_animes = [];
var trade_form_player_out_animes = [];

var button_card_kind_available_imgs = [];
var button_card_kind_unavailable_imgs = [];
var card_kind_buttons = [];

var consume_kind_button_available_imgs = [];
var consume_kind_button_unavailable_imgs = [];

var roll_button, end_button, trade_button, consume_button, card_button;
var trade_kind_buttons_container, trade_kind_oversea_button;
var trade_kind_player_button, trade_player_choice_buttons_container;
var card_kind_button_container;
var consume_kind_button;
var consume_kind_house_button;
var consume_kind_road_button;
var consume_kind_city_button;
var consume_kind_chance_card_mouseover_anime;
var consume_kind_house_mouseover_anime;
var consume_kind_road_mouseover_anime;
var consume_kind_city_mouseover_anime;
var consume_kind_chance_card_button;
var consume_kind_buttons_container;

var roll_button_in_anime, roll_button_out_anime, end_button_in_anime, end_button_out_anime;
var trade_kind_button_in_anime, trade_kind_button_out_anime;
var consume_kind_button_in_anime, consume_kind_button_out_anime;
var card_kind_button_in_anime, card_kind_button_out_anime;

var trade_player_img, trade_oversea_img, trade_player_clear_img, trade_oversea_clear_img;
var trade_base_img, trade_base_clear_img, trade_form_decide_button_img;

var trade_oversea_char_disappear_anime, trade_player_disappear_anime;
var trade_oversea_expand_anime, trade_oversea_in_anime, trade_oversea_out_anime;
var trade_button_in_anime, trade_button_out_anime;
var consume_button_in_anime, consume_button_out_anime;
var card_button_in_anime, card_button_out_anime;

var tf;

var trade_form_players = [];

var monopoly_form;
var harvest_form;

var trade_button_accept_img, trade_button_decline_img, trade_button_accept_pushed_img, trade_button_decline_pushed_img;

var trade_confirm_form_in_anime, trade_confirm_form_out_anime;

var trade_confirm_form;

var burst_button_inactive_img, burst_button_mousedown_img;
var burst_form, burst_button_img;

var card_knight_button, card_road_button, card_harvest_button, card_monopoly_button;

var diceroll_switch_image1, diceroll_switch_image2;
var diceroll_switch_anime1, diceroll_switch_anime2;

var robber_screen_in_anime, robber_screen_out_anime;
var robber_screen;

var victory_screen_in_anime, victory_screen_out_anime;
var victory_screen;

var burst_screen_me_img;
var burst_screen_other_img;

var resource_amount_images = [];

var states = {};

function setup_ui() {
  game_info.name = $.cookie('name');

  var resource_amount_image_src = document.getElementById('resource_amount');
  for (var i = 0; i < 5; i++) {
    resource_amount_images.push(
      cutout_to_canvas(resource_amount_image_src, 16 + 32 * i, 16, 24, 56));
  }

  var spinbox_button_img = document.getElementById('spinbox_button');
  up_off = cutout_to_canvas(spinbox_button_img, 0, 0, 8, 8);
  up_on = cutout_to_canvas(spinbox_button_img, 8, 0, 8, 8);
  down_off = cutout_to_canvas(spinbox_button_img, 0, 8, 8, 8);
  down_on = cutout_to_canvas(spinbox_button_img, 8, 8, 8, 8);

  var trade_form_player_img_src = document.getElementById('trade_player_base');
  for (var i = 0; i < 4; i++) {
    var t = cutout_to_canvas(trade_form_player_img_src, 0, i * (168 + 8), 264, 168);
    trade_form_player_imgs.push(t);
    t = cutout_to_canvas(trade_form_player_img_src, 0, i * (168 + 8), 264, 168);
    trade_form_player_imgs1.push(t);
  }

  var deck_source = document.getElementById('icon_resource');
  for (var i = 0; i < 5; i++) {
    resource_deck_imgs[resource_order[i]] = [];
    for (var j = 0; j < 3; j++) {
      var x = x_left + i * (deck_width + deck_x_margin);
      var y = y_top + j * (deck_height + deck_y_margin);

      var img_canvas = document.createElement('canvas');
      img_canvas.width = deck_width;
      img_canvas.height = deck_height;

      var ctx = img_canvas.getContext('2d');
      ctx.drawImage(deck_source,
                x, y, deck_width, deck_height,
                0, 0, deck_width, deck_height);

      resource_deck_imgs[resource_order[i]].push(img_canvas);
    }
  }

  var chance_card_deck_source = document.getElementById('icon_chance');
  for (var i = 0; i < 3; i++) {
    var y = y_top + i * (chance_card_height + deck_y_margin);
    var img_canvas = document.createElement('canvas');
    img_canvas.width = chance_card_width;
    img_canvas.height = chance_card_height;

    var ctx = img_canvas.getContext('2d');
    ctx.drawImage(chance_card_deck_source,
            x_left, y, chance_card_width, chance_card_height,
            0, 0, chance_card_width, chance_card_height);

    chance_card_deck_imgs.push(img_canvas);
  }

  di = new DeckInfoArea();
  ui.add_object(di, 3);

  var chance_card_img_source = document.getElementById('icon_chance_card');
  chance_card_imgs.push(cutout_to_canvas(chance_card_img_source, 0, 0, 18, 18));
  chance_card_imgs.push(cutout_to_canvas(chance_card_img_source, 20, 0, 18, 18));
  chance_card_imgs.push(cutout_to_canvas(chance_card_img_source, 40, 0, 18, 18));
  chance_card_imgs.push(cutout_to_canvas(chance_card_img_source, 60, 0, 18, 18));
  chance_card_imgs.push(cutout_to_canvas(chance_card_img_source, 100, 0, 18, 18));

  var dice_img_src = document.getElementById('icon_dice');
  for (var i = 0; i < 6; i++) {
    dice_imgs.push(cutout_to_canvas(dice_img_src, 16 + (24 + 8) * i, 16, 24, 24));
  }

  var chance_used_img_src = document.getElementById('chance_used');
  for (var i = 0; i < 4; i++) {
    chance_used_imgs.push(cutout_to_canvas(chance_used_img_src, (28 + 2) * i, 0, 28, 18));
  }

  var chance_available_img_src = document.getElementById('chance_available');
  for (var i = 0; i < 5; i++) {
    chance_available_imgs.push(cutout_to_canvas(chance_available_img_src, (35 + 2) * i, 0, 35, 24));
  }

  var special_card_img_src = document.getElementById('special_card');
  special_card_army_img = cutout_to_canvas(special_card_img_src, 7, 7, 18, 18);
  special_card_road_img = cutout_to_canvas(special_card_img_src, 31, 7, 18, 18);

  var chance_other_used_img_src = document.getElementById('chance_other_used');
  for (var i = 0; i < 5; i++) {
    chance_other_used_imgs.push(cutout_to_canvas(chance_other_used_img_src, (18 + 2) * i, 0, 18, 34));
  }

  var board_cell_img_source = document.getElementById('icon_land');
  land_number_img = document.getElementById('font_land');
  land_edge_img = document.getElementById('land_edge');
  for (var i = 0; i < 6; i++) {
    var img = document.createElement('canvas');
    img.width = board_cell_width;
    img.height = board_cell_height;

    var ctx = img.getContext('2d');
    var x = x_left + (deck_x_margin + board_cell_width) * i;
    ctx.drawImage(board_cell_img_source,
            x, y_top, board_cell_width, board_cell_height,
            0, 0, board_cell_width, board_cell_height);

    board_cell_imgs[cell_order[i]] = img;
  }

  board_object_img = document.getElementById('board_object');

  var icon_robber_source = document.getElementById('icon_robber');
  robber_pink_img = cutout_to_canvas(icon_robber_source,
      16, 16, board_cell_width, board_cell_height);
  robber_blue_img = cutout_to_canvas(icon_robber_source,
      16 + board_cell_width + 8, 16, board_cell_width, board_cell_height);

  di = new DiceInfoArea();
  ui.add_object(di, 3);

  pi = new PlayerInfoArea();
  ui.add_object(pi, 3);

  bd = new Board();
  ui.add_object(bd, 3);

  var button_icon_img = document.getElementById('icon_button');
  button_trade_img = cutout_to_canvas(button_icon_img, 16, 16, 120, 64);
  button_consume_img = cutout_to_canvas(button_icon_img, 144, 16, 120, 64);
  button_card_img = cutout_to_canvas(button_icon_img, 272, 16, 120, 64);
  button_trade_kind_img = cutout_to_canvas(button_icon_img, 16, 88, 144, 144);
  button_roll_img = cutout_to_canvas(button_icon_img, 16, 240, 112, 112);
  button_end_img = cutout_to_canvas(button_icon_img, 136, 240, 112, 112);
  button_card_kind_available_img = cutout_to_canvas(button_icon_img, 472, 88, 144, 144);
  button_card_kind_unavailable_img = cutout_to_canvas(button_icon_img, 624, 88, 144, 144);

  button_trade_player_img = cutout_to_canvas(button_icon_img, 16, 88, 72, 144);
  button_trade_oversea_img = cutout_to_canvas(button_icon_img, 88, 88, 72, 144);

  button_player_choice_img_src = cutout_to_canvas(button_icon_img, 320, 88, 144, 192);

  button_player_choice_whole_img = document.createElement('canvas');
  button_player_choice_whole_img.width = 144;
  button_player_choice_whole_img.height = 144;

  for (var i = 0; i < 4; i++) {
    button_player_choice_imgs.push(
        cutout_to_canvas(button_player_choice_img_src, 0, i * 48, 144, 48));
    button_player_choice_imgs1.push(
        cutout_to_canvas(button_player_choice_img_src, 0, i * 48, 144, 48));
  }

  for (var i = 0; i < 4; i++) {
    var b = new CanvasButton(0, 0, 144, 48,
        button_player_choice_imgs1[i], button_player_choice_imgs1[i], null);
    b.set_visibility(false);
    b.set_activeness(false);
    set_button_description(b, sprintf("{0}と交換", game_info.player_info[i].name));

    // Extend!
    b.player_id = i;

    ui.add_object(b, 3);
    trade_player_choice_buttons[i] = b;
  }

  trade_player_choice_button_in_anime = new FadeAnimation(
      284, 544, 1, 0, button_trade_kind_img, button_player_choice_whole_img, easing_linear, 150);
  ui.add_object(trade_player_choice_button_in_anime, 3);

  trade_player_choice_button_out_anime = new BloomAnimation(284, 544,
      button_player_choice_whole_img, true, easing_linear, 100);
  ui.add_object(trade_player_choice_button_out_anime, 2);

  for (var i = 0; i < 4; i++) {
    var a = new FadeAnimation(284, 544, 1, 0,
        button_player_choice_imgs1[i], null, easing_linear, 150);
    ui.add_object(a, 2);
    trade_player_choice_button_next_animes.push(a);
  }

  for (var i = 0; i < 4; i++) {
    var a = new RectangleExpandAnimation(
        button_player_choice_imgs[i],
        0, 0, 0, 0,
        560, 704, 232, 495,
        easing_linear,
        150);
    ui.add_object(a, 3);
    trade_player_choice_button_expand_animes.push(a);
  }

  for (var i = 0; i < 4; i++) {
    var a = new BloomAnimation(
        232, 560, trade_form_player_imgs1[i], true, easing_linear, 150);
    ui.add_object(a, 3);
    trade_form_player_out_animes.push(a);
  }

  var button_card_kind_msgs = [
    '「騎士」を使用',
    '「街道」を使用',
    '「収穫」を使用',
    '「独占」を使用',
  ];
  for (var i = 0; i < 4; i++) {
    var p = four_squares[i];
    var a = cutout_to_canvas(button_card_kind_available_img, p.x, p.y, 72, 72);
    var u = cutout_to_canvas(button_card_kind_unavailable_img, p.x, p.y, 72, 72);
    button_card_kind_available_imgs.push(a);
    button_card_kind_unavailable_imgs.push(u);
    var b = new CanvasButton(620 + p.x, 544 + p.y,
        a.width, a.height, a, a, null);
    b.set_activeness(false);
    b.set_visibility(false);
    b.inactive_img = u;
    set_button_description(b, button_card_kind_msgs[i]);
    ui.add_object(b, 3);
    card_kind_buttons.push(b);
  }

  for (var i = 0; i < 4; i++) {
    var p = four_squares[i];
    var a = cutout_to_canvas(button_icon_img, 168 + p.x, 88 + p.y, 72, 72);
    var u = cutout_to_canvas(button_icon_img, 776 + p.x, 88 + p.y, 72, 72);
    consume_kind_button_available_imgs.push(a);
    consume_kind_button_unavailable_imgs.push(u);
  }

  card_kind_button_container = container({
    x: 620, y: 544,
    width: 144, height: 144,
    children: card_kind_buttons
  });
  ui.add_object(card_kind_button_container, 2);

  roll_button = new CanvasButton(960 - 144 / Math.sqrt(2) - button_roll_img.width / 2, 704 - 144 / Math.sqrt(2) - button_roll_img.height / 2,
      button_roll_img.width, button_roll_img.height,
      button_roll_img, button_roll_img, null);
  roll_button.set_activeness(false);
  roll_button.set_visibility(false);
  set_button_description(roll_button, 'ダイスロール!');
  ui.add_object(roll_button, 3);

  end_button = new CanvasButton(960 - 144 / Math.sqrt(2) - button_roll_img.width / 2, 704 - 144 / Math.sqrt(2) - button_roll_img.height / 2,
      button_end_img.width, button_end_img.height,
      button_end_img, button_end_img, null);
  end_button.set_visibility(false);
  end_button.set_activeness(false);
  set_button_description(end_button, 'ターンエンドだ!');
  ui.add_object(end_button, 3);

  trade_button = new CanvasButton(296, 584,
      button_trade_img.width, button_trade_img.height,
      button_trade_img, button_trade_img, null);
  trade_button.set_activeness(false);
  trade_button.set_visibility(false);
  set_button_description(trade_button, '他のプレイヤーや海外と資源の交換をします');
  ui.add_object(trade_button, 3);

  consume_button = new CanvasButton(464, 584,
      button_consume_img.width, button_consume_img.height,
      button_consume_img, button_consume_img, null);
  consume_button.set_activeness(false);
  consume_button.set_visibility(false);
  set_button_description(consume_button, '資源を使って開発をします');
  ui.add_object(consume_button, 3);

  card_button = new CanvasButton(632, 584,
      button_card_img.width, button_card_img.height,
      button_card_img, button_card_img, null);
  card_button.set_activeness(false);
  card_button.set_visibility(false);
  set_button_description(card_button, 'チャンスカードを使用します');
  ui.add_object(card_button, 3);

  trade_kind_player_button = button({
    x: 284,
    y: 544,
    imgOff: button_trade_player_img,
    active: false,
    visible: false,
  });
  set_button_description(trade_kind_player_button, '他のプレイヤーと資源を交換します');
  ui.add_object(trade_kind_player_button, 3);

  trade_kind_oversea_button = button({
    x: 284 + 72,
    y: 544,
    imgOff: button_trade_oversea_img,
    active: false,
    visible: false,
  });
  set_button_description(trade_kind_oversea_button, '海外と資源を交換します');
  ui.add_object(trade_kind_oversea_button, 3);

  trade_kind_buttons_container = container({
    x: 284, y: 544,
    width: 144, height: 144,
    children: [trade_kind_player_button, trade_kind_oversea_button]
  });
  ui.add_object(trade_kind_buttons_container, 2);

  trade_player_choice_buttons_container = new CanvasButton(284, 544,
      button_player_choice_whole_img.width, button_player_choice_whole_img.height,
      button_player_choice_whole_img, button_player_choice_whole_img, null);
  trade_player_choice_buttons_container.set_activeness(false);
  trade_player_choice_buttons_container.set_visibility(false);
  ui.add_object(trade_player_choice_buttons_container, 2);

  consume_kind_house_button = button({
    x: 452, y: 544,
    width: 72, height: 72,
    imgOff: cutout_to_canvas(button_icon_img, 168, 88, 72, 72),
    imgOn: cutout_to_canvas(button_icon_img, 168, 88, 72, 72),
    imgInactive: cutout_to_canvas(button_icon_img, 776, 88, 72, 72),
  });
  set_button_description(consume_kind_house_button, '家を建築します', '家を建築するための資源が足りません');
  ui.add_object(consume_kind_house_button, 3);

  consume_kind_house_frame = button({
    x: 452, y: 544,
    width: 72, height: 72,
    imgOff: cutout_to_canvas(button_icon_img, 400, 8, 72, 72),
    imgOn: cutout_to_canvas(button_icon_img, 400, 8, 72, 72),
  });
  ui.add_object(consume_kind_house_frame, 4);

  consume_kind_house_mouseover_anime = new FadeAnimation(
      452, 544, 0.5, 0, cutout_to_canvas(button_icon_img, 400, 8, 72, 72), null, easing_linear, 100);
  ui.add_object(consume_kind_house_mouseover_anime, 4);

  consume_kind_road_button = button({
    x: 452 + 72, y: 544,
    width: 72, height: 72,
    imgOff: cutout_to_canvas(button_icon_img, 168 + 72, 88, 72, 72),
    imgOn: cutout_to_canvas(button_icon_img, 168 + 72, 88, 72, 72),
    imgInactive: cutout_to_canvas(button_icon_img, 776 + 72, 88, 72, 72),
  });
  set_button_description(consume_kind_road_button, '道を建築します', '道を建築するための資源が足りません');
  ui.add_object(consume_kind_road_button, 3);

  consume_kind_road_frame = button({
    x: 452 + 72, y: 544,
    width: 72, height: 72,
    imgOff: cutout_to_canvas(button_icon_img, 400, 8, 72, 72),
    imgOn: cutout_to_canvas(button_icon_img, 400, 8, 72, 72),
  });
  ui.add_object(consume_kind_road_frame, 4);

  consume_kind_road_mouseover_anime = new FadeAnimation(
      452 + 72, 544, 0.5, 0, cutout_to_canvas(button_icon_img, 400, 8, 72, 72), null, easing_linear, 100);
  ui.add_object(consume_kind_road_mouseover_anime, 4);

  consume_kind_city_button = button({
    x: 452, y: 544 + 72,
    width: 72, height: 72,
    imgOff: cutout_to_canvas(button_icon_img, 168, 88 + 72, 72, 72),
    imgOn: cutout_to_canvas(button_icon_img, 168, 88 + 72, 72, 72),
    imgInactive: cutout_to_canvas(button_icon_img, 776, 88 + 72, 72, 72),
  });
  set_button_description(consume_kind_city_button, '家を街に発展させます', '街を建築するための資源が足りません');
  ui.add_object(consume_kind_city_button, 3);

  consume_kind_city_frame = button({
    x: 452, y: 544 + 72,
    width: 72, height: 72,
    imgOff: cutout_to_canvas(button_icon_img, 400, 8, 72, 72),
    imgOn: cutout_to_canvas(button_icon_img, 400, 8, 72, 72),
  });
  ui.add_object(consume_kind_city_frame, 4);

  consume_kind_city_mouseover_anime = new FadeAnimation(
      452, 544 + 72, 0.5, 0, cutout_to_canvas(button_icon_img, 400, 8, 72, 72), null, easing_linear, 100);
  ui.add_object(consume_kind_city_mouseover_anime, 4);

  consume_kind_chance_card_button = button({
    x: 452 + 72, y: 544 + 72,
    width: 72, height: 72,
    imgOff: cutout_to_canvas(button_icon_img, 168 + 72, 88 + 72, 72, 72),
    imgOn: cutout_to_canvas(button_icon_img, 168 + 72, 88 + 72, 72, 72),
    imgInactive: cutout_to_canvas(button_icon_img, 776 + 72, 88 + 72, 72, 72),
  });
  set_button_description(consume_kind_chance_card_button, 'チャンスカードを引きます', 'チャンスカードを引くための資源が足りません');
  ui.add_object(consume_kind_chance_card_button, 3);

  consume_kind_chance_card_mouseover_anime = new FadeAnimation(
      452 + 72, 544 + 72, 0.5, 0, cutout_to_canvas(button_icon_img, 400, 8, 72, 72), null, easing_linear, 100);
  ui.add_object(consume_kind_chance_card_mouseover_anime, 4);

  consume_kind_buttons_container = container({
    x: 452, y: 544,
    width: 144, height: 144,
    children: [
      consume_kind_house_button,
      consume_kind_road_button,
      consume_kind_city_button,
      consume_kind_chance_card_button,
    ]
  });
  ui.add_object(consume_kind_buttons_container, 2);

  roll_button.set_handler(function() {
    if (!(states.game.state == 'diceroll' &&
        game_info.turn == game_info.me)) {
            return;
    }

    send_diceroll();
  });

  end_button.set_handler(function() {
    if (!states.game.state == 'any') {
            return;
    }

    send_end();
    states.game.transit('none');
  });

  consume_button.set_handler(function() {
    states.consume_button.transit('kind');
  });

  trade_button.set_handler(function() {
    states.trade_button.transit('kind');
  });

  card_button.set_handler(function() {
    states.card_button.transit('kind');
  });

  consume_kind_house_button.set_handler(function() {
    states.game.transit('house');
  });

  consume_kind_house_button.set_mouse_on_handler(function() {
    ui.do_action_list([consume_kind_house_mouseover_anime]);
  });

  consume_kind_road_button.set_handler(function() {
    states.game.transit('road');
  });

  consume_kind_road_button.set_mouse_on_handler(function() {
    ui.do_action_list([consume_kind_road_mouseover_anime]);
  });

  consume_kind_city_button.set_handler(function() {
    states.game.transit('city');
  });

  consume_kind_city_button.set_mouse_on_handler(function() {
    ui.do_action_list([consume_kind_city_mouseover_anime]);
  });

  consume_kind_chance_card_button.set_handler(function() {
    states.consume_button.transit('top');
    send_generate_card();
  });

  consume_kind_chance_card_button.set_mouse_on_handler(function() {
    ui.do_action_list([consume_kind_chance_card_mouseover_anime]);
  });

  consume_kind_buttons_container.set_mouse_off_handler(function() {
    var state = states.consume_button.state;
    if (!(state === 'house' || state === 'road' || state === 'city')) {
      states.consume_button.transit('top');
    }
  });

  consume_kind_buttons_container.set_external_click_handler(function(evt) {
    var canvas_offset = ui.get_offset();
    var x = evt.pageX - canvas_offset.left;
    var y = evt.pageY - canvas_offset.top;
    if (board_x - 20 <= x && x <= board_x + 4 * board_cell_width + 20
        && board_y[0] - 20 <= y && y <= board_y[4] + 20) {
      // Do nothing
    } else {
      states.game.transit('any');
    }
  });

  trade_kind_oversea_button.set_handler(function() {
    states.trade_button.transit('oversea');
  });

  trade_kind_player_button.set_handler(function() {
    states.trade_button.transit('playerchoice');
  });

  trade_kind_buttons_container.set_mouse_off_handler(function() {
    states.trade_button.transit('top');
  });

  for (var i = 0; i < 4; i++) {
    (function(j) {
      trade_player_choice_buttons[i].set_handler(function() {
        states.trade_button.transit('player', { player_id: j });
      });
    })(i);
  }

  trade_player_choice_buttons_container.set_mouse_off_handler(function() {
    states.trade_button.transit('top');
  });

  card_kind_button_container.set_mouse_off_handler(function() {
    states.card_button.transit('top');
  });

  roll_button_in_anime = new RevolutionAnimation(960, 704, 144,
      Math.PI * 3 / 4, Math.PI * 5 / 4,
      -Math.PI / 2, 0,
      button_roll_img,
      button_roll_img.width / 2, button_roll_img.height / 2,
      easing_linear, 400);
  roll_button_out_anime = new RevolutionAnimation(
      960, 704, 144,
      Math.PI * 5 / 4, Math.PI * 7 / 4,
      0, Math.PI / 2,
      button_roll_img,
      button_roll_img.width / 2, button_roll_img.height / 2,
      easing_linear, 400);

  end_button_in_anime = new RevolutionAnimation(960, 704, 144,
      Math.PI * 3 / 4, Math.PI * 5 / 4,
      -Math.PI / 2, 0,
      button_end_img,
      button_end_img.width / 2, button_end_img.height / 2,
      easing_linear, 400);
  end_button_out_anime = new RevolutionAnimation(
      960, 704, 144,
      Math.PI * 5 / 4, Math.PI * 7 / 4,
      0, Math.PI / 2,
      button_end_img,
      button_end_img.width / 2, button_end_img.height / 2,
      easing_linear, 400);

  trade_kind_button_in_anime = new BloomAnimation(284, 544,
      button_trade_kind_img, false, easing_linear, 100);
  trade_kind_button_out_anime = new BloomAnimation(284, 544,
      button_trade_kind_img, true, easing_linear, 100);
  consume_kind_button_in_anime = new BloomAnimation(452, 544,
      button_consume_kind_img, false, easing_linear, 100);
  consume_kind_button_out_anime = new BloomAnimation(452, 544,
      button_consume_kind_img, true, easing_linear, 100);
  card_kind_button_in_anime = new BloomAnimation(620, 544,
      button_card_kind_img, false, easing_linear, 100);
  card_kind_button_out_anime = new BloomAnimation(620, 544,
      button_card_kind_img, true, easing_linear, 100);

  ui.add_object(roll_button_in_anime, 3);
  ui.add_object(roll_button_out_anime, 3);
  ui.add_object(end_button_in_anime, 3);
  ui.add_object(end_button_out_anime, 3);
  ui.add_object(consume_kind_button_in_anime, 3);
  ui.add_object(consume_kind_button_out_anime, 3);
  ui.add_object(trade_kind_button_in_anime, 3);
  ui.add_object(trade_kind_button_out_anime, 3);
  ui.add_object(card_kind_button_in_anime, 3);
  ui.add_object(card_kind_button_out_anime, 3);

  var trade_button_clear_img = document.getElementById('trade_button_clear');
  trade_player_img = cutout_to_canvas(button_trade_kind_img, 0, 0, 72, 144);
  trade_oversea_img = cutout_to_canvas(button_trade_kind_img, 72, 0, 72, 144);
  trade_player_clear_img = cutout_to_canvas(trade_button_clear_img, 0, 0, 72, 144);
  trade_oversea_clear_img = cutout_to_canvas(trade_button_clear_img, 72, 0, 72, 144);
  trade_base_img = document.getElementById('trade_base');
  trade_base_clear_img = document.getElementById('trade_base_clear');
  trade_form_decide_button_img = document.getElementById('trade_form_decide_button');

  trade_oversea_char_disappear_anime = new FadeAnimation(
      356, 544, 1, 0, trade_oversea_img, trade_oversea_clear_img, easing_linear, 150);
  ui.add_object(trade_oversea_char_disappear_anime, 3);

  trade_player_disappear_anime = new FadeAnimation(
      284, 544, 1, 0, trade_player_img, null, easing_linear, 150);
  ui.add_object(trade_player_disappear_anime, 3);

  trade_oversea_expand_anime = new RectangleExpandAnimation(
      trade_oversea_clear_img,
      544, 544 + 144, 356, 356 + 72,
      560, 704, 232, 495,
      easing_linear, 100);
  ui.add_object(trade_oversea_expand_anime, 3);

  trade_oversea_in_anime = new FadeAnimation(
      232, 560, 1, 0, trade_base_clear_img, trade_base_img, easing_linear, 150);
  ui.add_object(trade_oversea_in_anime, 3);

  trade_oversea_out_anime = new BloomAnimation(
      232, 560, trade_base_img, true, easing_linear, 150);
  ui.add_object(trade_oversea_out_anime, 3);

  trade_button_in_anime = new FadeAnimation(
      296, 584, 0, 1, button_trade_img, null, easing_linear, 150);
  ui.add_object(trade_button_in_anime, 3);

  trade_button_out_anime = new FadeAnimation(
      296, 584, 1, 0, button_trade_img, null, easing_linear, 150);
  ui.add_object(trade_button_out_anime, 3);

  consume_button_in_anime = new FadeAnimation(
      464, 584, 0, 1, button_consume_img, null, easing_linear, 150);
  ui.add_object(consume_button_in_anime, 3);

  consume_button_out_anime = new FadeAnimation(
      464, 584, 1, 0, button_consume_img, null, easing_linear, 150);
  ui.add_object(consume_button_out_anime, 3);

  card_button_in_anime = new FadeAnimation(
      632, 584, 0, 1, button_card_img, null, easing_linear, 150);
  ui.add_object(card_button_in_anime, 3);

  card_button_out_anime = new FadeAnimation(
      632, 584, 1, 0, button_card_img, null, easing_linear, 150);
  ui.add_object(card_button_out_anime, 3);

  tf = new TradeFormOversea(232, 536);
  tf.set_activeness(false);
  tf.set_visibility(false);
  tf.set_handler(function() {
    if (tf.capacity == 0) {
            return;
    } else if (tf.require != tf.capacity) {
            alert('The export and the import are mismatched');
            return;
    }

    states.trade_button.transit('top');

    send_oversea_trade(
        tf.ex_sbs.map(function(x) { return x.value; }),
        tf.in_sbs.map(function(x) { return x.value; }));
  });
  ui.add_object(tf, 5);

  for (var i = 0; i < 4; i++) {
    var t = new TradeFormPlayer(232, 536, i);
    t.set_activeness(false);
    t.set_visibility(false);

    function gen_handler(form, index) {
      return (function() {
        var exs = form.ex_sbs.map(function(x) { return x.value; });
        var ins = form.in_sbs.map(function(x) { return x.value; });

        for (var i = 0; i < 5; i++) {
          if (exs[i] != 0 && ins[i] != 0) {
            alert('Illegal trade');
            return;
          }
        }

        var exs_sum = exs.reduce(function(x, y) { return x + y; }, 0);
        var ins_sum = ins.reduce(function(x, y) { return x + y; }, 0);

        if (exs_sum == 0 || ins_sum == 0) {
          alert('Illegal trade');
          return;
        }

        states.trade_button.transit('top');

        send_local_trade(index, exs, ins);
      });
    }

    t.set_handler(gen_handler(t, i));
    ui.add_object(t, 5);

    trade_form_players.push(t);
  }

  monopoly_form = new MonopolyForm();
  monopoly_form.set_activeness(false);
  monopoly_form.set_visibility(false);
  monopoly_form.set_handler(function(i) {
    states.card_button.transit('top');
    send_card_monopoly(resource_order[i]);
  });
  ui.add_object(monopoly_form, 2);

  harvest_form = new HarvestForm(trade_form_decide_button_img);
  harvest_form.set_activeness(false);
  harvest_form.set_visibility(false);
  harvest_form.set_handler(function(evt) {
    states.card_button.transit('top');
    send_card_harvest(harvest_form.sbs.map(function(x) { return x.value; }));
  });
  ui.add_object(harvest_form, 3);

  var trade_confirm_button_img = document.getElementById('trade_confirm_button');
  trade_button_accept_img = cutout_to_canvas(trade_confirm_button_img, 16, 16, 56, 24);
  trade_button_decline_img = cutout_to_canvas(trade_confirm_button_img, 16, 48, 56, 24);
  trade_button_accept_pushed_img = cutout_to_canvas(trade_confirm_button_img, 80, 16, 56, 24);
  trade_button_decline_pushed_img = cutout_to_canvas(trade_confirm_button_img, 80, 48, 56, 24);

  var ctx = trade_confirm_form_img.getContext('2d');
  ctx.fillStyle = 'rgb(75,75,75)';
  ctx.fillRect(0, 0, 400, 104);
  ctx.drawImage(trade_button_decline_img, 536 - 264, 648 - 576);
  ctx.drawImage(trade_button_accept_img, 600 - 264, 648 - 576);

  trade_confirm_form_in_anime =
      new StraightLineAnimation(264, 704, 264, 576, trade_confirm_form_img1, easing_linear, 150);
  trade_confirm_form_out_anime =
      new StraightLineAnimation(264, 576, 264, 704, trade_confirm_form_img1, easing_linear, 150);
  ui.add_object(trade_confirm_form_in_anime, 3);
  ui.add_object(trade_confirm_form_out_anime, 3);

  trade_confirm_form = new TradeConfirmForm();
  trade_confirm_form.set_visibility(false);
  trade_confirm_form.set_activeness(false);
  ui.add_object(trade_confirm_form, 3);

  trade_confirm_form.set_accept_handler(function() {
    send_acceptance(true);
    states.game.transit('none');
  });

  trade_confirm_form.set_decline_handler(function() {
    send_acceptance(false);
    states.game.transit('none');
  });

  var burst_button_img_src = document.getElementById('burst_button');
  burst_button_inactive_img = cutout_to_canvas(burst_button_img_src, 0, 0, 88, 24);
  burst_button_mousedown_img = cutout_to_canvas(burst_button_img_src, 0, 32, 88, 24);

  burst_button_img = document.createElement('canvas');
  burst_button_img.width = burst_button_inactive_img.width;
  burst_button_img.height = burst_button_inactive_img.height;

  burst_form = new BurstForm(
      burst_button_img,
      burst_button_inactive_img,
      burst_button_mousedown_img);
  burst_form.set_activeness(false);
  burst_form.set_visibility(false);
  burst_form.set_handler(function(evt) {
    this.parent_canvas_ui.do_action_list([
      function() {
        burst_form.set_activeness(false);
        burst_form.set_visibility(false);
      },
      function() { ui.redraw(); }
    ]);
    send_burst(burst_form.sbs.map(function(x) { return x.value; }));
  });
  ui.add_object(burst_form, 3);

  card_knight_button = card_kind_buttons[0];
  card_knight_button.set_handler(function() {
    states.card_button.transit('top');
    send_card_knight();
  });

  card_road_button = card_kind_buttons[1];
  card_road_button.set_handler(function() {
    states.card_button.transit('top');
    ui.do_action_list([
      function() {
        card_kind_button_container.set_activeness(false);
        card_kind_buttons.forEach(function(b) {
          b.set_activeness(false);
          b.set_visibility(false);
        });
      },
      card_kind_button_out_anime,
      function() {
        card_button.set_visibility(true);
        card_button.set_activeness(true);
        ui.redraw();
      }
    ]);

    send_card_road();
  });

  card_harvest_button = card_kind_buttons[2];
  card_harvest_button.set_handler(function() {
    states.card_button.transit('harvest');
  });

  card_monopoly_button = card_kind_buttons[3];
  card_monopoly_button.set_handler(function() {
    states.card_button.transit('monopoly');
  });

  diceroll_switch_image1 = new SwitchImageRandom(dice_imgs, 50);
  diceroll_switch_image2 = new SwitchImageRandom(dice_imgs, 50);
  diceroll_switch_image1.set_end_time(440);

  diceroll_switch_anime1 = new StraightLineAnimation(256, 168, 256 + 10, 168,
      diceroll_switch_image1, gen_damped_oscillation(3), 500);
  diceroll_switch_anime2 = new StraightLineAnimation(288, 168, 288 + 10, 168,
      diceroll_switch_image2, gen_damped_oscillation(3), 500);
  ui.add_object(diceroll_switch_anime1, 9);
  ui.add_object(diceroll_switch_anime2, 9);

  var robber_screen_img = document.getElementById('robber_screen');

  robber_screen_in_anime = new FadeAnimation(
      0, 0, 0, 1, robber_screen_img, null, easing_linear, 150);
  ui.add_object(robber_screen_in_anime, 1);

  robber_screen_out_anime = new FadeAnimation(
      0, 0, 1, 0, robber_screen_img, null, easing_linear, 150);
  ui.add_object(robber_screen_out_anime, 1);

  robber_screen = new CanvasButton(
      0, 0, 960, 704, robber_screen_img, robber_screen_img, null);
  robber_screen.set_visibility(false);
  robber_screen.set_activeness(false);
  ui.add_object(robber_screen, 1);

  var victory_screen_img_src = document.getElementById('victory_screen');
  var victory_screen_img = cutout_to_canvas(victory_screen_img_src, 0, 214, 960, 275);
  var victory_screen_in_anime = new FadeAnimation(
      0, 0, 0, 1, victory_screen_img, null, easing_linear, 150);
  var victory_screen_out_anime = new FadeAnimation(
      0, 0, 1, 0, victory_screen_img, null, easing_linear, 150);
  overlay_ui.add_object(victory_screen_in_anime, 0);
  overlay_ui.add_object(victory_screen_out_anime, 0);

  var victory_screen = new CanvasButton(
      0, 0, 960, 275, victory_screen_img, victory_screen_img, null);
  victory_screen.set_visibility(false);
  victory_screen.set_activeness(false);
  victory_screen.set_handler(function() { send_victory(); });
  overlay_ui.add_object(victory_screen, 0);

  var burst_screen_img_src = document.getElementById('burst_screen');
  burst_screen_other_img = cutout_to_canvas(burst_screen_img_src, 16, 16, 264 - 16, 88 - 16);
  burst_screen_me_img = cutout_to_canvas(burst_screen_img_src, 16, 96, 264 - 16, 232 - 96);

  var r_info = {
    'none': { reach: BA(function() { roll_button.disappear();
        end_button.disappear() }) },
    'roll': action_appearance(roll_button),
    'end': action_appearance(end_button),
    'none->roll': BA(roll_button_in_anime),
    'roll->end': BA([roll_button_out_anime, end_button_in_anime]),
    'end->none': BA(end_button_out_anime)
  };

  var c_info = {
    'none': { reach: BA(function() {
      consume_button.disappear();
      consume_kind_buttons_container.disappear() }) },
    'top': action_appearance(consume_button),
    'kind': {
      reach: BA(function() {
        consume_kind_buttons_container.appear();
        consume_kind_house_button.set_activeness(menu_is_available('house'));
        consume_kind_road_button.set_activeness(menu_is_available('road'));
        consume_kind_city_button.set_activeness(menu_is_available('city'));
        consume_kind_chance_card_button.set_activeness(menu_is_available('card'));
      }),
      leave: BA(function(old_sp, new_sp, to) {
        if (!(to === 'house' || to === 'road' || to === 'city')) {
          consume_kind_buttons_container.disappear();
        }
      })
    },
    'none->top': BA(consume_button_in_anime),
    'top->none': BA(consume_button_out_anime),
    'top->kind': BA(consume_kind_button_in_anime),
    'kind->top': BA(consume_kind_button_out_anime),
    'house': action_custom_appearance(
        { both: [consume_kind_house_frame],
          leaveOnly: [consume_kind_buttons_container] }),
    'road': action_custom_appearance(
        { both: [consume_kind_road_frame],
          leaveOnly: [consume_kind_buttons_container] }),
    'city': action_custom_appearance(
        { both: [consume_kind_city_frame],
          leaveOnly: [consume_kind_buttons_container] }),
  };

  var t_info = {
    'none': { reach: BA(function() {
      trade_form_players.forEach(function(x) { x.disappear(); });
      tf.disappear();
      trade_kind_buttons_container.disappear();
      trade_kind_player_button.disappear();
      trade_kind_oversea_button.disappear();
      trade_button.disappear(); }) },
    'top': action_appearance(trade_button),
    'kind': action_appearance(trade_kind_buttons_container),
    'oversea': action_appearance(tf),
    'playerchoice': {
      reach: BA(function() {
        trade_player_choice_buttons_container.set_activeness(true);
        player_choice_list.forEach(function(x) {
          trade_player_choice_buttons[x].appear();
        });
      }),
      leave: BA(function() {
        trade_player_choice_buttons_container.set_activeness(false);
        player_choice_list.forEach(function(x) {
          trade_player_choice_buttons[x].disappear();
        });
      })
    },
    'player': {
      reach: BA(function(old_sp, new_sp) {
        trade_form_players[new_sp.player_id].reset();
        trade_form_players[new_sp.player_id].appear();
      }),
      leave: BA(function(old_sp, new_sp) {
        trade_form_players[old_sp.player_id].disappear();
      })
    },
    'none->top': BA([trade_button_in_anime]),
    'top->none': BA([consume_button_out_anime]),
    'top->kind': BA([trade_kind_button_in_anime]),
    'kind->top': BA([trade_kind_button_out_anime]),
    'kind->oversea': BA(
        [trade_oversea_char_disappear_anime, trade_player_disappear_anime],
        trade_oversea_expand_anime,
        function() { tf.reset(); }
    ),
    'oversea->top': BA(),
    'kind->playerchoice': BA(trade_player_choice_button_in_anime),
    'playerchoice->top': BA(trade_button_in_anime),
    'playerchoice->player' : LA(function(old_sp, new_sp) {
      var other_ids = player_choice_list.filter(function(x) {
        return new_sp.player_id != x;
      });
      var other_animes = other_ids.map(function(x) {
        return trade_player_choice_button_next_animes[x];
      });
      var expand_anime = trade_player_choice_button_expand_animes[new_sp.player_id];
      var b = trade_player_choice_buttons[new_sp.player_id];

      return [
        other_animes,
        function() { b.disappear() },
        expand_anime,
      ];
    }),
    'player->top': BA(trade_button_in_anime),
  };

  var ch_info = {
    'none': { reach: BA(function() { card_button.disappear(); }) },
    'top': action_appearance(card_button),
    'kind': {
      reach: BA(function() {
        card_kind_button_container.appear();
        for (var i = 0; i < 4; i++) {
          var my_info = game_info.player_info[game_info.me];
          var card_kind = chance_card_button_order[i];
          var activeness = my_info.chance_cards[card_kind] > 0;
          var b = card_kind_buttons[i];
          b.set_visibility(true);
          b.set_activeness(activeness);
        }
      }),
      leave: BA(function() { card_kind_button_container.disappear(); })
    },
    'harvest': action_appearance(harvest_form),
    'monopoly': action_appearance(monopoly_form),
    'none->top': BA(card_button_in_anime),
    'top->none': BA(card_button_out_anime),
    'top->kind': BA(card_kind_button_in_anime),
    'kind->top': BA(card_kind_button_out_anime),
    // 'kind->harvest': BA()
    // 'kind->monopoly': BA()
  };

  var re_info = {
    'none': { reach: BA(function() {
      roll_button.disappear();
      end_button.disappear();
    })
    },
    'roll': action_appearance(roll_button),
    'end': action_appearance(end_button),
    'none->roll': BA(roll_button_in_anime),
    'roll->end': BA([roll_button_out_anime, end_button_in_anime]),
    'end->none': BA(end_button_out_anime),
  };

  var rs_info = {
    'none': { reach: BA(function() { robber_screen.disappear(); }) },
    'show': action_appearance(robber_screen),
    'none->show': BA(robber_screen_in_anime),
    'show->none': BA(robber_screen_out_anime),
  };

  var bf_info = {
    'none': { reach: BA(function() { burst_form.disappear(); }) },
    'show': action_appearance(burst_form),
    //'none->show': BA(function() { burst_form.reset(); })
  };

  var ta_info = {
    'none': { reach: BA(function() { trade_confirm_form.disappear() })},
    'show': action_appearance(trade_confirm_form),
    'none->show': BA(function(old_sp, new_sp) {
      var ctx = trade_confirm_form_img1.getContext('2d');
      ctx.drawImage(trade_confirm_form_img, 0, 0);
      ctx.font = 'sans';
      ctx.fillStyle = 'rgb(255,255,255)';
      var message = gen_trade_confirm_message(new_sp.d);
      var lines = message.split('\n');
      for (var i = 0; i < lines.length; i++) {
        ctx.fillText(lines[i], 20, 20 + i * 20);
      }
    }, trade_confirm_form_in_anime),
    'show->none': BA(trade_confirm_form_out_anime),
  };

  var vs_info = {
    'none': { reach: BA(function() { $('#overlay').css('display', 'none') }),
              leave: BA(function() { $('#overlay').css('display', 'block') })},
    'none->show': BA(victory_screen_in_anime),
    'show->none': BA(victory_screen_out_anime),
    'show': { reach: BA(function() { victory_screen.appear(); overlay_ui.redraw(); }),
              leave: BA(function() { victory_screen.disappear(); })}
  };

  states.roll_button = new AnimationState(ui, r_info, 'none');
  states.consume_button = new AnimationState(ui, c_info, 'none');
  states.trade_button = new AnimationState(ui, t_info, 'none');
  states.card_button = new AnimationState(ui, ch_info, 'none');
  states.roll_button = new AnimationState(ui, re_info, 'none');
  states.robber_screen = new AnimationState(ui, rs_info, 'none');
  states.burst_form = new AnimationState(ui, bf_info, 'none');
  states.trade_acceptance = new AnimationState(ui, ta_info, 'none');
  states.victory_screen = new AnimationState(overlay_ui, vs_info, 'none');

  function gen_all_none(m) {
    return function() {
      states.roll_button.transit('none');
      states.consume_button.transit('none');
      states.trade_button.transit('none');
      states.card_button.transit('none');
      states.robber_screen.transit('none');
      states.trade_acceptance.transit('none');
      update_global_hint(m);
    };
  }

  var gs_info = {
    'none': action_reach(gen_all_none('')),
    'house_begin': action_reach(gen_all_none('家を設置する場所を選んでください')),
    'house': action_reach(function() {
      states.consume_button.transit('house');
      update_global_hint('家を設置する場所を選んでください');
    }),
    'road_begin': action_reach(gen_all_none('道を設置する場所を選んでください')),
    'road': action_reach(function() {
      states.consume_button.transit('road');
      update_global_hint('道を設置する場所を選んでください');
    }),
    'city': action_reach(function() {
      states.consume_button.transit('city');
      update_global_hint('街を設置する場所を選んでください');
    }),
    'victory': action_reach(function() {
      states.victory_screen.transit('show');
      update_global_hint('勝利宣言をしてください');
    }),
    'diceroll': action_reach(function() {
      states.roll_button.transit('roll');
      states.consume_button.transit('none');
      states.trade_button.transit('none');
      states.card_button.transit('top');
      states.robber_screen.transit('none');
      update_global_hint('ダイスを振ってください');
    }),
    'knight': action_reach(gen_all_none('盗賊を移動させてください')),
    'target_of_robber': action_reach(gen_all_none('資源を奪う相手を選んでください')),
    'trade_acceptance': action_reach(function(a, sp) {
      states.trade_acceptance.transit('show', sp);
      update_global_hint('国内貿易の申し入れがあります');
    }),
    'any': action_reach(function() {
      states.roll_button.transit('end');
      states.consume_button.transit('top');
      states.trade_button.transit('top');
      states.card_button.transit('top');
      states.robber_screen.transit('none');
      update_global_hint('');
    }),
    'burst': action_reach(function() {
      states.roll_button.transit('none');
      states.consume_button.transit('none');
      states.trade_button.transit('none');
      states.card_button.transit('none');
      states.robber_screen.transit('none');
      states.burst_form.transit('show');
      update_global_hint('バースト中 捨てるカードを選んでください');
    }),
  };

  states.game = new AnimationState(ui, gs_info, 'none');

  // Websocket connection
  ws_disp.bind('joined', on_joined);
  ws_disp.bind('password_required', on_password_required);
  ws_disp.bind('connection_closed', on_connection_closed);
  ws_disp.bind('connection_error', on_connection_error);
  ws_disp.bind('room_member_changed', on_room_member_changed);
  ws_disp.bind('game_start', on_game_start);
  ws_disp.bind('chat', on_chat);
  ws_disp.bind('dispose', on_dispose);

  // Join
  ws_disp.trigger('join',
      { session_id: session_id, room_id: room_id });

  setup_completed = true;

  switch_screen(false);
}

function CanvasSpinBox(x, y, level) {
  CanvasWidget.call(this);

  this.value = 0;
  this.text_width = 24;
  this.x = x;
  this.y = y;
  this.level = level;
  this.up_button = new CanvasButton(x + this.text_width, y, 8, 8,
      up_off, up_on, null);
  this.down_button = new CanvasButton(x + this.text_width, y + 24, 8, 8,
      down_off, down_on, null);
  this.parent_canvas_ui = null;

  var that = this;
  this.up_button.set_handler(function() { that.count_up(); });
  this.down_button.set_handler(function() { that.count_down(); });
}

inherits(CanvasSpinBox, CanvasWidget);

CanvasSpinBox.prototype.on_value_changed = function(is_up) {
  if (is_up) {
    ++this.value;
  } else {
    --this.value;
  }
};

CanvasSpinBox.prototype.set_parent_canvas_ui = function(ui) {
  this.parent_canvas_ui = ui;
  ui.add_object(this.up_button, this.level);
  ui.add_object(this.down_button, this.level);
};

CanvasSpinBox.prototype.count_up = function() {
  this.on_value_changed(true);
  this.parent_canvas_ui.redraw();
};

CanvasSpinBox.prototype.count_down = function() {
  this.on_value_changed(false);
  this.parent_canvas_ui.redraw();
};

CanvasSpinBox.prototype.redraw = function(ctx) {
  ctx.font = '700 18px Roboto';
  ctx.fillStyle = 'rgb(0,0,0)';
  ctx.textBaseline = 'middle';
  ctx.fillText(this.value.toString(), this.x, this.y + 16, this.text_width);
};

CanvasSpinBox.prototype.set_activeness = function(active) {
  this.active = active;
  this.up_button.set_activeness(active);
  this.down_button.set_activeness(active);
};

CanvasSpinBox.prototype.set_visibility = function(visible) {
  this.visible = visible;
  this.up_button.set_visibility(visible);
  this.down_button.set_visibility(visible);
};

// Trade form

function TradeForm(x, y, bg_image, button_img) {
  CanvasWidget.call(this);

  this.x = x;
  this.y = y;
  this.width = bg_image.width;
  this.height = bg_image.height;
  this.ex_sbs = [];
  this.in_sbs = [];
  this.parent_canvas_ui = null;
  this.level = 0;
  this.bg_image = bg_image;

  for (var i = 0; i < 5; i++) {
    this.ex_sbs.push(new CanvasSpinBox(x + 48 + i * 40, y + 62, 7));
    this.in_sbs.push(new CanvasSpinBox(x + 48 + i * 40, y + 62 + 36, 7));
  }

  this.decide_button = new CanvasButton(x + 168, y + 137,
      button_img.width, button_img.height,
      button_img, button_img, null);
  this.decide_button.set_mousedown_img(document.getElementById('trade_button_mousedown'));
}

inherits(TradeForm, CanvasWidget);

TradeForm.prototype.redraw = function(ctx) {
  ctx.drawImage(this.bg_image,
      this.x, this.y,
      this.bg_image.width, this.bg_image.height);
};

TradeForm.prototype.set_parent_canvas_ui = function(ui) {
  this.parent_canvas_ui = ui;
  var that = this;
  this.ex_sbs.forEach(function(x) { ui.add_object(x, 6); });
  this.in_sbs.forEach(function(x) { ui.add_object(x, 6); });
  ui.add_object(this.decide_button, 6);
};

TradeForm.prototype.set_activeness = function(active) {
  this.active = active;

  this.ex_sbs.forEach(function(x) { x.set_activeness(active); });
  this.in_sbs.forEach(function(x) { x.set_activeness(active); });
  this.decide_button.set_activeness(active);
};

TradeForm.prototype.set_visibility = function(visible) {
  this.visible = visible;

  this.ex_sbs.forEach(function(x) { x.set_visibility(visible); });
  this.in_sbs.forEach(function(x) { x.set_visibility(visible); });
  this.decide_button.set_visibility(visible);
};

TradeForm.prototype.set_handler = function(handler) {
  this.decide_button.set_handler(handler);
};

TradeForm.prototype.click_event = function(evt) {
  var canvas_offset = this.parent_canvas_ui.get_offset();
  var x = evt.pageX - (canvas_offset.left + this.x);
  var y = evt.pageY - (canvas_offset.top + this.y);
  var that = this;

  if (!(0 <= x && x < this.width &&
      0 <= y && y < this.height)) {
    this.parent_canvas_ui.redraw();
    states.trade_button.transit('top');
    return false;
  }
  // else

  return true;
};

TradeForm.prototype.reset = function() {
  this.in_sbs.forEach(function(x) { x.value = 0; });
  this.ex_sbs.forEach(function(x) { x.value = 0; });
};

// TradeFormOversea
function TradeFormOversea(x, y) {
  TradeForm.call(this, x, y, trade_base_img, trade_form_decide_button_img);

  var that = this;

  this.capacity = 0;
  this.require = 0;

  function gen_export_handler(kind) {
    return (function(is_up) {
      var my_info = game_info.player_info[game_info.me];
      // Get step
      var step = 4;
      if (my_info.ports[kind]) {
        step = 2;
      } else if (my_info.ports.versatile) {
        step = 3;
      }

      if (is_up) {
        if (this.value + step <= my_info.resources[kind]) {
          this.value += step;
          ++that.capacity;
        }
      } else {
        if (step <= this.value) {
          this.value -= step;
          --that.capacity;
        }
      }
    });
  }

  function import_handler(is_up) {
    if (is_up) {
      ++this.value;
      ++that.require;
    } else {
      if (this.value > 0) {
        --this.value;
        --that.require;
      }
    }
  }

  for (var i = 0; i < 5; i++) {
    this.ex_sbs[i].on_value_changed = gen_export_handler(resource_order[i]);
    this.in_sbs[i].on_value_changed = import_handler;
  }
}

inherits(TradeFormOversea, TradeForm);

TradeFormOversea.prototype.reset = function() {
  TradeForm.prototype.reset.call(this);
  this.require = 0;
  this.capacity = 0;
};

function TradeFormPlayer(x, y, player_id) {
  TradeForm.call(this, x, y, trade_form_player_imgs1[player_id], trade_form_decide_button_img);

  this.player_id = player_id;

  function gen_export_handler(kind) {
    return (function(is_up) {
      var my_info = game_info.player_info[game_info.me];

      if (is_up) {
        if (this.value + 1 <= my_info.resources[kind]) {
          ++this.value;
        }
      } else {
        if (this.value >= 1) {
          --this.value;
        }
      }
    });
  }

  function import_handler(is_up) {
    if (is_up) {
      ++this.value;
    } else {
      if (this.value >= 1) {
        --this.value;
      }
    }
  }

  for (var i = 0; i < 5; i++) {
    this.ex_sbs[i].on_value_changed = gen_export_handler(resource_order[i]);
    this.in_sbs[i].on_value_changed = import_handler;
  }
}

inherits(TradeFormPlayer, TradeForm);

// Deck info
var x_left = 16;
var y_top = 16;
var deck_width = 96;
var deck_height = 80;
var deck_x_margin = 8;
var deck_y_margin = 8;

var chance_card_width = 72;
var chance_card_height = 72;


function DeckInfoArea() {
  CanvasWidget.call(this);
}

inherits(DeckInfoArea, CanvasWidget);

DeckInfoArea.prototype.redraw = function(ctx) {
  ctx.fillStyle = di_bg_color;
  ctx.fillRect(deck_info_x, deck_info_y, deck_info_w, deck_info_h);

  ctx.fillStyle = 'rgb(255,255,255)';
  var players = game_info.player_list.reduce(function(x, y) { return x + ',' + y; }, '');
  ctx.fillText(players, 20, 20);

  var x = deck_info_margin_left;
  var i = 0;
  while (true) {
    var kind = resource_order[i];
    var img = resource_deck_imgs[kind][game_info.deck_info[kind]];
    ctx.drawImage(img, x, deck_info_margin_top, img.width, img.height);

    ++i;
    x += img.width;
    if (i == 5) break;
    x += deck_info_margin_mid;
  }
  x += deck_info_margin_mid_chance_card;

  var cc_img = chance_card_deck_imgs[game_info.deck_info.chance_cards];
  ctx.drawImage(cc_img,
      x, deck_info_margin_top_chance_card,
      cc_img.width, cc_img.height);
};

// Dice info
function DiceInfoArea() {
  CanvasWidget.call(this);

  this.dice1 = 1;
  this.dice2 = 1;
}

inherits(DiceInfoArea, CanvasWidget);

DiceInfoArea.prototype.set_value = function(dice1, dice2) {
  this.dice1 = dice1;
  this.dice2 = dice2;
};

DiceInfoArea.prototype.redraw = function(ctx) {
  ctx.drawImage(dice_imgs[this.dice1 - 1], 256, 168);
  ctx.drawImage(dice_imgs[this.dice2 - 1], 288, 168);
};

// Player info

function PlayerInfoArea() {
  CanvasWidget.call(this);

  this.interval_id = setInterval(this.tick.bind(this) , 1000 / 8);

  this.resources_shown_value = [
    { lumber: -1, brick: -1, wool: -1, ore: -1, grain: -1 },
    { lumber: -1, brick: -1, wool: -1, ore: -1, grain: -1 },
    { lumber: -1, brick: -1, wool: -1, ore: -1, grain: -1 },
    { lumber: -1, brick: -1, wool: -1, ore: -1, grain: -1 },
  ];
  this.others_resources_shown_value = [-1, -1, -1, -1];

  this.value_not_shown_remain_time = [
    { lumber: 0, brick: 0, wool: 0, ore: 0, grain: 0 },
    { lumber: 0, brick: 0, wool: 0, ore: 0, grain: 0 },
    { lumber: 0, brick: 0, wool: 0, ore: 0, grain: 0 },
    { lumber: 0, brick: 0, wool: 0, ore: 0, grain: 0 },
  ];

  this.others_value_not_shown_remain_time = [0, 0, 0, 0];
}

inherits(PlayerInfoArea, CanvasWidget);

PlayerInfoArea.prototype.tick = function() {
  var updated = false;

  for (var i = 0; i < 4; i++) {
    if (game_info.me === i) {
      for (var j = 0; j < resource_order.length; j++) {
        var kind = resource_order[j];
        var r = game_info.player_info[i].resources[kind];
        var s = this.resources_shown_value[i][kind];
        if (s !== r) {
          this.resources_shown_value[i][kind] += Math.sign(r - s);
          if (r == this.resources_shown_value[i][kind]) {
            this.value_not_shown_remain_time[i][kind] = 4;
          }
          updated = true;
        } else if (this.value_not_shown_remain_time[i][kind] > 0) {
          --this.value_not_shown_remain_time[i][kind];
          updated = true;
        }
      }
    } else {
      var r = 0;
      var s = this.others_resources_shown_value[i];
      $.each(game_info.player_info[i].resources, function(y, z) {
        r += z;
      });
      if (s !== r) {
        this.others_resources_shown_value[i] += Math.sign(r - s);
        updated = true;
        this.others_value_not_shown_remain_time[i] = 4;
      } else if (this.others_value_not_shown_remain_time[i] > 0) {
        --this.others_value_not_shown_remain_time[i];
        updated = true;
      }
    }
  }

  if (updated) {
    //ui.redraw();
  }
};

PlayerInfoArea.prototype.set_exact_shown_value = function() {
  for (var i = 0; i < 4; i++) {
    for (var j = 0; j < resource_order.length; j++) {
      var kind = resource_order[j];
      this.resources_shown_value[i][kind] =
          game_info.player_info[i].resources[kind];
    }
  }
};

PlayerInfoArea.prototype.redraw = function(ctx) {
  var y = player_area_y;
  for (var i = 0; i < 4; i++) {
    ctx.fillStyle = player_colors[i];
    var height = (game_info.me == i ? player_area_me_height :
        player_area_other_height);
    ctx.fillRect(player_area_x, y, player_area_width, height);
    if (_.includes(this.those_bursting, i)) {
      if (i === game_info.me) {
        ctx.drawImage(burst_screen_me_img, player_area_x, y);
      } else {
        ctx.drawImage(burst_screen_other_img, player_area_x, y);
      }
    }
    if (i == game_info.turn) {
      ctx.strokeStyle = player_edge_color;
      ctx.lineWidth = player_edge_width;
      ctx.strokeRect(player_area_x, y, player_area_width, height);
    }

    if (game_info.player_info[i].name != null) {
      var path = new Path2D();
      path.moveTo(player_area_x + player_area_victory_point_offset_x +
          player_area_victory_point_radius,
          y + player_area_victory_point_offset_y);
      path.arc(player_area_x + player_area_victory_point_offset_x +
          player_area_victory_point_radius,
          y + player_area_victory_point_offset_y +
          player_area_victory_point_radius,
          player_area_victory_point_radius,
          0, Math.PI * 2);
      ctx.fillStyle = player_area_victory_point_bg;
      ctx.fill(path);

      ctx.fillStyle = player_colors[i];
      ctx.font = player_area_victory_point_font;
      ctx.textBaseline = 'middle';
      //console.log(game_info.player_info[i].score.toString());
      var width = ctx.measureText(game_info.player_info[i].score).width;
      //console.log(player_area_x + player_area_victory_point_offset_x
      //                 + player_area_victory_point_radius - (width / 2));
      //console.log(y + player_area_victory_point_offset_y
      //                 + player_area_victory_point_radius);
      ctx.fillText(game_info.player_info[i].score,
          player_area_x + player_area_victory_point_offset_x +
          player_area_victory_point_radius - (width / 2),
          y + player_area_victory_point_offset_y +
          player_area_victory_point_radius);

      ctx.font = player_area_name_font;
      ctx.fillStyle = player_area_name_color;
      ctx.textBaseline = 'alphabetic';
      ctx.fillText(game_info.player_info[i].name,
          player_area_x + player_area_name_offset_x,
          y + player_area_name_offset_y);

      if (game_info.player_info[i].special_cards.longest_road &&
          game_info.player_info[i].special_cards.largest_army) {
        var rx = player_area_x + player_area_width - 52;
        var ry = y + 6;
        ctx.drawImage(special_card_road_img, rx, ry);
        ctx.drawImage(special_card_army_img, rx + 24, ry);
      } else if (game_info.player_info[i].special_cards.longest_road) {
        var rx = player_area_x + player_area_width - 52 + 24;
        var ry = y + 6;
        ctx.drawImage(special_card_road_img, rx, ry);
      } else if (game_info.player_info[i].special_cards.largest_army) {
        var rx = player_area_x + player_area_width - 52 + 24;
        var ry = y + 6;
        ctx.drawImage(special_card_army_img, rx, ry);
      }

      if (i == game_info.me) {
        for (var j = 0; j < 5; j++) {
          var rx = player_area_x + 8 + (24 + 6) * j;
          var ry = y + 40;
          var text = this.resources_shown_value[i][resource_order[j]];
          ctx.drawImage(resource_amount_images[j], rx, ry);

          ctx.fillStyle = 'rgb(255,255,255)';
          ctx.font = '100 18px Roboto';
          ctx.textBaseline = 'alphabetic';
          var width = ctx.measureText(text).width;

          if (this.value_not_shown_remain_time[i][resource_order[j]] % 2 === 0) {
            ctx.fillText(text, rx + 24 / 2 - width / 2, ry + 48);
          }
        }
        var text = _.reduce(this.resources_shown_value[i],
          function (a, x) { return a + x; });

        ctx.fillStyle = 'rgb(0,0,0)';
        ctx.font = '100 18px Roboto';
        ctx.textBaseline = 'alphabetic';
        var width = ctx.measureText(text).width;
        ctx.fillText('計', player_area_x + 8 + (24 + 6) * 5 + 24 / 2 - ctx.measureText('計').width / 2, y + 68);
        ctx.fillText(text, player_area_x + 8 + (24 + 6) * 5 + 24 / 2 - width / 2, y + 88);

        var chance_cards = game_info.player_info[i].chance_cards;
        var new_chance_cards = game_info.player_info[i].new_chance_cards;
        var cnt = 0;
        for (var a = 0; a < 5; a++) {
          var text = chance_cards[chance_card_order[a]] + new_chance_cards[chance_card_order[a]];
          if (text == 0) { continue; }
          var rx = player_area_x + 8 + cnt * (35 + 4);
          var ry = y + 104;
          ctx.drawImage(chance_available_imgs[a], rx, ry);
          ctx.fillStyle = 'rgb(0,0,0)';
          ctx.font = '300 18px Roboto';
          ctx.textBaseline = 'alphabetic';
          ctx.fillText(text, rx + 24, ry + 22);
          ++cnt;
        }

        cnt = 0;
        for (var j = 0; j < 4; j++) {
          var text = game_info.player_info[i].used_chance_cards[chance_used_order[j]];
          if (text == 0) { continue; }
          var rx = player_area_x + player_area_width - 36;
          var ry = y + 41 + (18 + 5) * cnt;
          ctx.drawImage(chance_used_imgs[j], rx, ry);
          ctx.fillStyle = 'rgb(255,255,255)';
          ctx.font = '100 14px Roboto';
          ctx.textBaseline = 'alphabetic';
          ctx.fillText(text, rx + 18, ry + 17);
          ++cnt;
        }
      } else {
        var that = this;
        function ccard_sum(x) {
          var p = game_info.player_info[x];
          var r = 0;
          $.each(p.chance_cards, function(y, z) {
            r += z;
          });
          $.each(p.new_chance_cards, function(y, z) {
            r += z;
          });
          return r;
        }

        ctx.fillStyle = 'rgb(75,75,75)';
        ctx.fillRect(player_area_x + 8, y + 40, 16, 24);
        ctx.fillStyle = 'rgb(0,0,0)';
        ctx.font = '700 18px Roboto';
        ctx.textBaseline = 'alphabetic';
        if (this.others_value_not_shown_remain_time[i] % 2 === 0) {
          ctx.fillText('x' + this.others_resources_shown_value[i], player_area_x + 28, y + 64);
        }

        ctx.fillStyle = 'rgb(225,225,225)';
        ctx.fillRect(player_area_x + 68, y + 48, 16, 16);
        ctx.fillStyle = 'rgb(0,0,0)';
        ctx.font = '700 18px Roboto';
        ctx.textBaseline = 'alphabetic';
        ctx.fillText('x' + ccard_sum(i), player_area_x + 88, y + 64);

        var cnt = 0;
        for (var j = 0; j < 5; j++) {
          var text = game_info.player_info[i].used_chance_cards[chance_card_order[j]];
          if (text == 0) { continue; }

          var rx = player_area_x + 138 + (18 + 10) * cnt;
          var ry = y + 30;

          ctx.drawImage(chance_other_used_imgs[j], rx, ry);
          ctx.fillStyle = 'rgb(255,255,255)';
          ctx.font = '100 14px Roboto';
          ctx.textBaseline = 'alphabetic';
          var width = ctx.measureText(text).width;
          ctx.fillText(text, rx + (18 - width) / 2, ry + 32);
          ++cnt;
        }
      }
    }

    y += height;
    y += player_area_margin;
  }
};

PlayerInfoArea.prototype.set_bursting = function(those_bursting) {
  this.those_bursting = those_bursting;
}

// Board

/*
 *       0   1   2
 *
 *     3   4   5   6
 *
 *   7   8   9   10  11
 *
 *     12  13  14  15
 *
 *       16  17  18
 */

var board_cell_width = 68;
var board_cell_height = 79;
var board_x = 294;
var board_y = [198.921, 257.811, 316.701, 375.591, 434.480];
var board_value_width = 15;
var board_value_font = '700 20px Roboto';
var board_value_black = 'rgb(0,57,151)';
var board_value_red = 'rgb(222,35,0)';
var board_geometry_info = [
  { x: (board_x + (0.5 * 2 + 0) * board_cell_width), y: board_y[0], lx: 7 , ly: 2 },
  { x: (board_x + (0.5 * 2 + 1) * board_cell_width), y: board_y[0], lx: 11, ly: 2 },
  { x: (board_x + (0.5 * 2 + 2) * board_cell_width), y: board_y[0], lx: 15, ly: 2 },

  { x: (board_x + (0.5 * 1 + 0) * board_cell_width), y: board_y[1], lx: 5 , ly: 4 },
  { x: (board_x + (0.5 * 1 + 1) * board_cell_width), y: board_y[1], lx: 9 , ly: 4 },
  { x: (board_x + (0.5 * 1 + 2) * board_cell_width), y: board_y[1], lx: 13, ly: 4 },
  { x: (board_x + (0.5 * 1 + 3) * board_cell_width), y: board_y[1], lx: 17, ly: 4 },

  { x: (board_x + (0.5 * 0 + 0) * board_cell_width), y: board_y[2], lx: 3 , ly: 6 },
  { x: (board_x + (0.5 * 0 + 1) * board_cell_width), y: board_y[2], lx: 7 , ly: 6 },
  { x: (board_x + (0.5 * 0 + 2) * board_cell_width), y: board_y[2], lx: 11, ly: 6 },
  { x: (board_x + (0.5 * 0 + 3) * board_cell_width), y: board_y[2], lx: 15, ly: 6 },
  { x: (board_x + (0.5 * 0 + 4) * board_cell_width), y: board_y[2], lx: 19, ly: 6 },

  { x: (board_x + (0.5 * 1 + 0) * board_cell_width), y: board_y[3], lx: 5 , ly: 8 },
  { x: (board_x + (0.5 * 1 + 1) * board_cell_width), y: board_y[3], lx: 9 , ly: 8 },
  { x: (board_x + (0.5 * 1 + 2) * board_cell_width), y: board_y[3], lx: 13, ly: 8 },
  { x: (board_x + (0.5 * 1 + 3) * board_cell_width), y: board_y[3], lx: 17, ly: 8 },

  { x: (board_x + (0.5 * 2 + 0) * board_cell_width), y: board_y[4], lx: 7 , ly: 10 },
  { x: (board_x + (0.5 * 2 + 1) * board_cell_width), y: board_y[4], lx: 11, ly: 10 },
  { x: (board_x + (0.5 * 2 + 2) * board_cell_width), y: board_y[4], lx: 15, ly: 10 }
];

// logical <-> display coord translation
var hexagon_vertex_angles = [];
for (var i = 0; i < 6; i++) {
  hexagon_vertex_angles.push((Math.PI / 6) * (i * 2 + 1));
}
var hexagon_edge_angles = [];
for (var i = 0; i < 6; i++) {
  hexagon_edge_angles.push((Math.PI / 6) * (i * 2));
}
var hexagon_edge_slopes = [];
for (var i = 0; i < 6; i++) {
  hexagon_edge_slopes.push((Math.PI / 3) * i);
}
var hexagon_vertex_radius = board_cell_height / 2;
var hexagon_edge_radius = board_cell_width / 2;
var logical_vertex_offset = [
  {x: 2 , y: 1 },
  {x: 0 , y: 1 },
  {x: -2, y: 1 },
  {x: -2, y: -1},
  {x: 0 , y: -1},
  {x: 2 , y: -1}];
var logical_edge_offset = [
  {x: 2 , y: 0 },
  {x: 1 , y: 1 },
  {x: -1, y: 1 },
  {x: -2, y: 0 },
  {x: -1, y: -1},
  {x: 1 , y: -1}];

var hexagon_center_click_radius_thres = 10;
var hexagon_vertex_click_radius_thres = 10;
var hexagon_edge_click_radius_thres = 10;

var hexagon_click_radius_max = Math.max(
    hexagon_vertex_click_radius_thres,
    hexagon_edge_click_radius_thres);

var board_rect = {
  left: board_x - hexagon_click_radius_max,
  top: board_y[0] - hexagon_click_radius_max,
  right: board_x + board_cell_width * 5 + hexagon_click_radius_max,
  bottom: board_y[4] + board_cell_height + hexagon_click_radius_max
};

function Board() {
  CanvasWidget.call(this);
}

inherits(Board, CanvasWidget);

var cell_order = ['forest', 'hill', 'pasture', 'field', 'mountain', 'desert'];
var board_cell_imgs = {};

var land_number_margin_x = 16;
var land_number_margin_y = 16;
var land_number_width = 32;
var land_number_height = 32;
var land_number_margin = 8;

var land_edge_margin_x = 16;
var land_edge_margin_y = 16;
var land_edge_pos_offset_x = -board_cell_width;
var land_edge_pos_offset_y = -(board_y[1] - board_y[0]);

var board_object_img_offsets = {'road': [], 'house': [], 'city': []};
for (var i = 0; i < 4; i++) {
  board_object_img_offsets.road.push(
      {'x': 16, 'y': 16 + (32 + 8) * i,
        'w': 5, 'h': 32});
}
for (var i = 0; i < 4; i++) {
  board_object_img_offsets.house.push(
      {'x': 16 + 5 + 8, 'y': 16 + (32 + 8) * i,
        'w': 24, 'h': 24});
}
for (var i = 0; i < 4; i++) {
  board_object_img_offsets.city.push(
      {'x': 16 + 5 + 24 + 8 * 2, 'y': 16 + (32 + 8) * i,
        'w': 24, 'h': 28});
}

Board.prototype.redraw = function(ctx) {
  ctx.fillStyle = 'rgb(0,0,0)';
  ctx.textBaseline = 'middle';
  ctx.font = board_value_font;

  for (var i = 0; i < 19; i++) {
    var kind = game_info.board_info[i].kind;
    var value = game_info.board_info[i].value;
    var geo = board_geometry_info[i];
    ctx.drawImage(board_cell_imgs[kind], geo.x, geo.y);

    // for robber
    if (i == game_info.robber_pos) {
      var robber_img = robber_pink_img;
      var robber_pos_value = game_info.board_info[game_info.robber_pos].value;
      if (robber_pos_value == 6 || robber_pos_value == 8) {
        robber_img = robber_blue_img;
      }
      ctx.drawImage(robber_img, geo.x, geo.y);
    }

    ctx.drawImage(land_number_img,
        land_number_margin_x +
        (land_number_margin + land_number_width) * (value - 2),
        land_number_margin_y, land_number_width, land_number_height,
        geo.x + (board_cell_width - land_number_width) / 2,
        geo.y + (board_cell_height - land_number_height) / 2,
        land_number_width,
        land_number_height);
  }

  var w = land_edge_img.width - land_edge_margin_x;
  var h = land_edge_img.height - land_edge_margin_y;
  ctx.drawImage(land_edge_img,
      land_edge_margin_x, land_edge_margin_y, w, h,
      board_x + land_edge_pos_offset_x,
      board_y[0] + land_edge_pos_offset_y,
      w, h);

  // for edges
  for (var i = 0; i < 19; i++) {
    var bgi = board_geometry_info[i];
    var hexagon_center_lx = bgi.lx;
    var hexagon_center_ly = bgi.ly;

    for (var j = 0; j < 6; j++) {
      var lx = hexagon_center_lx + logical_edge_offset[j].x;
      var ly = hexagon_center_ly + logical_edge_offset[j].y;
      var cx = bgi.x + board_cell_width / 2;
      var cy = bgi.y + board_cell_height / 2;

      var obj = game_info.object_info[lx][ly];
      if (obj != 0 && obj != null) {
        var img_offset = board_object_img_offsets[obj.kind][obj.player];
        var angle = hexagon_edge_angles[j];
        var ex = cx + hexagon_edge_radius * Math.cos(angle);
        var ey = cy + hexagon_edge_radius * Math.sin(angle);

        var slope = hexagon_edge_slopes[j];
        ctx.translate(ex, ey);
        ctx.rotate(slope);
        ctx.drawImage(board_object_img,
            img_offset.x, img_offset.y, img_offset.w, img_offset.h,
            -img_offset.w / 2, -img_offset.h / 2,
            img_offset.w, img_offset.h);
        ctx.setTransform(1, 0, 0, 1, 0, 0);
      }
    }
  }

  // for vertexes
  for (var i = 0; i < 19; i++) {
    var bgi = board_geometry_info[i];
    var hexagon_center_lx = bgi.lx;
    var hexagon_center_ly = bgi.ly;

    for (var j = 0; j < 6; j++) {
      var lx = hexagon_center_lx + logical_vertex_offset[j].x;
      var ly = hexagon_center_ly + logical_vertex_offset[j].y;
      var cx = bgi.x + board_cell_width / 2;
      var cy = bgi.y + board_cell_height / 2;

      var obj = game_info.object_info[lx][ly];
      if (obj != 0 && obj != null) {
        var img_offset = board_object_img_offsets[obj.kind][obj.player];
        var angle = hexagon_vertex_angles[j];
        var vx = cx + hexagon_vertex_radius * Math.cos(angle);
        var vy = cy + hexagon_vertex_radius * Math.sin(angle);

        ctx.drawImage(board_object_img,
            img_offset.x, img_offset.y, img_offset.w, img_offset.h,
            vx - img_offset.w / 2, vy - img_offset.h / 2,
            img_offset.w, img_offset.h);
      }
    }
  }
};

Board.prototype.click_event = function(evt) {
  var vertex_logical = null;
  var edge_logical = null;
  var center_logical = null;

  // the coord clicked
  var canvas_offset = this.parent_canvas_ui.get_offset();
  var ex = evt.pageX - canvas_offset.left;
  var ey = evt.pageY - canvas_offset.top;

  // if out of bound
  if (!(board_rect.left <= ex && ex <= board_rect.right &&
        board_rect.top <= ey && ey <= board_rect.bottom)) {
    return;
  }

  for (var i = 0; i < 19; i++) {
    var bgi = board_geometry_info[i];
    var cx = bgi.x + board_cell_width / 2;
    var cy = bgi.y + board_cell_height / 2;

    var dx = ex - cx;
    var dy = ey - cy;
    if (dx * dx + dy * dy <= hexagon_center_click_radius_thres *
        hexagon_center_click_radius_thres) {
      center_logical = { x: bgi.lx, y: bgi.ly };
    }
  }

  for (var i = 0; i < 19; i++) {
    // the coord of the center of the hexagon
    var bgi = board_geometry_info[i];
    var cx = bgi.x + board_cell_width / 2;
    var cy = bgi.y + board_cell_height / 2;

    for (var j = 0; j < 6; j++) {
      var angle = hexagon_vertex_angles[j];
      var vx = cx + hexagon_vertex_radius * Math.cos(angle);
      var vy = cy + hexagon_vertex_radius * Math.sin(angle);

      var dx = ex - vx;
      var dy = ey - vy;
      if (dx * dx + dy * dy <= hexagon_vertex_click_radius_thres *
          hexagon_vertex_click_radius_thres) {
        var lo = logical_vertex_offset[j];
        var res = { x: bgi.lx + lo.x, y: bgi.ly + lo.y };
        if (vertex_logical != null &&
            !(vertex_logical.x == res.x && vertex_logical.y == res.y)) {
          console.dir(vertex_logical);
          console.log('WARNING: More than 2 vertexes are matched for the click.');
        }
        vertex_logical = res;
      }
    }

    for (var j = 0; j < 6; j++) {
      var angle = hexagon_edge_angles[j];
      var vx = cx + hexagon_edge_radius * Math.cos(angle);
      var vy = cy + hexagon_edge_radius * Math.sin(angle);

      var dx = ex - vx;
      var dy = ey - vy;
      if (dx * dx + dy * dy <= hexagon_edge_click_radius_thres *
          hexagon_edge_click_radius_thres) {
        var lo = logical_edge_offset[j];
        var res = { x: bgi.lx + lo.x, y: bgi.ly + lo.y };
        if (edge_logical != null &&
            !(edge_logical.x == res.x && edge_logical.y == res.y)) {
          console.log('WARNING: More than 2 edges are matched for the click.');
        }
        edge_logical = res;
      }
    }
  }

  // if not matched
  if (vertex_logical == null && edge_logical == null && center_logical == null) {
    return;
  }

  on_board_clicked(vertex_logical, edge_logical, center_logical);
};

var hx, hy;

function on_board_clicked(vertex_logical, edge_logical, center_logical) {
  if (states.game.state == 'house' && vertex_logical != null) {
    var x = vertex_logical.x;
    var y = vertex_logical.y;
    if (Rule.is_able_to_put_house(game_info.object_info, x, y, game_info.me, false)) {
      send_put_house(vertex_logical.x, vertex_logical.y);
    } else {
      alert('おけない');
    }
  } else if (states.game.state == 'house_begin' && vertex_logical != null) {
    var x = vertex_logical.x;
    var y = vertex_logical.y;
    if (Rule.is_able_to_put_house(game_info.object_info, x, y, game_info.me, true)) {
      send_put_house_begin(vertex_logical.x, vertex_logical.y);
    } else {
      alert('おけない');
    }
  } else if (states.game.state == 'road' && edge_logical != null) {
    var x = edge_logical.x;
    var y = edge_logical.y;
    if (Rule.is_able_to_put_road(game_info.object_info, x, y, game_info.me)) {
      send_put_road(edge_logical.x, edge_logical.y);
    } else {
      alert('おけない');
    }
  } else if (states.game.state == 'road_begin' && edge_logical != null) {
    var x = edge_logical.x;
    var y = edge_logical.y;
    if (Rule.is_able_to_put_road_at_the_beginning(game_info.object_info, x, y, game_info.me, hx, hy)) {
      send_put_road_begin(edge_logical.x, edge_logical.y);
    } else {
      alert('おけない');
    }
  } else if (states.game.state == 'knight' && center_logical != null) {
    send_knight(center_logical.x, center_logical.y);
  } else if (states.game.state == 'target_of_robber' && vertex_logical != null) {
    send_target_of_robber(vertex_logical.x, vertex_logical.y);
  } else if (states.game.state == 'city' && vertex_logical != null) {
    var x = vertex_logical.x;
    var y = vertex_logical.y;
    if (Rule.is_able_to_put_city(game_info.object_info, x, y, game_info.me)) {
      send_put_city(vertex_logical.x, vertex_logical.y);
    } else {
      alert('おけない');
    }
  }
}

var player_choice_list = [1, 2, 3];

function gen_button_player_choice_img() {
  player_choice_list = [];

  var ctx = button_player_choice_whole_img.getContext('2d');
  var y = 0;
  for (var i = 0; i < 4; i++) {
    if (i != game_info.me) {
      ctx.drawImage(button_player_choice_img_src, 0, i * 48, 144, 48, 0, y, 144, 48);
      trade_player_choice_buttons[i].x = 284;
      trade_player_choice_buttons[i].y = 544 + y;
      trade_player_choice_button_next_animes[i].pos_x = 284;
      trade_player_choice_button_next_animes[i].pos_y = 544 + y;
      trade_player_choice_button_expand_animes[i].s = [544 + y, 544 + y + 48, 284, 284 + 144];
      player_choice_list.push(i);

      y += 48;
    }
  }

  for (var i = 0; i < 4; i++) {
    if (game_info.player_info[i].name == null) {
      continue;
    }

    var ctx = button_player_choice_imgs1[i].getContext('2d');
    ctx.drawImage(button_player_choice_imgs[i], 0, 0);
    ctx.font = player_area_name_font;
    ctx.fillStyle = player_area_name_color;
    ctx.textBaseline = 'alphabetic';
    ctx.fillText(game_info.player_info[i].name, 24, 32);
  }

  for (var i = 0; i < 4; i++) {
    if (game_info.player_info[i].name == null) {
      continue;
    }

    var ctx = trade_form_player_imgs1[i].getContext('2d');
    ctx.drawImage(trade_form_player_imgs[i], 0, 0);
    ctx.font = player_area_name_font;
    ctx.fillStyle = player_area_name_color;
    ctx.textBaseline = 'alphabetic';
    ctx.fillText(game_info.player_info[i].name, 8, 24);
  }
}

var button_card_kind_img = document.createElement('canvas');
button_card_kind_img.width = 144;
button_card_kind_img.height = 144;

function gen_button_card_kind_img() {
  for (var i = 0; i < 4; i++) {
    var p = four_squares[i];
    var ctx = button_card_kind_img.getContext('2d');
    var card_kind = chance_card_button_order[i];
    var my_info = game_info.player_info[game_info.me];
    var img = null;
    if (my_info.chance_cards[card_kind] > 0) {
      img = button_card_kind_available_imgs[i];
    } else {
      img = button_card_kind_unavailable_imgs[i];
    }
    ctx.drawImage(img, p.x, p.y);
  }
}

function menu_is_available(obj) {
  var my_info = game_info.player_info[game_info.me];
  return _.every(resource_order, function(k) {
    return my_info.resources[k] >= develop_recipe[obj][k];
  });
}

var button_consume_kind_img = document.createElement('canvas');
button_card_kind_img.width = 144;
button_card_kind_img.height = 144;

var consume_button_order = ['house', 'road', 'city', 'card'];

function gen_button_consume_kind_img() {
  for (var i = 0; i < 4; i++) {
    var p = four_squares[i];
    var ctx = button_consume_kind_img.getContext('2d');
    var menu_kind = consume_button_order[i];
    var img = null;
    if (menu_is_available(menu_kind)) {
      img = consume_kind_button_available_imgs[i];
    } else {
      img = consume_kind_button_unavailable_imgs[i];
    }
    ctx.drawImage(img, p.x, p.y);
  }
}
function MonopolyForm() {
  CanvasWidget.call(this);

  var that = this;

  this.x = 620;
  this.y = 544;
  this.bg_image = document.getElementById('monopoly_form');
  this.width = this.bg_image.width;
  this.height = this.bg_image.height;

  var dummy_image = document.createElement('canvas');
  dummy_image.width = 32;
  dummy_image.height = 32;

  this.buttons = [];
  for (var i = 0; i < 5; i++) {
    var b = new CanvasButton(
        this.x + 8 + (32 + 8) * i, this.y + 44,
        32, 32,
        dummy_image, dummy_image, null);
    b.set_visibility(false);
    b.set_activeness(false);
    b.index = i;
    (function(bb) {
      bb.set_handler(function() {
        that.handler(bb.index);
      });
    })(b);
    this.buttons.push(b);
  }
}

inherits(MonopolyForm, CanvasWidget);

MonopolyForm.prototype.redraw = function(ctx) {
  ctx.drawImage(this.bg_image, this.x, this.y);
};

MonopolyForm.prototype.set_parent_canvas_ui = function(ui) {
  this.parent_canvas_ui = ui;
  this.buttons.forEach(function(x) { ui.add_object(x, 4); });
};

MonopolyForm.prototype.set_activeness = function(active) {
  this.active = active;
  this.buttons.forEach(function(x) { x.set_activeness(active); });
};

MonopolyForm.prototype.set_visibility = function(visible) {
  this.visible = visible;
  this.buttons.forEach(function(x) { x.set_visibility(visible); });
};

MonopolyForm.prototype.set_handler = function(handler) {
  this.handler = handler;
};

MonopolyForm.prototype.click_event = function(evt) {
  var canvas_offset = this.parent_canvas_ui.get_offset();
  var x = evt.pageX - (canvas_offset.left + this.x);
  var y = evt.pageY - (canvas_offset.top + this.y);
  var that = this;

  if (!(0 <= x && x < this.width &&
      0 <= y && y < this.height)) {
    states.card_button.transit('top');
    return false;
  }
  // else

  return true;
};

function HarvestForm(button_img) {
  CanvasWidget.call(this);

  var that = this;

  this.x = 620;
  this.y = 544;
  this.bg_image = document.getElementById('harvest_form');
  this.width = this.bg_image.width;
  this.height = this.bg_image.height;
  this.sbs = [];
  this.sum = 0;

  function sbs_handler(is_up) {
    if (is_up) {
      if (that.sum < 2) {
        ++this.value;
        ++that.sum;
      }
    } else {
      if (this.value >= 1) {
        --this.value;
        --that.sum;
      }
    }
  }

  for (var i = 0; i < 5; i++) {
    this.sbs.push(new CanvasSpinBox(this.x + 8 + (8 + 32) * i, this.y + 42, 4));
  }

  this.sbs.forEach(function(x) { x.on_value_changed = sbs_handler; });

  this.decide_button = new CanvasButton(this.x + 112, this.y + 81,
      button_img.width, button_img.height,
      button_img, button_img, null);
  this.decide_button.set_mousedown_img(document.getElementById('trade_button_mousedown'));
}

inherits(HarvestForm, CanvasWidget);

HarvestForm.prototype.reset = function() {
  this.sum = 0;
  this.sbs.forEach(function(x) { x.value = 0; });
};

HarvestForm.prototype.redraw = function(ctx) {
  ctx.drawImage(this.bg_image, this.x, this.y);
};

HarvestForm.prototype.set_parent_canvas_ui = function(ui) {
  this.parent_canvas_ui = ui;
  this.sbs.forEach(function(x) { ui.add_object(x, 4); });
  ui.add_object(this.decide_button, 4);
};

HarvestForm.prototype.set_activeness = function(active) {
  this.active = active;
  this.sbs.forEach(function(x) { x.set_activeness(active); });
  this.decide_button.set_activeness(active);
};

HarvestForm.prototype.set_visibility = function(visible) {
  this.visible = visible;
  this.sbs.forEach(function(x) { x.set_visibility(visible); });
  this.decide_button.set_visibility(visible);
};

HarvestForm.prototype.click_event = function(evt) {
  var canvas_offset = this.parent_canvas_ui.get_offset();
  var x = evt.pageX - (canvas_offset.left + this.x);
  var y = evt.pageY - (canvas_offset.top + this.y);
  var that = this;

  if (!(0 <= x && x < this.width &&
      0 <= y && y < this.height)) {
    states.card_button.transit('top');
    return false;
  }
  // else

  return true;
};

HarvestForm.prototype.set_handler = function(handler) {
  this.decide_button.set_handler(handler);
};

HarvestForm.prototype.reset = function() {
  this.sbs.forEach(function(x) { x.value = 0; });
};

var trade_confirm_form_img = document.createElement('canvas');
trade_confirm_form_img.width = 400;
trade_confirm_form_img.height = 104;

var trade_confirm_form_img1 = document.createElement('canvas');
trade_confirm_form_img1.width = 400;
trade_confirm_form_img1.height = 104;


function TradeConfirmForm() {
  CanvasWidget.call(this);

  var bw = trade_button_decline_img.width;
  var bh = trade_button_decline_img.height;
  this.decline_button = new CanvasButton(536, 648, bw, bh,
      trade_button_decline_img,
      trade_button_decline_img,
      null);
  this.decline_button.set_mousedown_img(trade_button_decline_pushed_img);
  this.accept_button = new CanvasButton(600, 648, bw, bh,
      trade_button_accept_img,
      trade_button_accept_img,
      null);
  this.accept_button.set_mousedown_img(trade_button_accept_pushed_img);
}

inherits(TradeConfirmForm, CanvasWidget);

TradeConfirmForm.prototype.redraw = function(ctx) {
  ctx.drawImage(trade_confirm_form_img1, 264, 576);
};

TradeConfirmForm.prototype.set_parent_canvas_ui = function(ui) {
  this.parent_canvas_ui = ui;
  ui.add_object(this.accept_button, 4);
  ui.add_object(this.decline_button, 4);
};

TradeConfirmForm.prototype.set_activeness = function(active) {
  this.active = active;

  this.accept_button.set_activeness(active);
  this.decline_button.set_activeness(active);
};

TradeConfirmForm.prototype.set_visibility = function(visible) {
  this.visible = visible;

  this.accept_button.set_visibility(visible);
  this.decline_button.set_visibility(visible);
};

TradeConfirmForm.prototype.set_accept_handler = function(handler) {
  this.accept_button.set_handler(handler);
};

TradeConfirmForm.prototype.set_decline_handler = function(handler) {
  this.decline_button.set_handler(handler);
};

function gen_trade_confirm_message(d) {
  var partner_name = game_info.player_info[d.partner].name;
  var exs = d['export'];
  var ins = d['import'];
  return sprintf('{0} が交易を申し込んできました\n' +
      '(出) 木:{1} 土:{2} 羊:{3} 麦:{4} 鉄:{5}\n' +
      '(求) 木:{6} 土:{7} 羊:{8} 麦:{9} 鉄:{10}',
      partner_name,
      exs[0], exs[1], exs[2], exs[3], exs[4],
      ins[0], ins[1], ins[2], ins[3], ins[4]);
}

function BurstForm(button_img, button_inactive_img, button_mousedown_img) {
  CanvasWidget.call(this);

  var that = this;

  this.x = 344;
  this.y = 544;
  this.bg_image = document.getElementById('burst_form');
  this.width = this.bg_image.width;
  this.height = this.bg_image.height;
  this.sbs = [];
  this.num_to_dispose = 0;
  this.sum = 0;

  for (var i = 0; i < 5; i++) {
    this.sbs.push(new CanvasSpinBox(this.x + 24 + (8 + 32) * i, this.y + 42, 4));
  }

  function gen_handler(kind) {
    return (function(is_up) {
      var my_info = game_info.player_info[game_info.me];
      if (is_up) {
        if (that.sum < that.num_to_dispose &&
            this.value + 1 <= my_info.resources[kind]) {
          ++this.value;
          ++that.sum;

          that.decide_button.set_activeness(
              that.sum == that.num_to_dispose);
          ui.redraw();
        }
      } else {
        if (this.value > 0) {
          --this.value;
          --that.sum;
        }
        that.decide_button.set_activeness(
            that.sum == that.num_to_dispose);
        ui.redraw();
      }

    });
  }

  for (var i = 0; i < 5; i++) {
    this.sbs[i].on_value_changed = gen_handler(resource_order[i]);
  }

  this.decide_button = new CanvasButton(this.x + 136, this.y + 81,
      button_img.width, button_img.height,
      button_img, button_img, null);
  this.decide_button.inactive_img = button_inactive_img;
  this.decide_button.set_mousedown_img(button_mousedown_img);

  this.decide_button.redraw = function(ctx) {
    CanvasButton.prototype.redraw.call(this, ctx);

    if (this.visible) {
      if (!this.active) {
        var x = this.x + 44;
        var y = this.y + 18;
        ctx.font = '500 18px Roboto';
        ctx.fillStyle = 'rgb(255,255,255)';
        ctx.fillText(that.num_to_dispose - that.sum, x, y);
      }
    }
  };
}

inherits(BurstForm, CanvasWidget);

BurstForm.prototype.redraw = function(ctx) {
  ctx.drawImage(this.bg_image, this.x, this.y);
};

BurstForm.prototype.set_parent_canvas_ui = function(ui) {
  this.parent_canvas_ui = ui;
  this.sbs.forEach(function(x) { ui.add_object(x, 4); });
  ui.add_object(this.decide_button, 4);
};

BurstForm.prototype.set_activeness = function(active) {
  this.active = active;
  this.sbs.forEach(function(x) { x.set_activeness(active); });
  if (!active) {
    this.decide_button.set_activeness(false);
  }
};

BurstForm.prototype.set_visibility = function(visible) {
  this.visible = visible;
  this.sbs.forEach(function(x) { x.set_visibility(visible); });
  this.decide_button.set_visibility(visible);
};

BurstForm.prototype.set_handler = function(handler) {
  this.decide_button.set_handler(handler);
};

BurstForm.prototype.reset = function(num) {
  this.sbs.forEach(function(x) { x.value = 0; });
  this.sum = 0;
  this.num_to_dispose = num;
  this.decide_button.set_activeness(false);
};

// Sounds
function play_notification() {
  var x = document.getElementById('notification_sound');
  x.play();
}

function play_important_notification() {
  var x = document.getElementById('notification_sound2');
  x.play();
}

function play_burst_notification() {
  var x = document.getElementById('notification_sound3');
  x.play();
}

// Card kind button

function gen_damped_oscillation(num) {
  return function(r) {
    return Math.sin(num * (r * 2 * Math.PI));
  }
}

function comment_out_tags(s) {
  return s.replace(/</, '&lt;').replace(/>/, '&gt;');
}

function gen_message_html(player_id, name, message) {
  var s = '<div>' +
      name + ': ' + comment_out_tags(message) +
      '</div>';
  if (player_id == null) {
    return s;
  }

  return player_color_span(player_id, s);
}

function gen_system_log_html(log) {
  return '<div>' + log + '</div>';
}

function gen_roommember_html(members) {
  return '<div id="roommember_area">' +
      members.join('<br/>') +
      '</div>';
}

function player_color_span(player_id, html) {
  if (player_id == null) {
    return html;
  }

  return sprintf('<span class="player{0}">{1}</span>', player_id, html);
}

function gen_consume_message(d) {
  var kind_j = consume_kind_e2j[d.kind];
  switch (d.kind) {
    case 'house':
    case 'road':
    case 'city':
      return player_color_span(d.player_id, kind_j + 'を建設');
    case 'card':
      return player_color_span(d.player_id, 'チャンスカードをドロー');
    case 'knight':
    case 'building_road':
      return player_color_span(d.player_id, '【' + kind_j + '】を使用');
    case 'harvest':
      var b = '【' + kind_j + '】を使用<br/>';
      if (d.resource_kinds[0] == d.resource_kinds[1]) {
        b += resource_kind_e2j[resource_order[0]] + '2を収穫';
      } else {
        b += resource_kind_e2j[resource_order[0]] +
            'と' +
                    resource_kind_e2j[resource_order[1]] +
                    'を収穫';
      }
      return player_color_span(d.player_id, b);
    case 'monopoly':
      var b = '【' + kind_j + '】を使用<br/>';
      b += resource_kind_e2j[d.resource_kind] + 'を独占';
      return player_color_span(d.player_id, b);
  }
}

function gen_trade_suggestion_message(d) {
  var partner_name = game_info.player_info[d.partner_id].name;
  var player_name = game_info.player_info[d.player_id].name;

  var exsa = [];
  var insa = [];
  for (var i = 0; i < 5; i++) {
    var r = resource_kind_e2j[resource_order[i]];
    if (d.exs[i] > 0) {
      exsa.push(r + d.exs[i]);
    }
    if (d.ins[i] > 0) {
      insa.push(r + d.ins[i]);
    }
  }

  var s = sprintf('{0}が{1}に国内取引を提案<br/>出:{2}<br/>求:{3}<br/>',
      player_name, partner_name, exsa.join(), insa.join());

  return player_color_span(d.player_id, s);
}

function gen_trade_result_message(d) {
  var partner_name = game_info.player_info[d.partner_id].name;
  var player_name = game_info.player_info[d.player_id].name;

  var s = sprintf('{0}が{1}との取引を{2}',
      partner_name, player_name, (d.ok ? '承諾' : '拒否'));

  return player_color_span(d.player_id, s);
}

function gen_trade_oversea_message(d) {
  var player_name = game_info.player_info[d.player_id].name;

  var exsa = [];
  var insa = [];
  for (var i = 0; i < 5; i++) {
    var r = resource_kind_e2j[resource_order[i]];
    if (d.exs[i] > 0) {
      exsa.push(r + d.exs[i]);
    }
    if (d.ins[i] > 0) {
      insa.push(r + d.ins[i]);
    }
  }

  var s = sprintf('{0}が海外取引で{1}を{2}に交換',
      player_name,
      exsa.join(), insa.join());

  return player_color_span(d.player_id, s);
}

function gen_largest_army_changed_message(d) {
  var player_name = game_info.player_info[d.player_id].name;

  var s = sprintf('{0}がラージェスト・アーミーを獲得', player_name);
  return player_color_span(d.player_id, s);
}

function gen_longest_road_changed_message(d) {
  var player_name = game_info.player_info[d.player_id].name;

  var s = sprintf('{0}がロンゲスト・ロードを獲得', player_name);
  return player_color_span(d.player_id, s);
}

function gen_robbery_message(d) {
  if (d.target_id == null) {
    return player_color_span(d.player_id, '盗賊を移動');
  } else {
    var target_name = game_info.player_info[d.target_id].name;
    return player_color_span(
        d.player_id, sprintf('盗賊を移動<br/>{0}から資源を強奪', target_name));
  }
}

function gen_burst_message(d) {
  var burst_names = [];
  for (var i = 0; i < d.length; i++) {
    if (d[i].is_burst) {
      burst_names.push(game_info.player_info[i].name);
    }
  }

  return sprintf('{0}がバースト', burst_names.join());
}

function gen_dispose_message(d) {
  var player_name = game_info.player_info[d.player_id].name;

  var e = [];
  for (var i = 0; i < 5; i++) {
    var r = resource_kind_e2j[resource_order[i]];
    if (d.amounts[i] > 0) {
      e.push(r + d.amounts[i]);
    }
  }

  var s = sprintf('{0}が{1}を破棄', player_name, e.join());
  return player_color_span(d.player_id, s);
}

function gen_room_member_changed_message(d) {
  var member = d.member;

  var s;
  if (d.is_joined) {
    s = sprintf('{0}が入室', member);
  } else {
    s = sprintf('{0}が退室', member);
  }

  var player_id = null;
  for (var i = 0; i < 4; i++) {
    if (game_info.player_info[i].name == member) {
      player_id = i;
    }
  }

  return player_color_span(player_id, s);
}

function append_to_system_log_area(s) {
  var o = $('#systemlog_area');

  o.append(gen_system_log_html(s));

  var bot = o.scrollTop() + o.height();
  o.animate({scrollTop: bot}, 100);
}

function append_to_chat_area(s) {
  var o = $('#chat_area');

  o.append(gen_system_log_html(s));

  var bot = o.scrollTop() + o.height();
  o.animate({scrollTop: bot}, 100);
}

function on_player_changed(d) {
  game_info.player_list = d.player_list;
  ui.redraw();
}

function on_board_changed(d) {
  game_info.board_info = d.board_info;
  ui.redraw();
}

function on_object_changed(d) {
  game_info.object_info = d.object_info;
  play_notification();
  ui.redraw();
}

function on_dispose(d) {
  append_to_system_log_area(gen_dispose_message(d));
  _.remove(pi.those_bursting, function(x) { return x === d.player_id; });
  ui.redraw();
}

function on_waiting(d) {
  if (_.includes(d.waiting_player_ids, game_info.me)) {
    switch (d.waiting_command) {
      case 'put_house_begin':
        states.game.transit('house_begin');
        break;
      case 'put_house':
        states.game.transit('house');
        break;
      case 'put_road_begin':
        states.game.transit('road_begin');
        hx = d.x;
        hy = d.y;
        break;
      case 'put_road':
        states.game.transit('road');
        break;
      case 'victory':
        states.game.transit('victory');
        play_important_notification();
        break;
      case 'diceroll':
        states.game.transit('diceroll');
        play_important_notification();
        break;
      case 'knight':
        states.game.transit('knight');
        play_important_notification();
        break;
      case 'target_of_robber':
        states.game.transit('target_of_robber');
        break;
      case 'trade_acceptance':
        states.game.transit('trade_acceptance', {d: d});
        break;
      case 'any':
        states.game.transit('any');
        break;
      case 'burst':
        var num = d.burst_info[game_info.me].num_to_dispose;
        if (num > 0) {
          burst_form.reset(num);
          states.game.transit('burst');
        }
        ui.redraw();
        break;
      default:
        console.log('Unknown waiting command :' + d.waiting_command);
        break;
    }
  } else {
    states.game.transit('none');
  }

  switch (d.waiting_command) {
    case 'knight':
      pi.set_bursting([]);
      break;
    case 'burst':
      var those_bursting = _.filterIndex(d.burst_info,
        function (x) { return x.num_to_dispose > 0; });
      pi.set_bursting(those_bursting);
      break;
  }

  if (d.waiting_command == 'diceroll') {
    var name = game_info.player_info[d.waiting_player_ids[0]].name;
    append_to_system_log_area(name + 'のターン');
  } else if (d.waiting_command == 'burst') {
    append_to_system_log_area(gen_burst_message(d.burst_info));
  }

  game_info.turn = d.waiting_player_ids[0];
  ui.redraw();
}

function on_player_changed(d) {
  console.log(JSON.stringify(d.data));
  for (var i = 0; i < 4; i++) {
    $.extend(game_info.player_info[i], d.data[i]);
  }
  go_with_setup(gen_button_card_kind_img);
  go_with_setup(gen_button_consume_kind_img);
  ui.redraw();
}

function find_member_by_user_id(id) {
  return _.find(room_members, function(x) { return x.user_id === id; });
}

function on_game_start(d) {
  switch_screen(true);
  for (var i = 0; i < 4; i++) {
    if (my_user_id === d.players[i]) {
      game_info.me = i;
    }

    if (d.players.length > i) {
      game_info.player_info[i].name = find_member_by_user_id(d.players[i]).name;
    } else {
      game_info.player_info[i].name = null;
    }
  }
  game_info.turn = 0;
  go_with_setup(gen_button_player_choice_img);
  ui.redraw();
  append_to_system_log_area('ゲームスタート');
}

function on_diceroll(d) {
  if (d.dice[0] + d.dice[1] == 7) {
    ui.do_action_list([
      function() {
              di.set_visibility(false);
      },
      [diceroll_switch_anime1, diceroll_switch_anime2],
      function() {
              di.set_value(d.dice[0], d.dice[1]);
              di.set_visibility(true);
              append_to_system_log_area('ダイス: ' + (d.dice[0] + d.dice[1]));
      },
      function() {
        states.robber_screen.transit('show');
      }
    ]);
  } else {
    ui.do_action_list([
      function() {
                di.set_visibility(false);
      },
      [diceroll_switch_anime1, diceroll_switch_anime2],
      function() {
                di.set_value(d.dice[0], d.dice[1]);
                di.set_visibility(true);
                append_to_system_log_area('ダイス: ' + (d.dice[0] + d.dice[1]));
                play_notification();
      }
    ]);
  }
}

function on_robber_moved(d) {
  game_info.robber_pos = d.hex_id;
  play_notification();
  ui.redraw();
}

function on_robbery(d) {
  states.robber_screen.transit('none');
  append_to_system_log_area(gen_robbery_message(d));
  play_notification();
  ui.redraw();
}

function on_trade_result(d) {
  append_to_system_log_area(gen_trade_result_message(d));
  play_notification();
}

function on_trade_suggestion(d) {
  append_to_system_log_area(gen_trade_suggestion_message(d));
  play_important_notification();
}

function on_trade_oversea(d) {
  append_to_system_log_area(gen_trade_oversea_message(d));
  play_notification();
}

function on_longest_road_changed(d) {
  append_to_system_log_area(gen_longest_road_changed_message(d));
}

function on_largest_army_changed(d) {
  append_to_system_log_area(gen_largest_army_changed_message(d));
}

function append_chat_g(player_id, name, message) {
  append_to_chat_area(gen_message_html(player_id, name, message));
  play_notification();
}

function on_room_member_changed(d) {
  $('#roommember_area').replaceWith(
      gen_roommember_html(d.room_member_list));
  append_to_chat_area(gen_room_member_changed_message(d));
}

function on_victory(d) {
  states.game.transit('none');
  var n = game_info.player_info[d.player_id].name;
  append_to_system_log_area(n + 'が勝利');
  document.getElementById('notification_sound4').play();
  $('#back_button').css('display', 'block');
}

function on_shortage(d) {
  append_to_system_log_area(d.kind + 'の不足、' + d.kind + 'は配られませんでした');
}

function on_consume(d) {
  append_to_system_log_area(gen_consume_message(d));
}

function on_resource_deck_info(d) {
  if (d.on_diceroll) {
    setTimeout(function() { game_info.deck_info = d.deck; }, 500);
  } else {
    game_info.deck_info = d.deck;
  }
}

function send_put_house(x, y) {
  ws_disp.trigger('command', {'command': 'put_house', 'x': x, 'y': y});
}

function send_put_city(x, y) {
  ws_disp.trigger('command', {'command': 'put_city', 'x': x, 'y': y});
}

function send_put_house_begin(x, y) {
  ws_disp.trigger('command', {'command': 'put_house_begin', 'x': x, 'y': y});
}

function send_put_road(x, y) {
  ws_disp.trigger('command', {'command': 'put_road', 'x': x, 'y': y});
}

function send_put_road_begin(x, y) {
  ws_disp.trigger('command', {'command': 'put_road_begin', 'x': x, 'y': y});
}

function send_diceroll() {
  ws_disp.trigger('command', {'command': 'diceroll'});
}

function send_oversea_trade(exs, ins) {
  ws_disp.trigger('command', {'command': 'trade_oversea',
    'export': exs,
    'import': ins });
}

function send_local_trade(player_id, exs, ins) {
  ws_disp.trigger('command',
      {'command': 'trade_local',
        'partner': player_id,
        'export': exs,
        'import': ins });
}

function send_generate_card() {
  ws_disp.trigger('command', {'command': 'generate_card'});
}

function send_knight(x, y) {
  ws_disp.trigger('command', {'command': 'knight', 'x': x, 'y': y });
}

function send_target_of_robber(x, y) {
  ws_disp.trigger('command', {'command': 'target_of_robber', 'x': x, 'y': y });
}

function send_acceptance(ok) {
  ws_disp.trigger('command', {'command': 'trade_acceptance', 'ok': ok});
}

function send_card_knight() {
  ws_disp.trigger('command', {'command': 'use_card', 'kind': 'knight'});
}

function send_card_road() {
  ws_disp.trigger('command', {'command': 'use_card', 'kind': 'building_road'});
}

function send_card_monopoly(kind) {
  ws_disp.trigger('command', {'command': 'use_card', 'kind': 'monopoly', 'resource_kind': kind});
}

function send_card_harvest(nums) {
  var p = [];
  for (var c = 0; c < 2; c++) {
    for (var i = 0; i < 5; i++) {
      if (nums[i] > 0) {
        nums[i]--;
        var k = resource_order[i];
        p.push(k);
      }
    }
  }
  ws_disp.trigger('command', {'command': 'use_card', 'kind': 'harvest', 'resource_kinds': p});
}

function send_end() {
  ws_disp.trigger('command', {'command': 'end'});
}

function send_victory() {
  ws_disp.trigger('command', {'command': 'victory'});
}

function send_burst(nums) {
  ws_disp.trigger('command', {'command': 'burst', 'amounts': nums });
}

function send_chat(message) {
  ws_disp.trigger('chat', {'command': 'chat', 'message': message});
}

// Chat
$(function() {
  $('#chat_textbox').keypress(function(evt) {
    if (evt.keyCode == 13) {
      var input = $(this).get(0);
      var message = input.value;
      if (message.length > 0) {
        send_chat(message);
        input.value = '';
      }
    }
  });
});


// State

var LA = make_lazy_action;
var BA = make_bound_action;

function action_appearance() {
  var args = Array.prototype.slice.call(arguments);
  return ({
    reach: BA(function() { _.each(args, function(x) { x.appear(); }); }),
    leave: BA(function() { _.each(args, function(x) { x.disappear(); }); })
  });
}

function action_custom_appearance(kwargs) {
  return ({
    reach: BA(function() {
      _.each(kwargs.reachOnly, function(x) { x.appear(); });
      _.each(kwargs.both, function(x) { x.appear(); });
    }),
    leave: BA(function() {
      _.each(kwargs.leaveOnly, function(x) { x.disappear(); });
      _.each(kwargs.both, function(x) { x.disappear(); });
    }),
  });
}

function action_enable(x) {
  return ({
    reach: BA(function() { x.set_activeness(true); }),
    leave: BA(function() { x.set_activeness(false); })
  });
}

function action_reach(f) {
  return ({ reach: BA(f) });
}

function set_button_description(b, m, m_inactive) {
  b.set_mouse_on_handler(function() {
    $('#hint_area').html(m);
  });
  b.set_mouse_off_handler(function() {
    $('#hint_area').html(global_hint);
  });
  if (m_inactive) {
    b.set_mouse_on_inactive_handler(function() {
      if (b.visible) {
        $('#hint_area').html(m_inactive);
      }
    });
    b.set_mouse_off_inactive_handler(function() {
      if (b.visible) {
        $('#hint_area').html(global_hint);
      }
    });
  }
}

function update_global_hint(m) {
  global_hint = m;
  $('#hint_area').html(global_hint);
}
