//= require lodash
//= require jquery
//= require jquery.cookie
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require websocket_rails/main
//= require dropit
//= require canvas_ui
//= require catan_rule
//= require catan_ui

function sprintf(format) {
  for (var i = 1; i < arguments.length; i++) {
    var r = new RegExp('\\{' + (i - 1) + '\\}');
    format = format.replace(r, arguments[i]);
  }
  return format;
}

var session_id = $.cookie('session_id');
var room_id = location.pathname.match(/[^\/]+/g).pop();

var ws_uri = sprintf('{0}/websocket', location.host);
var ws_disp = new WebSocketRails(ws_uri);

var room_members = [];
var player_list = [];
var not_player_list = [];

var have_authority = false;
var my_user_id = null;

$(function() {
  // Setup user interface
  $('.menu').dropit();
  $('#start_button').click(on_start_button_clicked);
  $('#back_button').click(on_back_button_clicked);

  setup_sortable();
});

function go_to_root() {
  location.href = '/';
}

function on_connection_closed(d) {
  alert('Connection closed\n' + JSON.stringify(d));
  go_to_root();
}

function on_connection_error(d) {
  alert('Connection error\n' + JSON.stringify(d));
  go_to_root();
}

function on_joined(d) {
  my_user_id = d.user_id;
  d.chat_log.forEach(function(x) {
    on_chat(x);
  });
  if (d.is_playing) {
  }
}

function on_password_required(d) {
  var pass = prompt('Password');
  if (pass === null) {
    go_to_root();
    return; // NOTREACHED
  }

  ws_disp.trigger('join',
      { session_id: session_id, room_id: room_id, password: pass });
}

function on_room_member_changed(d) {
  update_room_members(d);

  have_authority = (d.owner_id == my_user_id);

  $('.privileged_item').css('display', have_authority ? 'block' : 'none');
  $('.unprivileged_item').css('display', !have_authority ? 'block' : 'none');
}

function player_list_element(name, user_id) {
  return $('<li/>').text(name).attr('id', user_id).addClass('sortable_player_name');
}

var sortable_initiaized = false;
function setup_sortable() {
  if (have_authority) {
    sortable_initiaized = true;
    $('#player_list, #not_player_list').sortable({
      connectWith: '.room_member_list',
      update: on_room_member_list_changed
    }).disableSelection();
  } else if(sortable_initiaized) {
    sortable_initiaized = false;
    $('#player_list, #not_player_list').sortable('destroy');
  }
}

function update_room_members(d) {
  room_members = d.room_member_list;
  player_list = d.players;
  not_player_list = d.not_players;
  $('#player_list li').remove();
  $('#not_player_list li').remove();

  player_list.forEach(function(x) {
    var m = find_member_by_user_id(x);
    if (m) {
      $('#player_list').append(player_list_element(m.name, m.user_id));
    }
  });
  not_player_list.forEach(function(x) {
    var m = find_member_by_user_id(x);
    if (m) {
      $('#not_player_list').append(player_list_element(m.name, m.user_id));
    }
  });

  setup_sortable();
}

function on_room_member_list_changed() {
  var player_user_id_list = $('#player_list').sortable('toArray');
  var not_player_user_id_list = $('#not_player_list').sortable('toArray');

  ws_disp.trigger('change_player_list',
      { player_list: player_user_id_list,
        not_player_list: not_player_user_id_list });
}

function on_start_button_clicked() {
  ws_disp.trigger('game_start', {});
}

function on_back_button_clicked() {
  switch_screen(false);
  $('#back_button').css('display', 'none');
}

function switch_screen(is_playing) {
  $('#loading_screen').css('display', 'none');
  $('#playing_screen').css('display', (is_playing ? 'block' : 'none'));
  $('#lobby_screen').css('display', (!is_playing ? 'block' : 'none'));
}

function on_chat(d) {
  append_chat_l(d.player_id, d.name, d.message);
  append_chat_g(d.player_id, d.name, d.message);
}

function append_chat_l(player_id, name, message) {
  var s = gen_message_html(player_id, name, message);
  var o = $('#chat_view_l');

  o.append(gen_system_log_html(s));

  var bot = o.scrollTop() + o.height();
  o.animate({scrollTop: bot}, 100);
}

$(function() {
  $('#chat_textbox_l').keypress(function(evt) {
        if (evt.keyCode == 13) {
      var input = $(this).get(0);
      var message = input.value;
      if (message.length > 0) {
        send_chat(message);
        input.value = '';
      }
        }
  });
});

function show_authority_transfer_form() {
  $('#at_new_owner_list').empty();
  for (var i = 0; i < room_members.length; i++) {
    var ul = $('<ul />');
    var r = $('<input>').attr({
      type: 'radio',
      name: 'new_owner',
      value: room_members[i].user_id
            }).change(function() {
      $('#transfer_authority_button').prop('disabled', false);
            });
    $('#at_new_owner_list').append(r);
    $('#at_new_owner_list').append($('<label/>').text(room_members[i].name));
  }
  $('#transfer_authority_button').prop('disabled', true);
  $('#authority_transfer_screen').css('display', 'block');
}

function on_choosing_someone_to_transfer_authority() {
  $('#transfer_authority_button').prop('disabled', false);
}

function hide_authority_transfer_form() {
  $('#authority_transfer_screen').css('display', 'none');
}

function transfer_authority() {
  var user_id = $('input[name="new_owner"]:checked').val();
  ws_disp.trigger('transfer_authority', { user_id: user_id });
  hide_authority_transfer_form();
}

function dice_1d100() {
  ws_disp.trigger('dice_1d100', {});
}
