(function (root) {
  'use strict';

  var Rule = {};
  var geometry_info = {
    "edge": [
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false],
        [false, false, false, true, false, true, false, true, false, true, false, false, false, false, false, false, false, false],
        [false, false, true, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, true, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, true, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, true, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false],
        [false, false, false, true, false, true, false, true, false, true, false, false, false, false, false, false, false, false],
        [false, false, false, false, true, false, false, false, true, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
    ],
    "vertex": [
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, true, false, true, false, true, false, true, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, true, false, true, false, true, false, true, false, true, false, true, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, true, false, true, false, true, false, true, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false],
        [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
    ]};

    var CELL_GEOMETRY = [
        { x: 7 , y: 2  },
        { x: 11, y: 2  },
        { x: 15, y: 2  },
        { x: 5 , y: 4  },
        { x: 9 , y: 4  },
        { x: 13, y: 4  },
        { x: 17, y: 4  },
        { x: 3 , y: 6  },
        { x: 7 , y: 6  },
        { x: 11, y: 6  },
        { x: 15, y: 6  },
        { x: 19, y: 6  },
        { x: 5 , y: 8  },
        { x: 9 , y: 8  },
        { x: 13, y: 8  },
        { x: 17, y: 8  },
        { x: 7 , y: 10 },
        { x: 11, y: 10 },
        { x: 15, y: 10 }
    ];

    var VERTEX_OFFSET = [
        { x: 2 , y: 1  },
        { x: 0 , y: 1  },
        { x: -2, y: 1  },
        { x: -2, y: -1 },
        { x: 0 , y: -1 },
        { x: 2 , y: -1 }
    ];

    var EDGE_OFFSET = [
        { x: 2 , y: 0  },
        { x: 1 , y: 1  },
        { x: -1, y: 1  },
        { x: -2, y: 0  },
        { x: -1, y: -1 },
        { x: 1 , y: -1 }
    ];

    var DX = [1, 0, -1, 0];
    var DY = [0, 1, 0, -1];

    function is_edge(x, y) {
      return geometry_info.edge[x][y];
    }

    function is_vertex(x, y) {
      return geometry_info.vertex[x][y];
    }

    function is_valid_coord(x, y) {
      return 0 <= x && x < 23 && 0 <= y && y < 18
    }

    function is_obj(object_info, x, y, player_num, kind) {
        if (!is_valid_coord(x, y)) {
          return false;
        }

        var o = object_info[x][y];
        if (o != null && o['kind'] == kind && o['player'] == player_num) {
          return true;
        }
        return false;
    }

    function is_someones_obj(object_info, x, y, kind) {
        if (!is_valid_coord(x, y)) {
          return false;
        }

        var o = object_info[x][y];
        if (o !== null && o['kind'] === kind) {
          return true;
        }
        return false;
    }

    function is_house_obj(object_info, x, y, player_num) {
        return is_obj(object_info, x, y, player_num, 'house');
    }

    function is_city_obj(object_info, x, y, player_num) {
        return is_obj(object_info, x, y, player_num, 'city');
    }

    function is_road_obj(object_info, x, y, player_num) {
      return is_obj(object_info, x, y, player_num, 'road');
    }

    function is_able_to_put_house(object_info, x, y, player_num, force) {
        if (!is_vertex(x, y)) {
            return false;
        }

        if (is_someones_obj(object_info, x, y, 'house') || is_someones_obj(object_info, x, y, 'city')) {
            return false;
        }

        for (var i = 0; i < 4; i++) {
            if (!is_edge(x + DX[i], y + DY[i])) {
              continue;
            }

            var ax = x + DX[i] * 2;
            var ay = y + DY[i] * 2;
            if (is_someones_obj(object_info, ax, ay, 'house') || is_someones_obj(object_info, ax, ay, 'city')) {
                return false;
            }
        }

        if (force) {
            return true;
        }

        for (var i = 0; i < 4; i++) {
            var ax = x + DX[i];
            var ay = y + DY[i];
            if (is_road_obj(object_info, ax, ay, player_num)) {
              return true;
            }
        }
        return false;
    }

    function is_able_to_put_city(object_info, x, y, player_num) {
      return is_house_obj(object_info, x, y, player_num);
    }

    function is_able_to_put_road(object_info, x, y, player_num) {
        if (!is_edge(x, y)) {
          return false;
        }

        if (is_someones_obj(object_info, x, y, 'road')) {
          return false;
        }

        for (var i = 0; i < 4; i++) {
            var ax = x + DX[i];
            var ay = y + DY[i];

            if (is_vertex(ax, ay)) {
                if (is_house_obj(object_info, ax, ay, player_num)
                  || is_city_obj(object_info, ax, ay, player_num)) {
                  return true;
                }

                for (var j = 0; j < 4; j++) {
                    var bx = ax + DX[j];
                    var by = ay + DY[j];

                    if (is_road_obj(object_info, bx, by, player_num)) {
                      return true;
                    }
                }
            }
        }

        return false;
    }

    function is_able_to_put_road_at_the_beginning(object_info, x, y, player_num, hx, hy) {
        if (!is_edge(x, y)) {
          return false;
        }

        if (is_someones_obj(object_info, x, y, 'road')) {
            return false;
        }

        for (var i = 0; i < 4; i++) {
            var ax = x + DX[i];
            var ay = y + DY[i];

            if (ax === hx && ay === hy) {
              return true;
            }
        }

        return false
    }

    Rule.is_able_to_put_house = is_able_to_put_house;
    Rule.is_able_to_put_city = is_able_to_put_city;
    Rule.is_able_to_put_road = is_able_to_put_road;
    Rule.is_able_to_put_road_at_the_beginning = is_able_to_put_road_at_the_beginning;

    root.Rule = Rule;
})(this);
