//= require jquery
//= require jquery.cookie
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require dropit

$(function() {
  hide_new_room_form();
  request_room_table();
  setInterval(request_room_table, 10000);
  $('#has_pass_checkbox').change(on_has_pass_checkbox_changed);
});

function show_new_room_form() {
  $('#new_room_screen').css('display', 'block');
}

function hide_new_room_form() {
  $('#new_room_screen').css('display', 'none');
}

function request_room_table() {
  $.get('/home/room_table', update_room_table);
}

function update_room_table(data) {
  $('#room_table_body').empty().append(data);
}

function on_has_pass_checkbox_changed() {
  var value = $('input[name="has_pass"]:checked').val();
  $('#pass_textbox').prop('disabled', value != '1');
}

