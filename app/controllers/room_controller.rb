class RoomController < ApplicationController
  def index
      unless room
          redirect_to root_path
          return
      end
  end

  helper_method :room_id, :room, :is_room_owner
  private
  def room_id
      @room_id ||= params[:room_id]
  end

  def room
      @room ||= $rooms[room_id]
  end

  def is_room_owner
      current_user && (CatanRoomController.has_own_room? current_user.user_id)
  end
end

