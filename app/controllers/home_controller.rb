class HomeController < BaseController
    def index
    end

    def create_room
        name = params[:name]
        pass = params[:pass]

        if current_user && !$owner_map[current_user.user_id]
            room_id = CatanRoomController.create_room(name, pass, current_user.user_id, current_user.name)
            redirect_to "/room/#{room_id}"
        else
            redirect_to root_path
        end
    end

    def destroy_room
        if current_user
            CatanRoomController.destroy_owning_room(current_user.user_id)
        end
        redirect_to root_path
    end

    def room_table
        render layout: false
    end
end

