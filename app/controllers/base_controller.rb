class BaseController < ApplicationController
    protect_from_forgery

    def login_required
        @current_user ||= User.find_by(:session_id => cookies[:session_id])

        if not (@current_user && @current_user.activated)
            redirect_to root_path
        end
    end
end
