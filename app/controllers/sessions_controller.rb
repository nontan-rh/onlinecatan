require 'securerandom'

class SessionsController < ApplicationController
    def gen_session_id
        SecureRandom.uuid()
    end

    def callback
        auth = request.env['omniauth.auth']

        provider = auth['provider']
        uid = auth['uid']

        user = User.find_user(provider, uid)
        session_id = gen_session_id()

        if not user
            user = User.create_with_omniauth(auth)
            user.update(session_id: session_id)
            cookies[:session_id] = session_id
            redirect_to registry_index_path
        elsif not user.activated
            user.update(session_id: session_id)
            cookies[:session_id] = session_id
            redirect_to registry_index_path
        else
            user.update(session_id: session_id)
            cookies[:session_id] = session_id
            cookies[:name] = user.name
            redirect_to root_path
        end
    end

    def destroy
        cookies[:session_id] = nil
        redirect_to root_path
    end
end

