require 'json'
require 'catan/room'
require 'securerandom'

# key: room_id, value: room
$rooms ||= { }

# key: user_id, value: room
$user_map ||= { }

# key: user_id, value: room
$owner_map ||= { }

class CatanRoomController < WebsocketRails::BaseController
    def self.create_room(name, pass, owner_user_id, owner_name)
        room = Room.new(name, pass, owner_user_id, owner_name)
        room_id = room.room_id
        $rooms[room_id] = room
        $owner_map[owner_user_id] = room

        room_id
    end

    def self.destroy_owning_room(user_id)
        room = $owner_map[user_id]

        return unless (room && room.owner_user_id == user_id)

        room_members = room.room_members
        room_members.each {|x|
            $user_map.delete(x.user_id)
            x.close!
        }

        $rooms.delete room.room_id
        $owner_map.delete user_id
    end

    def self.has_own_room?(user_id)
        !!$owner_map[user_id]
    end

    # Returns [user_id, room]
    def get_user_info(session_id)
      user_id = User.find_by(session_id: session_id).user_id
      room = $user_map[user_id]
      [user_id, room]
    end

    def join_receive
        room_id = message['room_id']
        session_id = message['session_id']
        user_id = User.find_by(session_id: session_id).user_id

        room = $rooms[room_id]
        return unless room

        pass = room.password
        if room.owner_user_id == user_id or pass == nil or pass == ''
            $user_map[user_id] = room
            room.add_member(user_id, session_id)
        elsif not message['password']
            send_message :password_required, {}
        elsif pass != message['password']
            connection.close!
        else
            $user_map[user_id] = room
            room.add_member(user_id, session_id)
        end
    end

    def command_receive
        session_id = connection.user_identifier
        user_id, room = get_user_info session_id

        return unless room

        begin
            room.on_receiving_command session_id, message
        rescue => e
            byebug
        end
    end

    def chat_receive
        session_id = connection.user_identifier
        user_id, room = get_user_info session_id

        return unless room

        room.on_receiving_chat session_id, message
    end

    def disconnected
        session_id = connection.user_identifier
        user_id, room = get_user_info session_id

        $user_map.delete(user_id)
        need_to_delete_room = room.remove_member session_id
        if need_to_delete_room
            CatanRoomController.destroy_owning_room room.owner_user_id
        end
    end

    def player_list_changed
        session_id = connection.user_identifier
        user_id, room = get_user_info session_id

        return unless room && user_id == room.owner_user_id

        room.change_player_list message['player_list'], message['not_player_list']
    end

    def start_button_clicked
        session_id = connection.user_identifier
        user_id, room = get_user_info session_id

        return unless room && user_id == room.owner_user_id

        room.game_start
    end

    def transfer_authority
        session_id = connection.user_identifier
        user_id, room = get_user_info session_id
        new_owner_id = message['user_id']

        return if $owner_map[new_owner_id] || user_id != room.owner_user_id

        return unless room.transfer_authority message['user_id']
        $owner_map.delete user_id
        $owner_map[new_owner_id] = room
    end

    def dice_1d100
        session_id = connection.user_identifier
        user_id, room = get_user_info session_id

        return unless room

        room.dice_1d100 session_id
    end
end
