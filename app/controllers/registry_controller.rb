class RegistryController < ApplicationController
    protect_from_forgery

    def login_required
        @current_user ||= User.find_by(:session_id => cookies[:session_id])

        if not @current_user
            redirect_to root_path
        end
    end

    def index
    end

    def settings
    end

    def register
        current_user.update(name: params[:name], activated: true)

        cookies[:name] = params[:name]
        redirect_to root_path
    end
end
