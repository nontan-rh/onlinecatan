require 'securerandom'

class User < ActiveRecord::Base
    def self.create_with_omniauth(auth)
        create! do |user|
            user.provider = auth['provider']
            user.uid = auth['uid']

            user.name = auth['name']
            user.session_id = SecureRandom.uuid()
            user.user_id = SecureRandom.uuid()
            user.activated = false
        end
    end

    def self.find_user(provider, uid)
        User.find_by(provider: provider, uid: uid)
    end
end

